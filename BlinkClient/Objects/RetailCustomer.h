//
//  RetailCustomer.h
//  BlinkClient
//
//  Created by Petar on 12/17/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntryDetails.h"
#import "RetailCustomerID.h"

@interface RetailCustomer : NSObject

//NSObject elements
@property NSString* CustomerID;
@property NSString* Name;
@property NSString* Surname;
@property NSString* Email;
@property NSString* Phone;
@property NSDate*   Registerdate;
@property NSString* Device;
@property NSString* Token;
@property NSString* ImgURL;
@property NSString* Pin;
@property EntryDetails *EntryDetails;

//Methodes
-(NSDictionary *)dictionaryWithPropertiesOfObject:(RetailCustomer*)obj;
-(NSDictionary *)dictionaryWithPropertiesOfLowerObject:(id)obj;
-(RetailCustomer*)getCustomerFromDict:(NSDictionary*)data;
//
@end
