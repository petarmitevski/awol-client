//
//  RetailCardFunds.h
//  BlinkClient
//
//  Created by Petar on 12/25/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RetailCardFunds : NSObject
@property float TotalFunds;
@property float ReservedFunds;
@property float AvailableFunds;
@end
