//
//  BinAPIObject.h
//  BlinkClient
//
//  Created by Petar on 1/26/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BinAPIObject : NSObject

@property NSDictionary*BinApiObject;
@end
/*
 {
 "valid": true,
 "country": "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF",
 "country-code": "MK",
 "card-type": "DEBIT",
 "card-brand": "VISA",
 "card-category": "ELECTRON",
 "issuer-phone": "00389 2 5 100 444",
 "issuer": "NLB TUTUNSKA BANKA AD SKOPJE",
 "issuer-website": "http://www.nlbtb.com.mk/"
 }
 */
