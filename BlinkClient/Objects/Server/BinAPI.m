//
//  BinAPI.m
//  BlinkClient
//
//  Created by Petar on 1/26/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "BinAPI.h"
#import <Firebase/Firebase.h>
#import <FirebaseAuth/FirebaseAuth.h>

@implementation BinAPI
+(void)getCardDetailsForBin:(NSString*)bin{
    BinAPIObject*object=[[BinAPIObject alloc]init];
    
        //POST DATA
    
    NSDictionary *body=[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"petar95mkd",@"0ePi0vKqx01d5Q3htAUDALEBJHH1szTE0e6YJ1lyArpzawfc",bin, nil] forKeys:[NSArray arrayWithObjects:@"user-id",@"api-key",@"bin-number", nil]];
        NSData *data = [NSJSONSerialization dataWithJSONObject:body options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonStr = [[NSString alloc] initWithData:data
                                                  encoding:NSUTF8StringEncoding];
        NSLog(@"JSON REQUEST %@",jsonStr);
        //URL CONFIG
        NSString *serverURL = @"https://neutrinoapi.com/bin-lookup";
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: serverURL]];
        //POST DATA SETUP
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            if (error) {
                NSLog(@"Error:%@",error.description);
            }
            if (data) {
                NSString *returnString = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
                NSLog(@"Response:%@",returnString);

                NSDictionary *json_response = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:0
                                                                                error:NULL];
                object.BinApiObject=[[NSDictionary alloc]initWithDictionary:json_response];
                NSLog(@"json_response: %@",json_response);
                [self finishWithObject:object];
                
            }
        }];
}

+(void)finishWithObject:(BinAPIObject*)object{
    NSDictionary * userInfo = [[NSDictionary alloc]initWithDictionary:object.BinApiObject];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"PaymentCardInfo"
     object:self
     userInfo:userInfo];
}

+(RetailCard*)cardObjectForObject:(BinAPIObject*)object andAdditionalInfo:(NSDictionary*)info{
    RetailCard *retailCard=[[RetailCard alloc]init];
    RetailCardFunds *retailCardFunds=[[RetailCardFunds alloc]init];
    [retailCardFunds setTotalFunds:20000];
    [retailCardFunds setReservedFunds:0];
    [retailCardFunds setAvailableFunds:20000];
    [retailCard setRegisterdate:[NSDate date]];
    [retailCard setCardFunds:retailCardFunds];
    [retailCard setCardID:[RetailCardID generateRetailCardID]];
    [retailCard setCustomerID:FIRAuth.auth.currentUser.uid];
    [retailCard setCardType:[self getCardType:object.BinApiObject[@"card-type"]]];
    [retailCard setBankIssuer:[self getBankName:object.BinApiObject[@"issuer"]]];
    [retailCard setInterbankNetwork:[self getCardBrand:object.BinApiObject[@"card-brand"]]];
    [retailCard setStatus:@"active"];
    [retailCard setInterType:[self getInterTypeForBank:retailCard.BankIssuer cardBrand:retailCard.InterbankNetwork type:retailCard.CardType]];
    NSString *expDate = info[@"expDate"];
    NSArray *expDateComp = [expDate componentsSeparatedByString:@"/"];
    [retailCard setEXPm:expDateComp[0]];
    [retailCard setEXPy:expDateComp[1]];
    [retailCard setCardNumber:info[@"cardNumber"]];
    [retailCard setCardBin:[info[@"cardNumber"] substringToIndex:6]];

    return retailCard;
    
}
+(NSString*)getBankName:(NSString*)string{
    if([string isEqualToString:@"NLB TUTUNSKA BANKA AD SKOPJE"] ||[string isEqualToString:@"NLB BANKA AD SKOPJE"]){
        return @"NLB Banka AD Skopje";
    }else if([string isEqualToString:@"KOMERCIJALNA BANKA A.D. SKOPJE"] || [string isEqualToString:@"KOMERCIJALNA BANKA AD SKOPJE"]){
        return @"Komercijalna Banka AD Skopje";
    }else if([string isEqualToString:@"HALKBANK AD SKOPJE"]){
        return @"Halkbank AD Skopje";
    }else if([string isEqualToString:@"STOPANSKA BANKA AD SKOPJE"]){
        return @"Stopanska Banka AD Skopje";
    }else{
        return nil;
    }
}
+(NSString*)getCardBrand:(NSString*)string{
    if([string isEqualToString:@"MASTERCARD"] ||[string isEqualToString:@"Mastercard"] || [string isEqualToString:@"MasterCard"]){
        return @"MasterCard";
    }else if([string isEqualToString:@"Visa"] || [string isEqualToString:@"VISA"]){
        return @"Visa";
    }else{
        return nil;
    }
}
+(NSString*)getCardType:(NSString*)string{
    if([string isEqualToString:@"DEBIT"] ||[string isEqualToString:@"Debit"]){
        return @"Debit";
    }else if([string isEqualToString:@"CREDIT"] || [string isEqualToString:@"Credit"]){
        return @"Credit";
    }else{
        return nil;
    }
}
+(NSString*)getInterTypeForBank:(NSString*)bank cardBrand:(NSString*)brand type:(NSString*)type{
    NSMutableString*returnString=[[NSMutableString alloc]init];
    if([bank isEqualToString:@"NLB Banka AD Skopje"]){
        [returnString appendString:@"1"];
    }else if([bank isEqualToString:@"Stopanska Banka AD Skopje"]){
        [returnString appendString:@"2"];
    }else if([bank isEqualToString:@"Komercijalna Banka AD Skopje"]){
        [returnString appendString:@"3"];
    }else if([bank isEqualToString:@"Halkbank AD Skopje"]){
        [returnString appendString:@"4"];
    }
    
    if([brand isEqualToString:@"MasterCard"]){
        [returnString appendString:@"2"];
    }else if([brand isEqualToString:@"Visa"]){
        [returnString appendString:@"1"];
    }
    
    if([type isEqualToString:@"Credit"]){
        [returnString appendString:@"1"];
    }else if([type isEqualToString:@"Debit"]){
        [returnString appendString:@"2"];
    }
    
    
    return [returnString copy];
}
/*
 @property NSString* CardID;
 @property NSString* CustomerID;
 @property NSString* CardNumber;
 @property NSString* CardBin;
 @property NSString* CardType;
 @property NSString* EXPm;
 @property NSString* EXPy;
 @property NSString* BankIssuer;
 @property NSString* InterbankNetwork;
 @property NSString* Status;
 @property RetailCardFunds *CardFunds;
 @property NSDate*   Registerdate;
 @property NSString* InterType;

 */
@end
