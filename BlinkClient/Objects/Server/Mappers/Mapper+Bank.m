//
//  Mapper+Bank.m
//  BlinkClient
//
//  Created by Petar on 12/27/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "Mapper+Bank.h"

@implementation Mapper_Bank
-(NSString*)bankNameForType:(id)type{
    
    static NSDictionary* bankNamesByType = nil;
    
    if (!bankNamesByType) {
        bankNamesByType  =  @{@"1"      : @"NLB Banka AD Skopje",
                              @"2"      : @"Stopanska Banka AD Skopje",
                              @"3"      : @"Ohridska Banka AD Skopje",
                              @"4"      : @"Komercijalna Banka AD Skopje",
                              @"5"      : @"Halkbank AD Skopje"
                              };
    }
    NSString* bankName = [bankNamesByType objectForKey:type];
    return bankName;
}
-(NSString*)bankImgForType:(id)type{
    
    static NSDictionary* bankImgByType = nil;
    
    if (!bankImgByType) {
        bankImgByType   =   @{@"1"      : @"cc-1",
                              @"2"      : @"cc-2",
                              @"3"      : @"cc-3",
                              @"4"      : @"cc-4",
                              @"5"      : @"cc-5"
                              };
    }
    NSString* bankImg = [bankImgByType objectForKey:type];
    return bankImg;
}

-(NSString*)bankLogoForType:(id)type{
    
    static NSDictionary* bankLogoByType = nil;
    
    if (!bankLogoByType) {
        bankLogoByType   =   @{@"1"     : @"nlb",
                              @"2"      : @"stb",
                              @"3"      : @"ohb",
                              @"4"      : @"kob",
                              @"5"      : @"hab"
                              };
    }
    NSString* bankLogo = [bankLogoByType objectForKey:type];
    return bankLogo;
}
@end
