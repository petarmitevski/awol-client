//
//  Mapper+RetailCard.m
//  BlinkClient
//
//  Created by Petar on 12/27/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "Mapper+RetailCard.h"

@implementation Mapper_RetailCard


+(NSString*)bankCardForInterType:(NSString*)type{
    
    static NSDictionary* bankCardByInterType = nil;
    
    if (!bankCardByInterType) {
        bankCardByInterType  =  @{@"111"      : @"nlbvisaclassic",
                                  @"112"      : @"nlbvisaelectron",
                                  @"121"      : @"nlbmastercredit",
                                  @"122"      : @"nlbmasterdebit",
                                  @"211"      : @"stbvisaclassic",
                                  @"212"      : @"stbvisaelectron",
                                  @"221"      : @"stbmastercredit",
                                  @"222"      : @"stbmasterdebit",
                                  @"311"      : @"kobvisaclassic",
                                  @"312"      : @"kobvisaelectron",
                                  @"321"      : @"kobmastercredit",
                                  @"322"      : @"kobmasterdebit",
                                  @"411"      : @"habvisaclassic",
                                  @"412"      : @"habvisaelectron",
                                  @"421"      : @"habmastercredit",
                                  @"422"      : @"habmasterdebit"
                              };
    }
    NSString* bankCard = [bankCardByInterType objectForKey:type];
    return bankCard;
}
@end
