//
//  GetServerData.h
//  BlinkClient
//
//  Created by Petar on 12/24/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RetailCustomer.h"
#import "RetailCard.h"
#import "PreloginViewController.h"



@interface GetServerData : NSObject
@property id sender;
@property RetailCustomer* retailCustomer;
@property NSString* CustomerID;
@property NSArray* customerRetailCards;
+ (instancetype)sharedInstance;

-(void)initializeWithSender:(id)sender;
-(void)returnSingleton;
-(void)setCustomerAndContinue:(BOOL)canContinue;
-(void)setCustomerRetailCardsAndContinue:(BOOL)canContinue;

-(NSArray*)getCustomerRetailCards;
-(RetailCustomer*)getCustomer;
@end
