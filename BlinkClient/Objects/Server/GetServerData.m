//
//  GetServerData.m
//  BlinkClient
//
//  Created by Petar on 12/24/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "GetServerData.h"
#import <Firebase/Firebase.h>
#import "AppDelegate.h"

@implementation GetServerData


+ (instancetype)sharedInstance
{
    static GetServerData *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GetServerData alloc] init];
        
    });
    return sharedInstance;
}

-(void)initializeWithSender:(id)sender{
    if([sender isKindOfClass:[PreloginViewController class]])
        _sender=sender;

    [self setCustomerIDAndContinue:YES];
}
//-(void)setSender:(PreloginViewController *)sender{}
-(void)setCustomerIDAndContinue:(BOOL)canContinue{
    [self setCustomerID:[FIRAuth auth].currentUser.uid];
    if(canContinue){
        [self setCustomerAndContinue:YES];}
   // canContinue ? [self setCustomerIDAndContinue:YES]: nil;
}
-(void)setCustomerAndContinue:(BOOL)canContinue{
        FIRFirestore *defaultFirestore = [FIRFirestore firestore];
         FIRQuery *query=[[defaultFirestore collectionWithPath:@"RetailCustomers"] queryWhereField:@"CustomerID" isEqualTo:[FIRAuth auth].currentUser.uid];
    
        [query getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
            NSLog(@"QUERY RESULT: %@",snapshot.documents);
            for(FIRDocumentSnapshot *_Nullable snapshotDocument in snapshot.documents){
                NSLog(@"Query Result Customer: %@ data: %@",snapshotDocument.documentID,snapshotDocument.data);
                RetailCustomer *getCustomer=[[RetailCustomer alloc]getCustomerFromDict:snapshotDocument.data];
                [self setRetailCustomer:getCustomer];
            }
            if(canContinue){
                [self setCustomerRetailCardsAndContinue:YES];}
            //canContinue ? [self setCustomerRetailCardsAndContinue:YES] : nil;
        }];
}
-(void)setCustomerRetailCardsAndContinue:(BOOL)canContinue{
    FIRFirestore *defaultFirestore = [FIRFirestore firestore];
    FIRQuery *query=[[defaultFirestore collectionWithPath:@"RetailCards"] queryWhereField:@"CustomerID" isEqualTo:[FIRAuth auth].currentUser.uid];
    NSMutableArray *cards=[[NSMutableArray alloc]init];
    [query getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
        NSLog(@"QUERY RESULT: %@",snapshot.documents);
        for(FIRDocumentSnapshot *_Nullable snapshotDocument in snapshot.documents){
            NSLog(@"Query Result Cards: %@ data: %@",snapshotDocument.documentID,snapshotDocument.data);
            RetailCard *retailCard=[[RetailCard alloc]getRetailCardFromDict:snapshotDocument.data];
            if([retailCard.Status isEqualToString:@"active"]){
                [cards addObject:retailCard];
                
            }
    
        }
        [self setCustomerRetailCards:[cards copy]];
        
        if(canContinue){
            [self returnSingleton];}
        else{
            [[NSNotificationCenter defaultCenter]postNotificationName:@"setCustomerRetailCards-FINISHED"  object:nil userInfo:nil];
        }
      //  canContinue ? [self returnSingleton] :nil;
    }];

    
    }
-(RetailCustomer*)getCustomer{
    return _retailCustomer;
    
}
-(NSString*)getCustomerID{
    return _CustomerID;
}
-(NSArray*)getCustomerRetailCards{
    return _customerRetailCards;
}

-(void)returnSingleton{
    
    if([_sender isKindOfClass:[PreloginViewController class]]){
//        AppDelegate *app=[[UIApplication sharedApplication] delegate];
//        [app.window setRootViewController:app.apmm];
        [(PreloginViewController*)_sender finalizeServerSingleton];
    }
    
}
@end
