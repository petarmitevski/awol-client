//
//  BinAPI.h
//  BlinkClient
//
//  Created by Petar on 1/26/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BinAPIObject.h"
#import "RetailCard.h"
#import "RetailCardFunds.h"
#import "RetailCardID.h"
@interface BinAPI : NSObject

+(void)getCardDetailsForBin:(NSString*)bin;
+(void)finishWithObject:(BinAPIObject*)object;
+(RetailCard*)cardObjectForObject:(BinAPIObject*)object andAdditionalInfo:(NSDictionary*)info;
+(NSString*)getBankName:(NSString*)string;
+(NSString*)getCardBrand:(NSString*)string;
+(NSString*)getCardType:(NSString*)string;

@end
