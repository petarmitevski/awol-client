//
//  RetailCard.m
//  BlinkClient
//
//  Created by Petar on 12/25/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "RetailCard.h"
#import <objc/runtime.h>


@implementation RetailCard
- (NSDictionary *) dictionaryWithPropertiesOfLowerObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[obj valueForKey:key] forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

- (NSDictionary *) dictionaryWithPropertiesOfObject:(RetailCard*)obj
{
    NSDictionary *entryDetails=[self dictionaryWithPropertiesOfLowerObject:obj.CardFunds];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        if([key isEqualToString:@"CardFunds"]){
            break;
        }
        [dict setObject:[obj valueForKey:key] forKey:key];
    }
    [dict setObject:entryDetails forKey:@"CardFunds"];
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}
-(RetailCard*)getRetailCardFromDict:(NSDictionary*)data{
    [self setCustomerID:data[@"CustomerID"]];
    [self setCardID:data[@"CardID"]];
    [self setCardNumber:data[@"CardNumber"]];
    [self setCardBin:data[@"CardBin"]];
    [self setCardType:data[@"CardType"]];
    [self setEXPm:data[@"EXPm"]];
    [self setEXPy:data[@"EXPy"]];
    [self setBankIssuer:data[@"BankIssuer"]];
    [self setInterbankNetwork:data[@"InterbankNetwork"]];
    [self setStatus:data[@"Status"]];
    [self setRegisterdate:data[@"RegisterDate"]];
    [self setInterType:data[@"InterType"]];
    RetailCardFunds*retailCardsFunds=[[RetailCardFunds alloc]init];
    [retailCardsFunds setTotalFunds:[data[@"CardFunds"][@"TotalFunds"] floatValue]];
    [retailCardsFunds setReservedFunds:[data[@"CardFunds"][@"ReservedFunds"] floatValue]];
    [retailCardsFunds setAvailableFunds:[data[@"CardFunds"][@"AvailableFunds"] floatValue]];
    [self setCardFunds:retailCardsFunds];
    return self;
}

@end


