//
//  Device.h
//  BlinkClient
//
//  Created by Petar on 12/19/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/utsname.h>

@interface Device : NSObject
- (NSString*) deviceName;

@end
