//
//  RetailCustomer.m
//  BlinkClient
//
//  Created by Petar on 12/17/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "RetailCustomer.h"
#import <objc/runtime.h>

@implementation RetailCustomer
- (NSDictionary *) dictionaryWithPropertiesOfLowerObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[obj valueForKey:key] forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

- (NSDictionary *) dictionaryWithPropertiesOfObject:(RetailCustomer*)obj
{
    NSDictionary *entryDetails=[self dictionaryWithPropertiesOfLowerObject:obj.EntryDetails];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        if([key isEqualToString:@"EntryDetails"]){
            break;
        }
        [dict setObject:[obj valueForKey:key] forKey:key];
    }
    [dict setObject:entryDetails forKey:@"EntryDetails"];
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}
-(RetailCustomer*)getCustomerFromDict:(NSDictionary*)data{
    EntryDetails*entry=[[EntryDetails alloc]init];
    [entry setUsername:data[@"EntryDetails"][@"Username"]];
    [entry setPassword:data[@"EntryDetails"][@"Password"]];
    [self setName:data[@"Name"]];
    [self setCustomerID:data[@"CustomerID"]];
    [self setSurname:data[@"Surname"]];
    [self setEmail:data[@"Email"]];
    [self setToken:data[@"Token"]];
    [self setDevice:data[@"Device"]];
    [self setPhone:data[@"Phone"]];
    [self setEntryDetails:entry];
    [self setImgURL:data[@"ImgURL"]];
    [self setPin:data[@"Pin"]];
    [self setRegisterdate:data[@"Registerdate"]];
    
    return self;
}

@end
