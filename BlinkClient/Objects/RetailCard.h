//
//  RetailCard.h
//  BlinkClient
//
//  Created by Petar on 12/25/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RetailCardFunds.h"

@interface RetailCard : NSObject

@property NSString* CardID;
@property NSString* CustomerID;
@property NSString* CardNumber;
@property NSString* CardBin;
@property NSString* CardType;
@property NSString* EXPm;
@property NSString* EXPy;
@property NSString* BankIssuer;
@property NSString* InterbankNetwork;
@property NSString* Status;
@property NSDate*   Registerdate;
@property NSString* InterType;
@property RetailCardFunds *CardFunds;


+ (NSArray*)getRetailCards;
- (NSDictionary *) dictionaryWithPropertiesOfLowerObject:(id)obj;
- (NSDictionary *) dictionaryWithPropertiesOfObject:(RetailCard*)obj;
- (RetailCard*)getRetailCardFromDict:(NSDictionary*)data;
@end
