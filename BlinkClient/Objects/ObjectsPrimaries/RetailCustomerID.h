//
//  RetailCustomerID.h
//  BlinkClient
//
//  Created by Petar on 12/17/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RetailCustomerID : NSObject
@property NSString *ID;

-(NSString*)generateRetailCustomerID;

@end
