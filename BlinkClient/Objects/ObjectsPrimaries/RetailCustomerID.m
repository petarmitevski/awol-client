//
//  RetailCustomerID.m
//  BlinkClient
//
//  Created by Petar on 12/17/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "RetailCustomerID.h"
#import <Firebase/Firebase.h>
#import <objc/runtime.h>

@implementation RetailCustomerID
NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
-(NSString *) generateRetailCustomerID{
    NSMutableString *randomString = [NSMutableString stringWithCapacity: 20];
    for (int i=0; i<20; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
        __block NSString* cID=[randomString copy];
        __block BOOL canProceed=NO;
        [[[[FIRFirestore firestore] collectionWithPath:@"RetailCustomers"] queryWhereField:@"CustomerID" isEqualTo:cID] getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
            if(snapshot.documents.count==1){
                [self generateRetailCustomerID];
            }else { canProceed=YES;}
        }];
    return randomString;
}

@end
