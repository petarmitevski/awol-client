//
//  RetailCardID.m
//  BlinkClient
//
//  Created by Petar on 1/27/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "RetailCardID.h"
#import <objc/runtime.h>

@implementation RetailCardID

NSString *lettersAllowed = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
+(NSString *)generateRetailCardID{
    NSMutableString *randomString = [NSMutableString stringWithCapacity: 20];
    for (int i=0; i<20; i++) {
        [randomString appendFormat: @"%C", [lettersAllowed characterAtIndex: arc4random_uniform([lettersAllowed length])]];
    }
    return randomString;
}
@end
