//
//  RetailCardID.h
//  BlinkClient
//
//  Created by Petar on 1/27/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RetailCardID : NSObject
@property NSString *ID;

+(NSString*)generateRetailCardID;
@end
