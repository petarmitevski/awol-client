//
//  LoginViewController.h
//  BlinkClient
//
//  Created by Petar on 10/14/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFrameButton.h"

@interface LoginViewController : UIViewController

@property UIViewController*sender;



@property (strong, nonatomic) IBOutlet SWFrameButton *nextButton;
- (IBAction)nextButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonAction:(id)sender;


@end
