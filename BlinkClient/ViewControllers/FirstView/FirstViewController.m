//
//  FirstViewController.m
//  BlinkClient
//
//  Created by Petar on 10/14/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "FirstViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.loginButton setTitle:@"Login to blink" forState:UIControlStateNormal];
    [self.loginButton sizeToFit];
    self.loginButton.cornerRadius = 8.0f;
    self.loginButton.borderWidth = 1.3f;
    self.loginButton.tintColor = [UIColor whiteColor];
    
    [self.registerButton setTitle:@"Apply for an account" forState:UIControlStateNormal];
    [self.registerButton sizeToFit];
    self.registerButton.cornerRadius = 8.0f;
    self.registerButton.borderWidth = 1.3f;
    self.registerButton.tintColor = [UIColor whiteColor];
    //setBackgroundImage
//    UIGraphicsBeginImageContext(self.view.frame.size);
//    [[UIImage imageNamed:@"ic_bg"] drawInRect:self.view.bounds];
//    UIImage *imageBG = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    self.view.backgroundColor = [UIColor colorWithPatternImage:imageBG];
//    
    [super viewDidLoad];
//    //setLogoImage
//    UIImage *logoImage=[UIImage imageNamed:@"ic_firstview_logo"];
//    [self.logoImageView setImage:logoImage];
    
//    //setButtonsViewAttributes
//    [self.loginButton setTitle:@"Login to blink" forState:UIControlStateNormal];
//    [self.loginButton sizeToFit];
//     self.loginButton.tintColor = [UIColor whiteColor];
//
//    [self.registerButton setTitle:@"Apply for an account" forState:UIControlStateNormal];
//    [self.registerButton sizeToFit];
//    self.registerButton.tintColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)LoginButtonAction:(id)sender {
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"FirstView" bundle:nil];
    LoginViewController *loginViewController=[sb instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:loginViewController animated:YES];
    
    
}

- (IBAction)RegisterButtonAction:(id)sender {
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"FirstView" bundle:nil];
    RegisterViewController *registerViewController=[sb instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:registerViewController animated:YES];
}
@end
