//
//  RegisterViewController.h
//  BlinkClient
//
//  Created by Petar on 10/15/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFrameButton.h"

@interface RegisterViewController : UIViewController

@property (strong, nonatomic) IBOutlet SWFrameButton *nextButton;
- (IBAction)NextButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonAction:(id)sender;


@end
