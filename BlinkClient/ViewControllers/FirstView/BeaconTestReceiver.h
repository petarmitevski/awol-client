//
//  BeaconTestReceiver.h
//  BlinkClient
//
//  Created by Petar on 12/3/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BeaconTestReceiver : UIViewController<UIGestureRecognizerDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
