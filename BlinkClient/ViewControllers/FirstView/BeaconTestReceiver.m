//
//  BeaconTestReceiver.m
//  BlinkClient
//
//  Created by Petar on 12/3/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "BeaconTestReceiver.h"

@interface BeaconTestReceiver ()

@end

@implementation BeaconTestReceiver

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // Do any additional setup after loading the view.
}
//- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    [super pushViewController:viewController animated:animated];
//    self.interactivePopGestureRecognizer.enabled = YES;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
