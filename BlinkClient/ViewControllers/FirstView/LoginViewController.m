//
//  LoginViewController.m
//  BlinkClient
//
//  Created by Petar on 10/14/17.
//  Copyright © 2017 Petar. All rights reserved.
//


#import "LoginViewController.h"
#import "FirstViewController.h"
#import "BeaconTestReceiver.h"



@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //setup button next
    [self.nextButton setTitle:@"Next →" forState:UIControlStateNormal];
    [self.nextButton sizeToFit];
    self.nextButton.cornerRadius = 8.0f;
    self.nextButton.borderWidth = 1.3f;
    self.nextButton.tintColor = [UIColor whiteColor];
    
    [super viewDidLoad];
    
    
   // self.userNameTextField.borderStyle= UITextBorderStyleRoundedRect;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://localhost:5000/api/values/5"]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"Request reply: %@", requestReply);
    }] resume];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextButtonAction:(id)sender {
    
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"FirstView" bundle:nil];
    BeaconTestReceiver *beacon=[sb instantiateViewControllerWithIdentifier:@"BeaconTestReceiver"];
    
    [self.navigationController pushViewController:beacon animated:YES];
}

- (IBAction)backButtonAction:(id)sender {
    
//    if([_sender isKindOfClass:[FirstViewController class]]){
        [self.navigationController popViewControllerAnimated:YES];
  //  }
}
@end
