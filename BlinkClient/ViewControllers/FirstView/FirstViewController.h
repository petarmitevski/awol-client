//
//  FirstViewController.h
//  BlinkClient
//
//  Created by Petar on 10/14/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFrameButton.h"

@interface FirstViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet SWFrameButton *loginButton;
@property (strong, nonatomic) IBOutlet SWFrameButton *registerButton;


- (IBAction)LoginButtonAction:(id)sender;
- (IBAction)RegisterButtonAction:(id)sender;


@end
