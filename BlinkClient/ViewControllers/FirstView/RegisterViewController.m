//
//  RegisterViewController.m
//  BlinkClient
//
//  Created by Petar on 10/15/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //setup button next
    [self.nextButton setTitle:@"Next →" forState:UIControlStateNormal];
    [self.nextButton sizeToFit];
    self.nextButton.cornerRadius = 8.0f;
    self.nextButton.borderWidth = 1.3f;
    self.nextButton.tintColor = [UIColor whiteColor];
    
    [super viewDidLoad];
    
    
    // self.userNameTextField.borderStyle= UITextBorderStyleRoundedRect;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://blinkwebapitest.gearhostpreview.com/api/values"]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"Request reply: %@", requestReply);
    }] resume];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)NextButtonAction:(id)sender {
}
- (IBAction)backButtonAction:(id)sender {
    
            [self.navigationController popViewControllerAnimated:YES];
}
@end
