//
//  ManagePaymentCardsViewController.h
//  BlinkClient
//
//  Created by Petar on 1/25/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MLWBluuurView.h>
@interface ManagePaymentCardsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property UIView*containerView;
@property UIVisualEffectView *vibrancyEffectView;
@property MLWBluuurView *backgroundEffectView;
@property UITableView *tableView;
@property UIScrollView *scrollView;
@property UIView*secondView;
@property UIImageView*svImgView;
@property UILabel*cardID;
@property UILabel*cardName;
@property UILabel*cardType;
@property UILabel*cardNumber;

@property NSArray*cardsArray;
@end
