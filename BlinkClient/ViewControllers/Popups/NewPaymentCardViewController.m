//
//  NewPaymentCardViewController.m
//  BlinkClient
//
//  Created by Petar on 1/26/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "NewPaymentCardViewController.h"
#import "VibrantTextField.h"
#import "Luhn.h"
#import "BinAPI.h"
#import "Mapper+RetailCard.h"
#import <Firebase/Firebase.h>
#import "GetServerData.h"

@interface NewPaymentCardViewController ()

@end

@implementation NewPaymentCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self notificationsObservers];
    [self.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    _size=CGRectMake(30, self.view.frame.size.height, self.view.frame.size.width-60, self.view.frame.size.height/2);
    _sizeBounds=CGRectMake(0, 0, self.view.frame.size.width-60, self.view.frame.size.height/2);
    _containerView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 50, 30, 30)];
    [_containerView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [_containerView.layer setCornerRadius:15.0f];
    [_containerView setClipsToBounds:YES];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = _sizeBounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:blurEffectView];
    
    UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    _vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
    [_vibrancyEffectView setFrame:_sizeBounds];
    [[blurEffectView contentView] addSubview:_vibrancyEffectView];
    [self.view addSubview: [self setBackgroundBlurEffect:self.view]];
    [self.view addSubview:_containerView];
    [self setupTitle];
    _scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 45, _size.size.width, _size.size.height)];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width*3, _scrollView.frame.size.height)];
    [_scrollView setScrollEnabled:NO];
    [_containerView addSubview:_scrollView];
  //  [_containerView addSubview:_scrollView];
    [self setupFields];
    
    
   // [BinAPI getCardDetailsForBin:@"4324815583152808"];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self animateBackground];
    [super viewWillAppear:YES];
    NSLog(@"userInfo in vwa %@",_userInfo);
}
-(void)notificationsObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotification:)
                                                 name:@"PaymentCardInfo" object:nil];
}
-(void)animateBackground{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame=CGRectMake(30, self.view.frame.size.height, self.view.frame.size.width-60, self.view.frame.size.height/2);
                         frame.origin.y=self.view.frame.size.height/4;
                         [_containerView setFrame:frame];
                         [_backgroundEffectView setBlurRadius:5.0f];
                         // [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
                     }
                     completion:nil];
}
-(void)setupTitle{
    UILabel*titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 7, _size.size.width-20, 30)];
    [titleLabel setText:@"New payment card"];
    [titleLabel setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [titleLabel setFont:[UIFont systemFontOfSize:21.0f]];
    [_containerView addSubview:titleLabel];
    //[[_vibrancyEffectView contentView] addSubview:titleLabel];
    
    UIView *dashLine1=[[UIView alloc]initWithFrame:CGRectMake(10, 40, _size.size.width-20, 1)];
    [dashLine1 setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [dashLine1 tintColorDidChange];
    [_containerView addSubview:dashLine1];
    //[[_vibrancyEffectView contentView] addSubview:dashLine1];
    
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(_size.size.width-40, 10, 27, 27)];
    [btnClose setImage:[[UIImage imageNamed:@"ic_icon_closepopup"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    [[_vibrancyEffectView contentView] addSubview:btnClose];
    //ic_icon_closepopup
}
-(UIVisualEffectView*)setBackgroundBlurEffect:(UIView*)view{
    _backgroundEffectView = [[MLWBluuurView alloc] init];
    _backgroundEffectView.frame = view.bounds;
    _backgroundEffectView.clipsToBounds = YES;
    [_backgroundEffectView setBlurRadius:0.0f];
    return _backgroundEffectView;
}
-(void)dismiss{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame=CGRectMake(self.view.frame.size.width-50, 50, 30, 30);
                         [_containerView setFrame:frame];
                         [_containerView setAlpha:0.0f];
                         [_backgroundEffectView setBlurRadius:0.0f];
                         // [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.0f]];
                     }
                     completion:^(BOOL finished) {
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupFields{
    _firstView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, _size.size.width, _scrollView.frame.size.height)];
    [_firstView setBackgroundColor:[UIColor clearColor]];
    
    _secondView=[[UIView alloc]initWithFrame:CGRectMake(_size.size.width, 0, _size.size.width, _scrollView.frame.size.height)];
    [_secondView setBackgroundColor:[UIColor clearColor]];
    _thirdView=[[UIView alloc]initWithFrame:CGRectMake(_size.size.width*2, 0, _size.size.width, _scrollView.frame.size.height)];
    [_thirdView setBackgroundColor:[UIColor clearColor]];
    
    [_scrollView addSubview:_firstView];
    [_scrollView addSubview:_secondView];
    [_scrollView addSubview:_thirdView];
    UILabel *cardholderName=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, _size.size.width-20, 20)];
    [cardholderName setText:@"Cardholder First name"];
    VibrantTextField*cardholderNameField=[[VibrantTextField alloc]initWithFrame:CGRectMake(10, 40, _size.size.width-20, 40)];
    [cardholderNameField setBorderStyle:UITextBorderStyleRoundedRect];
    [cardholderNameField setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.25]];
    [cardholderNameField setDelegate:self];
    [cardholderNameField setTextColor:[UIColor darkGrayColor]];
    [cardholderNameField setTintColor:[UIColor darkGrayColor]];
    
    UILabel *cardholderSurname=[[UILabel alloc]initWithFrame:CGRectMake(10, 90, _size.size.width-20, 20)];
    [cardholderSurname setText:@"Cardholder Last name"];
    
    VibrantTextField*cardholderSurnameField=[[VibrantTextField alloc]initWithFrame:CGRectMake(10, 120, _size.size.width-20, 40)];
    [cardholderSurnameField setBorderStyle:UITextBorderStyleRoundedRect];
    [cardholderSurnameField setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.25]];
    [cardholderSurnameField setDelegate:self];
    [cardholderSurnameField setTextColor:[UIColor darkGrayColor]];
    [cardholderSurnameField setTintColor:[UIColor darkGrayColor]];
    
    UILabel *cardNumber=[[UILabel alloc]initWithFrame:CGRectMake(10, 170, _size.size.width-20, 20)];
    [cardNumber setText:@"Card number"];
    
    _cardNumberField=[[VibrantTextField alloc]initWithFrame:CGRectMake(10, 200, _size.size.width-20, 40)];
    [_cardNumberField setBorderStyle:UITextBorderStyleRoundedRect];
    [_cardNumberField setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.25]];
    [_cardNumberField setDelegate:self];
    [_cardNumberField setTextColor:[UIColor darkGrayColor]];
    [_cardNumberField setTintColor:[UIColor darkGrayColor]];
    _cardNumberField.keyboardType=UIKeyboardTypeNumberPad;
    [_cardNumberField setTag:103];
    [_cardNumberField addTarget:self
                      action:@selector(reformatAsCardNumber:)
            forControlEvents:UIControlEventEditingChanged];
    
    _cardTypeImgView=[[UIImageView alloc]initWithFrame:CGRectMake(_cardNumberField.frame.size.width-47, 9, 40, 22)];
    [_cardNumberField addSubview:_cardTypeImgView];
    _cardValidImgView=[[UIImageView alloc]initWithFrame:CGRectMake(_cardNumberField.frame.size.width-70, 10, 20, 20)];
     [_cardNumberField addSubview:_cardValidImgView];
    
    UILabel *cardExpDate=[[UILabel alloc]initWithFrame:CGRectMake(10, 250, _size.size.width-20, 20)];
    [cardExpDate setText:@"Card exp date"];
    
    _cardExpDateField=[[VibrantTextField alloc]initWithFrame:CGRectMake(10, 280, _size.size.width/4, 40)];
    [_cardExpDateField setBorderStyle:UITextBorderStyleRoundedRect];
    [_cardExpDateField setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.25]];
    [_cardExpDateField setDelegate:self];
    [_cardExpDateField setTextAlignment:NSTextAlignmentCenter];
    [_cardExpDateField setTextColor:[UIColor darkGrayColor]];
    [_cardExpDateField setTintColor:[UIColor darkGrayColor]];
    _cardExpDateField.keyboardType=UIKeyboardTypeNumberPad;
    [_cardExpDateField setTag:104];
    [_cardExpDateField addTarget:self
                        action:@selector(reformatAsExpDate:)
              forControlEvents:UIControlEventEditingChanged];
    
    UIButton*continueButton=[[UIButton alloc]initWithFrame:CGRectMake(_size.size.width-_size.size.width/2, 280, _size.size.width/2-10, 40)];
    [continueButton setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.25]];
    [continueButton setTitle:@"Continue" forState:UIControlStateNormal];
    [continueButton.layer setCornerRadius:5];
    [continueButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [continueButton addTarget:self action:@selector(continueButton1:) forControlEvents:UIControlEventTouchUpInside];
    [_firstView addSubview:cardholderName];
    [_firstView addSubview:cardholderNameField];
    [_firstView addSubview:cardholderSurname];
    [_firstView addSubview:cardholderSurnameField];
    [_firstView addSubview:cardNumber];
    [_firstView addSubview:_cardNumberField];
    [_firstView addSubview:cardExpDate];
    [_firstView addSubview:_cardExpDateField];
    [_firstView addSubview:continueButton];
    
    
    
    //second view
    UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setTintColor:[UIColor darkGrayColor]];
    CGRect spinnerFrame=_secondView.bounds;
    spinnerFrame.origin.y-=50;
    [spinner setFrame:spinnerFrame];
    [spinner startAnimating];
    
    
    
    
    _secondViewLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, spinnerFrame.size.height/2, spinnerFrame.size.width-20, 30)];
    [_secondViewLabel setText:@"Contacting your bank"];
    [_secondViewLabel setTextAlignment:NSTextAlignmentCenter];
    [_secondView addSubview:_secondViewLabel];
    [_secondView addSubview:spinner];
    
    UIView*imgContainer=[[UIView alloc]initWithFrame:CGRectMake(_size.size.width/5, 30, _size.size.width/5*3, _size.size.width/5*1.9)];
    [imgContainer.layer setCornerRadius:8.0f];
    [imgContainer setClipsToBounds:YES];
    
    _cardImg=[[UIImageView alloc]initWithFrame:imgContainer.bounds];
    [imgContainer addSubview:_cardImg];
    
    _bankName=[[UILabel alloc]initWithFrame:CGRectMake(10, 30+imgContainer.frame.size.height+25, _size.size.width-20, 25)];
    _cardIssuer=[[UILabel alloc]initWithFrame:CGRectMake(10, 30+imgContainer.frame.size.height+60, _size.size.width-20, 25)];
    _cardType=[[UILabel alloc]initWithFrame:CGRectMake(10, 30+imgContainer.frame.size.height+95, _size.size.width-20, 25)];
    [_bankName setTextColor:[UIColor colorWithWhite:1.0 alpha:0.7]];
    [_cardType setTextColor:[UIColor colorWithWhite:1.0 alpha:0.7]];
    [_cardIssuer setTextColor:[UIColor colorWithWhite:1.0 alpha:0.7]];
    UIButton*saveCard=[[UIButton alloc]initWithFrame:CGRectMake(_size.size.width/4, 30+imgContainer.frame.size.height+135, _size.size.width/2, 40)];
    [saveCard setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.7]];
    [saveCard.layer setCornerRadius:5.0f];
    [saveCard setTitle:@"Save card" forState:UIControlStateNormal];
    [saveCard addTarget:self action:@selector(saveCard) forControlEvents:UIControlEventTouchUpInside];
    [_thirdView addSubview:imgContainer];
    [_thirdView addSubview:_bankName];
    [_thirdView addSubview:_cardIssuer];
    [_thirdView addSubview:_cardType];
    [_thirdView addSubview:saveCard];

    
    
    
    
}
-(void)continueButton1:(id)sender{
    [self.view endEditing:YES];
    [self slideDown];
    
    NSUInteger targetCursorPosition =
    [_cardNumberField offsetFromPosition:_cardNumberField.beginningOfDocument
                       toPosition:_cardNumberField.selectedTextRange.start];
    NSString *cardNumberWithoutSpaces =
    [self removeNonDigits:_cardNumberField.text
andPreserveCursorPosition:&targetCursorPosition];
    [BinAPI getCardDetailsForBin:cardNumberWithoutSpaces];
    [self scrollToPage:1];
    
}
-(void)receivedNotification:(NSNotification *) notification{
    if(notification.userInfo){
        NSLog(@"user info: %@",notification.userInfo);
        NSUInteger targetCursorPosition =
        [_cardNumberField offsetFromPosition:_cardNumberField.beginningOfDocument
                                  toPosition:_cardNumberField.selectedTextRange.start];
        NSString *cardNumberWithoutSpaces =
        [self removeNonDigits:_cardNumberField.text
    andPreserveCursorPosition:&targetCursorPosition];
        BinAPIObject *object=[[BinAPIObject alloc]init];
        [object setBinApiObject:notification.userInfo];
        NSDictionary *additionalInfo=[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:cardNumberWithoutSpaces,_cardExpDateField.text, nil] forKeys:[NSArray arrayWithObjects:@"cardNumber",@"expDate", nil]];
        _retailCard=[BinAPI cardObjectForObject:object andAdditionalInfo:additionalInfo];
        NSString*imgName=[Mapper_RetailCard bankCardForInterType:_retailCard.InterType];
        [_cardImg setImage:[UIImage imageNamed:imgName]];
        [_bankName setText:[NSString stringWithFormat:@"• %@",_retailCard.BankIssuer]];
        [_cardIssuer setText:[NSString stringWithFormat:@"• %@",_retailCard.InterbankNetwork]];
        [_cardType setText:[NSString stringWithFormat:@"• %@",_retailCard.CardType]];
        [_secondViewLabel setText:@"Collecting info"];
        [NSTimer scheduledTimerWithTimeInterval:3.0f  target:self selector:@selector(continueToThirdPage) userInfo:nil repeats:nil];
    }
}
-(void)continueToThirdPage{
    [self scrollToPage:2];
}
-(void)scrollToPage:(int)page{
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width*page;
    frame.origin.y = 0;
    [_scrollView scrollRectToVisible:frame animated:YES];
}
-(void)slideUp{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame=CGRectMake(30, self.view.frame.size.height, self.view.frame.size.width-60, self.view.frame.size.height/2);
                         frame.origin.y=self.view.frame.size.height/4-150;
                         [_containerView setFrame:frame];
                         // [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
                     }
                     completion:nil];
}
-(void)slideDown{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame=CGRectMake(30, self.view.frame.size.height, self.view.frame.size.width-60, self.view.frame.size.height/2);
                         frame.origin.y=self.view.frame.size.height/4;
                         [_containerView setFrame:frame];
                         // [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
                     }
                     completion:nil];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField.tag==103){
        [self slideUp];
    }
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    [self slideDown];
    return YES;
}
-(void)reformatAsCardNumber:(UITextField *)textField
{
    NSUInteger targetCursorPosition =
    [textField offsetFromPosition:textField.beginningOfDocument
                       toPosition:textField.selectedTextRange.start];

    NSString *cardNumberWithoutSpaces =
    [self removeNonDigits:textField.text
andPreserveCursorPosition:&targetCursorPosition];

    if([cardNumberWithoutSpaces length]>3){
    if([[cardNumberWithoutSpaces substringToIndex:1]isEqualToString:@"5"]){
        [_cardTypeImgView setImage:[UIImage imageNamed:@"mcl"]];
    }else if([[cardNumberWithoutSpaces substringToIndex:1]isEqualToString:@"4"]){
        [_cardTypeImgView setImage:[UIImage imageNamed:@"vbl"]];
    }
    }
    if ([cardNumberWithoutSpaces length] == 16){
        BOOL isValid = [Luhn validateString:cardNumberWithoutSpaces];
        if (isValid) {
            [_cardValidImgView setImage:[[UIImage imageNamed:@"ic_icon_checkmark"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            [_cardValidImgView setTintColor:[UIColor darkGrayColor]];
            NSLog(@"Valid");
        }
        else {
            [_cardValidImgView setImage:[[UIImage imageNamed:@"ic_icon_wrong"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            [_cardValidImgView setTintColor:[UIColor darkGrayColor]];
            NSLog(@"Invalid");
        }
    }
    if ([cardNumberWithoutSpaces length] < 16) {
        [_cardValidImgView setImage:nil];
    }
    if ([cardNumberWithoutSpaces length] == 0) {
        [_cardTypeImgView setImage:nil];
    }
    if ([cardNumberWithoutSpaces length] > 16) {
        [textField setText:_previousTextFieldContent];
        textField.selectedTextRange = _previousSelection;
        return;
    }

    NSString *cardNumberWithSpaces =
    [self insertSpacesEveryFourDigitsIntoString:cardNumberWithoutSpaces
                      andPreserveCursorPosition:&targetCursorPosition];

    textField.text = cardNumberWithSpaces;
    UITextPosition *targetPosition =
    [textField positionFromPosition:[textField beginningOfDocument]
                             offset:targetCursorPosition];

    [textField setSelectedTextRange:
     [textField textRangeFromPosition:targetPosition
                           toPosition:targetPosition]
     ];
}

-(BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    _previousTextFieldContent = textField.text;
    _previousSelection = textField.selectedTextRange;
    return YES;
}

- (NSString *)removeNonDigits:(NSString *)string
    andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    NSUInteger originalCursorPosition = *cursorPosition;
    NSMutableString *digitsOnlyString = [NSMutableString new];
    for (NSUInteger i=0; i<[string length]; i++) {
        unichar characterToAdd = [string characterAtIndex:i];
        if (isdigit(characterToAdd)) {
            NSString *stringToAdd =
            [NSString stringWithCharacters:&characterToAdd
                                    length:1];

            [digitsOnlyString appendString:stringToAdd];
        }
        else {
            if (i < originalCursorPosition) {
                (*cursorPosition)--;
            }
        }
    }

    return digitsOnlyString;
}

- (NSString *)insertSpacesEveryFourDigitsIntoString:(NSString *)string
                          andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    NSMutableString *stringWithAddedSpaces = [NSMutableString new];
    NSUInteger cursorPositionInSpacelessString = *cursorPosition;
    for (NSUInteger i=0; i<[string length]; i++) {
        if ((i>0) && ((i % 4) == 0)) {
            [stringWithAddedSpaces appendString:@" "];
            if (i < cursorPositionInSpacelessString) {
                (*cursorPosition)++;
            }
        }
        unichar characterToAdd = [string characterAtIndex:i];
        NSString *stringToAdd =
        [NSString stringWithCharacters:&characterToAdd length:1];

        [stringWithAddedSpaces appendString:stringToAdd];
    }

    return stringWithAddedSpaces;
}







-(void)reformatAsExpDate:(UITextField *)textField
{
    NSUInteger targetCursorPosition =
    [textField offsetFromPosition:textField.beginningOfDocument
                       toPosition:textField.selectedTextRange.start];
    
    NSString *cardNumberWithoutSpaces =
    [self removeNonDigitsExpDate:textField.text
andPreserveCursorPosition:&targetCursorPosition];
    
    if ([cardNumberWithoutSpaces length] > 4) {
        [textField setText:_previousTextFieldContent];
        textField.selectedTextRange = _previousSelection;
        return;
    }
    
    NSString *cardNumberWithSpaces =
    [self insertDashEveryTwoDigitsIntoString:cardNumberWithoutSpaces
                      andPreserveCursorPosition:&targetCursorPosition];
    
    textField.text = cardNumberWithSpaces;
    UITextPosition *targetPosition =
    [textField positionFromPosition:[textField beginningOfDocument]
                             offset:targetCursorPosition];
    
    [textField setSelectedTextRange:
     [textField textRangeFromPosition:targetPosition
                           toPosition:targetPosition]
     ];
}
- (NSString *)removeNonDigitsExpDate:(NSString *)string
    andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    NSUInteger originalCursorPosition = *cursorPosition;
    NSMutableString *digitsOnlyString = [NSMutableString new];
    for (NSUInteger i=0; i<[string length]; i++) {
        unichar characterToAdd = [string characterAtIndex:i];
        if (isdigit(characterToAdd)) {
            NSString *stringToAdd =
            [NSString stringWithCharacters:&characterToAdd
                                    length:1];
            
            [digitsOnlyString appendString:stringToAdd];
        }
        else {
            if (i < originalCursorPosition) {
                (*cursorPosition)--;
            }
        }
    }
    
    return digitsOnlyString;
}

- (NSString *)insertDashEveryTwoDigitsIntoString:(NSString *)string
                          andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
    NSMutableString *stringWithAddedSpaces = [NSMutableString new];
    NSUInteger cursorPositionInSpacelessString = *cursorPosition;
    for (NSUInteger i=0; i<[string length]; i++) {
        if ((i>0) && ((i % 2) == 0)) {
            [stringWithAddedSpaces appendString:@"/"];
            if (i < cursorPositionInSpacelessString) {
                (*cursorPosition)++;
            }
        }
        unichar characterToAdd = [string characterAtIndex:i];
        NSString *stringToAdd =
        [NSString stringWithCharacters:&characterToAdd length:1];
        
        [stringWithAddedSpaces appendString:stringToAdd];
    }
    
    return stringWithAddedSpaces;
}
-(void)saveCard{
   // _retailCard;
    
    __block NSDictionary*cardDictionary=[_retailCard dictionaryWithPropertiesOfObject:_retailCard];
    __block FIRDocumentReference *ref = [[[FIRFirestore firestore] collectionWithPath:@"RetailCards"]documentWithPath:_retailCard.CardID];
    [ref setData:cardDictionary completion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error adding document: %@", error);
        } else {
            NSLog(@"Document added with ID: %@", ref.documentID);
            [[GetServerData sharedInstance]setCustomerRetailCardsAndContinue:NO];
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadDataOnSender) name:@"setCustomerRetailCards-FINISHED" object:nil];
        }
    }];
}
-(void)reloadDataOnSender{
    [_sender reloadData];
    [self dismiss];
}

@end
