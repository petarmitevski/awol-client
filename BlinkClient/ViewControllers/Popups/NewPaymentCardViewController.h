//
//  NewPaymentCardViewController.h
//  BlinkClient
//
//  Created by Petar on 1/26/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MLWBluuurView.h>
#import "VibrantTextField.h"
#import "RetailCard.h"
#import "MICardsView.h"
@interface NewPaymentCardViewController : UIViewController<UITextFieldDelegate>
@property UIView*containerView;
@property UIScrollView*scrollView;
@property UIVisualEffectView *vibrancyEffectView;
@property MLWBluuurView *backgroundEffectView;
@property UIView*firstView;
@property UIView*secondView;
@property UIView*thirdView;
@property CGRect size;
@property CGRect sizeBounds;
@property NSString *previousTextFieldContent;
@property UITextRange *previousSelection;
@property UIImageView *cardTypeImgView;
@property UIImageView *cardValidImgView;

@property UILabel*secondViewLabel;

@property VibrantTextField*cardNumberField;
@property VibrantTextField*cardExpDateField;
@property MICardsView*sender;
@property UIImageView *cardImg;
@property UILabel *bankName;
@property UILabel *cardType;
@property UILabel *cardIssuer;
@property RetailCard *retailCard;
@property NSDictionary *userInfo;
@end
