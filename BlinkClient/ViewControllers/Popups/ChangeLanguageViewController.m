//
//  ChangeLanguageViewController.m
//  BlinkClient
//
//  Created by Petar on 1/25/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "ChangeLanguageViewController.h"

@interface ChangeLanguageViewController ()

@end

@implementation ChangeLanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    _containerView=[[UIView alloc]initWithFrame:CGRectMake(30, self.view.frame.size.height, self.view.frame.size.width-60, self.view.frame.size.height/6*4)];
    [_containerView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [_containerView.layer setCornerRadius:15.0f];
    [_containerView setClipsToBounds:YES];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = _containerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:blurEffectView];
    [self.view addSubview:_containerView];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self animateBackground];
    [super viewWillAppear:YES];
}
-(void)animateBackground{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame=_containerView.frame;
                         frame.origin.y=self.view.frame.size.height/6;
                         [_containerView setFrame:frame];
                         [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
                     }
                     completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
