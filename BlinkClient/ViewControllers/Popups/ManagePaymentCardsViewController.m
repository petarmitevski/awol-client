//
//  ManagePaymentCardsViewController.m
//  BlinkClient
//
//  Created by Petar on 1/25/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "ManagePaymentCardsViewController.h"
#import <MLWBluuurView.h>
#import "ManagePaymentCardsTableViewCell.h"
#import <NotificationCenter/NotificationCenter.h>
#import "GetServerData.h"
#import "ManageFriendsViewController.h"
#import "Mapper+RetailCard.h"
@interface ManagePaymentCardsViewController ()

@end

@implementation ManagePaymentCardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    _containerView=[[UIView alloc]initWithFrame:CGRectMake(30, self.view.frame.size.height, self.view.frame.size.width-60, self.view.frame.size.height/6*4)];
    [_containerView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [_containerView.layer setCornerRadius:15.0f];
    [_containerView setClipsToBounds:YES];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = _containerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:blurEffectView];
    
    UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    _vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
    [_vibrancyEffectView setFrame:_containerView.bounds];
    [[blurEffectView contentView] addSubview:_vibrancyEffectView];
    [self.view addSubview: [self setBackgroundBlurEffect:self.view]];
    [self.view addSubview:_containerView];
    
    _cardsArray=[[NSArray alloc]init];
    _cardsArray=[[GetServerData sharedInstance]getCustomerRetailCards];
    
    _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, _containerView.frame.size.width, _containerView.frame.size.height-45)];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    [_tableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    _tableView.separatorColor=[UIColor clearColor];
    
    //[_tableView setBackgroundView:blurEffectView];
    //[[_vibrancyEffectView contentView] addSubview:_tableView];
    [self setupTitle];
    
    _scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 45, _containerView.frame.size.width, _containerView.frame.size.height)];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width*2, _scrollView.frame.size.height)];
    [_scrollView setScrollEnabled:NO];
    [_containerView addSubview:_scrollView];
    [_scrollView addSubview:_tableView];
    [self setupSecondView];
}
-(void)viewWillAppear:(BOOL)animated{
    [self animateBackground];
    [super viewWillAppear:YES];
}
-(void)animateBackground{
    [UIView animateWithDuration:0.5
     delay:0.0
     options:UIViewAnimationOptionCurveEaseInOut
     animations:^{
         CGRect frame=_containerView.frame;
         frame.origin.y=self.view.frame.size.height/6;
         [_containerView setFrame:frame];
          [_backgroundEffectView setBlurRadius:5.0f];
        // [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
     }
     completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)setupTitle{
    UILabel*titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 7, _containerView.frame.size.width-20, 30)];
    [titleLabel setText:@"Manage payment cards"];
    [titleLabel setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [titleLabel setFont:[UIFont systemFontOfSize:21.0f]];
    [_containerView addSubview:titleLabel];
    //[[_vibrancyEffectView contentView] addSubview:titleLabel];
    
    UIView *dashLine1=[[UIView alloc]initWithFrame:CGRectMake(10, 40, _containerView.frame.size.width-20, 1)];
    [dashLine1 setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [dashLine1 tintColorDidChange];
    [_containerView addSubview:dashLine1];
    //[[_vibrancyEffectView contentView] addSubview:dashLine1];
    
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(_containerView.frame.size.width-40, 10, 27, 27)];
    [btnClose setImage:[[UIImage imageNamed:@"ic_icon_closepopup"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    [[_vibrancyEffectView contentView] addSubview:btnClose];
    //ic_icon_closepopup
}
-(void)setupSecondView{
    _secondView=[[UIView alloc]initWithFrame:CGRectMake(_containerView.frame.size.width, 0, _containerView.frame.size.width, _containerView.frame.size.height-45)];
    [_secondView setBackgroundColor:[UIColor clearColor]];
    [_scrollView addSubview:_secondView];
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(10, 5, 35, 35)];
    [backButton setBackgroundColor:[UIColor clearColor]];
    [backButton setImage:[[UIImage imageNamed:@"ic_firstview_back"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [backButton setTintColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [_secondView addSubview:backButton];
    
    UIView*imgContainer=[[UIView alloc]initWithFrame:CGRectMake(_containerView.frame.size.width/5, 30, _containerView.frame.size.width/5*3, _containerView.frame.size.width/5*1.9)];
    [imgContainer.layer setCornerRadius:8.0f];
    [imgContainer setClipsToBounds:YES];
    _svImgView =[[UIImageView alloc]initWithFrame:imgContainer.bounds];
    [_svImgView setImage:[UIImage imageNamed:@"habmasterdebit"]];
    [_secondView addSubview:imgContainer];
    [imgContainer addSubview:_svImgView];
    UILabel *cardIDLBL=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+50, _containerView.frame.size.width-20, 30)];
    _cardID=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+70, _containerView.frame.size.width-20, 30)];
    UIView *dashLine1=[[UIView alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+100, _containerView.frame.size.width-20, 0.5f)];
    
    UILabel *cardNameLBL=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+100, _containerView.frame.size.width-20, 30)];
    _cardName=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+120, _containerView.frame.size.width-20, 30)];
    UIView *dashLine2=[[UIView alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+150, _containerView.frame.size.width-20, 0.5f)];
    
    UILabel *cardTypeLBL=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+150, _containerView.frame.size.width-20, 30)];
    _cardType=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+170, _containerView.frame.size.width-20, 30)];
    UIView *dashLine3=[[UIView alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+200, _containerView.frame.size.width-20, 0.5f)];
    
    UILabel *cardNumberLBL=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+200, _containerView.frame.size.width-20, 30)];
    _cardNumber=[[UILabel alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+220, _containerView.frame.size.width-20, 30)];
    UIView *dashLine4=[[UIView alloc]initWithFrame:CGRectMake(10, _containerView.frame.size.width/5*1.9+250, _containerView.frame.size.width-20, 0.5f)];
    
    UIButton *blockButton=[[UIButton alloc]initWithFrame:CGRectMake(_containerView.frame.size.width/4, dashLine4.frame.origin.y+40, _containerView.frame.size.width/2, 50)];
    [blockButton setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.7f]];
    [blockButton setTitleColor:[UIColor colorWithWhite:0.0f alpha:0.7f] forState:UIControlStateNormal];
    [blockButton setTitle:@"Block card" forState:UIControlStateNormal];
    [blockButton.layer setCornerRadius:10.0f];
    
    [dashLine1 setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.3f]];
    [dashLine2 setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.3f]];
    [dashLine3 setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.3f]];
    [dashLine4 setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.3f]];
    
    [cardIDLBL setTextColor:[UIColor colorWithWhite:0.0f alpha:1.0f]];
    [cardNameLBL setTextColor:[UIColor colorWithWhite:0.0f alpha:1.0f]];
    [cardTypeLBL setTextColor:[UIColor colorWithWhite:0.0f alpha:1.0f]];
    [cardNumberLBL setTextColor:[UIColor colorWithWhite:0.0f alpha:1.0f]];
    
    [cardIDLBL setText:@"Card ID:"];
    [cardNameLBL setText:@"Bank name:"];
    [cardTypeLBL setText:@"Card type:"];
    [cardNumberLBL setText:@"Card number:"];
    
    [_cardID setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [_cardName setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [_cardType setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    [_cardNumber setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
    
    [_cardID setTextAlignment:NSTextAlignmentRight];
    [_cardName setTextAlignment:NSTextAlignmentRight];
    [_cardType setTextAlignment:NSTextAlignmentRight];
    [_cardNumber setTextAlignment:NSTextAlignmentRight];
    
    [_cardID setText:@"Nde6ycJu98bcolQL0UVP"];
    [_cardName setText:@"NLB Banka"];
    [_cardType setText:@"MasterCard Debit"];
    [_cardNumber setText:@"1234 5678 9012 3456"];
    
    [_secondView addSubview:cardIDLBL];
    [_secondView addSubview:_cardID];
    [_secondView addSubview:cardNameLBL];
    [_secondView addSubview:_cardName];
    [_secondView addSubview:cardTypeLBL];
    [_secondView addSubview:_cardType];
    [_secondView addSubview:cardNumberLBL];
    [_secondView addSubview:_cardNumber];
    [_secondView addSubview:blockButton];
    
    [_secondView addSubview:dashLine1];
    [_secondView addSubview:dashLine2];
    [_secondView addSubview:dashLine3];
    [_secondView addSubview:dashLine4];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
     RetailCard *retailCard=[_cardsArray objectAtIndex:indexPath.row];
     [cell.cardName setText:[retailCard BankIssuer]];
     [cell.cardType setText:[NSString stringWithFormat:@"%@ %@",[retailCard InterbankNetwork], [retailCard CardType]]];
     [cell.cardNumber setText:[NSString stringWithFormat:@"•••• •••• •••• %@",[[retailCard CardNumber] substringFromIndex:12]]];
     NSString *imgName=[Mapper_RetailCard bankCardForInterType:[retailCard InterType]];
     [cell.cardImg setImage:[[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
     */
    RetailCard *retailCard=[_cardsArray objectAtIndex:indexPath.row];
    [_cardID setText:[retailCard CardID]];
    [_cardName setText:[retailCard BankIssuer]];
    [_cardType setText:[NSString stringWithFormat:@"%@ %@",[retailCard InterbankNetwork], [retailCard CardType]]];
    NSString*cardNumber=[retailCard CardNumber];
    [_cardNumber setText:[NSString stringWithFormat:@"%@ %@ %@ %@",[cardNumber substringWithRange:NSMakeRange(0, 4)],[cardNumber substringWithRange:NSMakeRange(4, 4)],[cardNumber substringWithRange:NSMakeRange(8, 4)],[cardNumber substringWithRange:NSMakeRange(12, 4)]]];
    NSString *imgName=[Mapper_RetailCard bankCardForInterType:[retailCard InterType]];
    [_svImgView setImage:[[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width*1;
    frame.origin.y = 0;
    [_scrollView scrollRectToVisible:frame animated:YES];
    
//    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"Popups" bundle:nil];
//    ManageFriendsViewController*fvc=[sb instantiateViewControllerWithIdentifier:@"ManageFriendsViewController"];
//    [fvc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//    [self presentViewController:fvc animated:YES completion:nil];
    
}
-(void)backButton{
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width*0;
    frame.origin.y = 0;
    [_scrollView scrollRectToVisible:frame animated:YES];
}
-(void)dismiss{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame=_containerView.frame;
                         frame.origin.y=self.view.frame.size.height;
                         [_containerView setFrame:frame];
                          [_backgroundEffectView setBlurRadius:0.0f];
                        // [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.0f]];
                     }
                     completion:^(BOOL finished) {
                          [self dismissViewControllerAnimated:NO completion:nil];
                     }];
    
}
-(UIVisualEffectView*)setBackgroundBlurEffect:(UIView*)view{
    _backgroundEffectView = [[MLWBluuurView alloc] init];
    _backgroundEffectView.frame = view.bounds;
    _backgroundEffectView.clipsToBounds = YES;
    [_backgroundEffectView setBlurRadius:0.0f];
    return _backgroundEffectView;
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //TransactionCell*cell=[[TransactionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell" withSize:CGSizeMake(self.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:indexPath])];
   // [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    
    CGSize size=CGSizeMake(_containerView.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:indexPath]);
    ManagePaymentCardsTableViewCell*cell=[[ManagePaymentCardsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell" withSize:CGSizeMake(_containerView.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:indexPath])];
   // UITableViewCell*cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    RetailCard *retailCard=[_cardsArray objectAtIndex:indexPath.row];
    [cell.cardName setText:[retailCard BankIssuer]];
    [cell.cardType setText:[NSString stringWithFormat:@"%@ %@",[retailCard InterbankNetwork], [retailCard CardType]]];
    [cell.cardNumber setText:[NSString stringWithFormat:@"•••• •••• •••• %@",[[retailCard CardNumber] substringFromIndex:12]]];
    NSString *imgName=[Mapper_RetailCard bankCardForInterType:[retailCard InterType]];
    [cell.cardImg setImage:[[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
//    CardsViewCell*cell=[[CardsViewCell alloc]init];
//    cell.layoutMargins=UIEdgeInsetsZero;
//    cell.preservesSuperviewLayoutMargins=NO;
//    [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
//    switch (indexPath.row) {
//        case 0:
//            cell=[self cellNo1:cell];
//            break;
//        case 1:
//            cell=[self cellNo2:cell];
//            break;
//        case 2:
//            cell=[self cellNo3:cell];
//            break;
//        case 3:
//            cell=[self cellNo4:cell];
//            break;
//        default:
//            break;
//    }
//
    return cell;
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cardsArray.count;
}
-(double)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  100.0f;
//    switch (indexPath.row) {
//        case 0:
//            return 20.0f;
//            break;
//        case 1:
//            return 115.0f;
//            break;
//        case 2:
//            return 130.0f;
//            break;
//        case 3:
//            return 100.0f;
//            break;
//        default:
//            return 130.0f;
//            break;
//    }
    
}
@end
