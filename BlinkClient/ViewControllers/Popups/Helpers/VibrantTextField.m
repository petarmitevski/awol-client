//
//  VibrantTextField.m
//  BlinkClient
//
//  Created by Petar on 1/26/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "VibrantTextField.h"

@implementation VibrantTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)tintColorDidChange{
    [super tintColorDidChange];
    self.textColor = self.tintColor;
}

@end
