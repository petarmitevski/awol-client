//
//  ManagePaymentCardsTableViewCell.h
//  BlinkClient
//
//  Created by Petar on 1/25/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManagePaymentCardsTableViewCell : UITableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
(NSString *)reuseIdentifier withSize:(CGSize)size;

@property UILabel*cardName;
@property UILabel*cardType;
@property UILabel *cardNumber;
@property UIImageView*cardImg;
@end
