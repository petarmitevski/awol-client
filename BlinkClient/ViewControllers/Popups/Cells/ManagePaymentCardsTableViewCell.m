//
//  ManagePaymentCardsTableViewCell.m
//  BlinkClient
//
//  Created by Petar on 1/25/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "ManagePaymentCardsTableViewCell.h"

@implementation ManagePaymentCardsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
(NSString *)reuseIdentifier withSize:(CGSize)size;

{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        _cardName=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, size.width/4*3, size.height/3)];
        _cardType=[[UILabel alloc]initWithFrame:CGRectMake(10, size.height/3+2.5, size.width/4*3, size.height/3)];
        _cardNumber=[[UILabel alloc]initWithFrame:CGRectMake(10, size.height/3*2+2.5, size.width/4*3, size.height/3)];
        UIView *cardImgView=[[UIView alloc]initWithFrame:CGRectMake(size.width/4*3-10, size.height/4, size.width/4-5, size.height/2)];
        [cardImgView.layer setCornerRadius:5.0f];
        [cardImgView setClipsToBounds:YES];
        [_cardName setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
        [_cardType setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
        [_cardNumber setTextColor:[UIColor colorWithWhite:0.0f alpha:0.7f]];
        _cardImg=[[UIImageView alloc]initWithFrame:cardImgView.bounds];
        [cardImgView addSubview:_cardImg];
        UIView *dashLine=[[UIView alloc]initWithFrame:CGRectMake(10, size.height-1, size.width-20, 0.5f)];
        [dashLine setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.3f]];
        [self.contentView addSubview:view];
        [view addSubview:_cardName];
        [view addSubview:_cardType];
        [view addSubview:_cardNumber];
        [view addSubview:dashLine];
        [view addSubview:cardImgView];
    }
    return self;
}
@end
