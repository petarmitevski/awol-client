//
//  PreloginViewController.h
//  BlinkClient
//
//  Created by Petar on 12/8/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFrameButton.h"
#import "UIColor+ColorPallete.h"
#import "Localization.h"
#import "HUDViewController.h"
#import "PinPadView.h"
#import "RetailCustomer.h"
#import "EntryDetails.h"
#import "GetServerData.h"

@interface PreloginViewController : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

//background
@property (nonatomic, strong) UIImageView *markImgView;
@property (nonatomic, strong) UIImage *markImg;
@property (nonatomic, strong) UIImageView *markOverlayImgView;
@property (nonatomic, strong) UIImage *markOverlayImg;
@property (nonatomic, strong) UIImageView *markDoubledImgView;
@property (nonatomic, strong) UIImage *markDoubledImg;


//scrollView
@property (nonatomic, strong) UIScrollView *mainScrollView;
    //scrollViewPages
    @property (nonatomic, strong) UIView *firstView;
    @property (nonatomic, strong) UIView *secondView;
    @property (nonatomic, strong) UIView *thirdView;
    @property (nonatomic, strong) UIView *fourthView;
    @property (nonatomic, strong) UIView *loginView;
        @property (nonatomic, strong) UITextField*usernameTextFieldL;
        @property (nonatomic, strong) UITextField*passwordTextFieldL;
    @property (nonatomic, strong) UIView *registerView;
        @property (nonatomic, strong) UITextField*firstNameTextFieldR;
        @property (nonatomic, strong) UITextField*lastNameTextFieldR;
        @property (nonatomic, strong) UITextField*usernameTextFieldR;
        @property (nonatomic, strong) UITextField*passwordTextFieldR;
        @property (nonatomic, strong) UITextField*phoneNumberTextFieldR;
        @property (nonatomic, strong) UITextField*countryCodeTextFieldR;
        @property (nonatomic, strong) UITextField*mailTextFieldR;

        @property (nonatomic, strong) UITextField*phoneCodeTextFieldT;



//logo
        @property (nonatomic, strong) UIImageView *logoImgView;
        @property (nonatomic, strong) UIImage *logoImg;


//pinPad
@property PinPadView *pinPadView;
//helpers
@property int counter;
@property CGRect logoFramePage1;
@property CGRect logoFramePage2;
@property NSURL *url;
@property NSData *profileImageData;
@property NSString* userUID;
@property BOOL hasToken;

//dictionary
@property NSDictionary* registrationDictionary;
@property RetailCustomer *customer;
@property EntryDetails *entry;
@property HUDViewController*HUD;



//methodes
-(void)didFinishDismissHUDWithTag:(int)tag;
-(void)finalizeServerSingleton;


//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
@end
