//
//  PreloginViewController.m
//  BlinkClient
//
//  Created by Petar on 12/8/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "PreloginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MainInterfaceViewController.h"
#import <Bluuur/Bluuur.h>
#import <FirebaseStorage/FirebaseStorage.h>
#import <Firebase/Firebase.h>
#import "RetailCustomerID.h"
#import "RetailCustomer.h"
#import "EntryDetails.h"
#import "AppDelegate.h"
#import "Device.h"
#import "KFKeychain.h"
#import "KYDrawerController.h"



@interface PreloginViewController ()

@end

@implementation PreloginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _hasToken=[defaults objectForKey:@"hasToken"];
    NSLog(@"user: %@",[FIRAuth auth].currentUser.uid);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pinEntrySucceeded:)
                                                 name:@"PinEntrySucceded"
                                               object:nil];
    if(!_hasToken){
        [self setupBackground];
        [self setupScrollView];
        [self setupScrollViewFrames];
  
    }else{
        
        [self setupBackground];
        [self setupPinPadWithType:2];
        
       // [self setupScrollView];
        //[self setupScrollViewFrames];
//       // [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(pinEntrySucceeded)
//                                                     name:@"PinEntrySucceded"
//                                                   object:nil];
    }
    
    
  //  [self setupPinPad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}
#pragma mark - Navigation

-(void)setupBackground{
    _counter=0;
    _markImgView=[[UIImageView alloc]initWithFrame:self.view.frame];
    [_markImgView setImage:[UIImage imageNamed:@"ic_main_bg1copy3"]];
//    _markImgView.image = [_markImgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [_markImgView setTintColor:UIColorFromRGBWithAlpha(0x33cea7, 1.0)];
    [_markImgView setAlpha:0.0];
    [self.view addSubview:_markImgView];
    
    _markOverlayImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, self.view.frame.size.height)];
    [_markOverlayImgView setImage:[UIImage imageNamed:@"ic_splash_overlay"]];
    [_markOverlayImgView setAlpha:0.0];
    
    _markDoubledImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [_markDoubledImgView setImage:[UIImage imageNamed:@"ic_splash_doubled"]];
//    _markDoubledImgView.image = [_markDoubledImgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [_markDoubledImgView setTintColor:UIColorFromRGBWithAlpha(0x33cea7, 1.0)];
    [_markDoubledImgView setAlpha:0.0];
    
   // [self.view addSubview:_markDoubledImgView];
    
    [self.view addSubview:_markOverlayImgView];
    
    [self animateBackground];
    
    
}
-(void)animateBackground{
    [UIView animateWithDuration:2.0f animations:^{
        
        [_markImgView setAlpha:1.0f];
        [_markOverlayImgView setAlpha:0.5f];
        CGRect frame=_markOverlayImgView.frame;
        frame.origin.y=0;
        [_markOverlayImgView setFrame:frame];
        
    } completion:^(BOOL finished) {
        [self breathIn];
    }];
}
-(void)breathIn{
    [UIView animateWithDuration:1.0f animations:^{
        
        [_markDoubledImgView setAlpha:1.0f];
        
    } completion:^(BOOL finished) {
        [self breathOut];
    }];
}
-(void)breathOut{
    [UIView animateWithDuration:1.0f animations:^{
        
        [_markDoubledImgView setAlpha:0.0f];
        _counter++;
        
    } completion:^(BOOL finished) {
        if(_counter==1){
            [self upLogo];
        }
        [self breathIn];
    }];
}
-(void)upLogo{
    [UIView animateWithDuration:2.0f animations:^{
        
        _logoFramePage1=_markOverlayImgView.frame;
        _logoFramePage1.origin.x=_logoFramePage1.size.width/6;
        _logoFramePage1.origin.y=100;
        _logoFramePage1.size.width=_logoFramePage1.size.width/1.5;
        _logoFramePage1.size.height=_logoFramePage1.size.height/1.5;
        [_mainScrollView setAlpha:1.0f];
        
        _logoFramePage2=_markOverlayImgView.frame;
        _logoFramePage2.origin.x=self.view.frame.size.width/4;
        _logoFramePage2.origin.y=-120;
        _logoFramePage2.size.width=self.view.frame.size.width/2;
        _logoFramePage2.size.height=self.view.frame.size.height/2;
        if(!_hasToken){
        [_markOverlayImgView setFrame:_logoFramePage1];
        }else {
          [_markOverlayImgView setFrame:_logoFramePage2];
            [_pinPadView setFrame:self.view.frame];
            [_pinPadView setAlpha:1.0f];
        }
    } completion:^(BOOL finished) {
        if(_hasToken){
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"ShowLocalAuthenticator"
             object:self
             userInfo:nil];
        }
    }];
}
-(void)setupScrollView{
    _mainScrollView=[[UIScrollView alloc]initWithFrame:self.view.frame];
    [_mainScrollView setPagingEnabled:YES];
    [_mainScrollView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width*4, _mainScrollView.frame.size.height)];
    [_mainScrollView setScrollEnabled:YES];
    [_mainScrollView setAlpha:0.0f];
    [_mainScrollView setScrollEnabled:NO];
    [self.view addSubview:_mainScrollView];
    
    
    
}
-(void)setupLogoView{
    _logoImg=[UIImage imageNamed:@"ic_prelogin_logo"];
    _logoImgView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4-20, self.view.frame.size.height/7, self.view.frame.size.width/2+50, self.view.frame.size.height/7)];
    [_logoImgView setImage:_logoImg];
    [_logoImgView setContentMode:UIViewContentModeScaleAspectFit];
    
    
}
-(void)setupScrollViewFrames{
    _firstView=[[UIView alloc]initWithFrame:[self getPageFrame:0]];
    [_firstView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    _secondView=[[UIView alloc]initWithFrame:[self getPageFrame:1]];
    [_secondView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    _thirdView=[[UIView alloc]initWithFrame:[self getPageFrame:2]];
    [_thirdView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    _fourthView=[[UIView alloc]initWithFrame:[self getPageFrame:3]];
    [_fourthView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [self setupSecondViewFrames];
    
    #pragma mark //seting up firstView
    UIView *containerView=[[UIView alloc]initWithFrame:CGRectMake(15, (self.view.frame.size.height/3)*1.55, self.view.frame.size.width-30, 200)];
    [containerView setBackgroundColor:[UIColor clearColor]];

    SWFrameButton *loginButton=[[SWFrameButton alloc]initWithFrame:CGRectMake(35, 30, containerView.frame.size.width-70, 40)];
    [loginButton clipsToBounds];
    [loginButton setTitle:@"Login to your account" forState:UIControlStateNormal];
    loginButton.cornerRadius = 8.0f;
    loginButton.borderWidth = 1.3f;
    loginButton.tintColor = [UIColor whiteColor];
    [loginButton setTag:101];
    [loginButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    SWFrameButton *registerButton=[[SWFrameButton alloc]initWithFrame:CGRectMake(35, 100, containerView.frame.size.width-70, 40)];
    [registerButton clipsToBounds];
    [registerButton setTitle:@"Apply for an account" forState:UIControlStateNormal];
    registerButton.cornerRadius = 8.0f;
    registerButton.borderWidth = 1.3f;
    registerButton.tintColor = [UIColor whiteColor];
    [registerButton setTag:102];
    [registerButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [containerView addSubview:loginButton];
    [containerView addSubview:registerButton];
    
   // [_firstView addSubview:_logoImgView];
    [_firstView addSubview:containerView];
    
    
    [_mainScrollView addSubview:_firstView];
    [_mainScrollView addSubview:_secondView];
    [_mainScrollView addSubview:_thirdView];
    [_mainScrollView addSubview:_fourthView];
}
-(void)setupSecondViewFrames{
    int initialOriginY=50;
    //loginView Frame
    _loginView=[[UIView alloc]initWithFrame:_firstView.frame];
    [_loginView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(15, 70, 40, 40)];
    [backButton setBackgroundColor:[UIColor clearColor]];
    [backButton setImage:[UIImage imageNamed:@"ic_firstview_back"] forState:UIControlStateNormal];
    [backButton setAlpha:0.65f];
    [backButton setTag:301];
    [backButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
  //  [_secondView addSubview:backButton];
    
    
    
    UILabel *usernameLabelL=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+75, self.view.frame.size.width-30, 30)];
    [_loginView addSubview:[self decorateLabel:usernameLabelL withText:@"Username:"]];
    UILabel *passwordLabelL=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+150, self.view.frame.size.width-30, 30)];
    [_loginView addSubview:[self decorateLabel:passwordLabelL withText:@"Password:"]];
    
    _usernameTextFieldL=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+105, self.view.frame.size.width-30, 35)];
    [_loginView addSubview:[self decorateTextField:_usernameTextFieldL andIsPassword:NO]];
    _passwordTextFieldL=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+180, self.view.frame.size.width-30, 35)];
    [_loginView addSubview:[self decorateTextField:_passwordTextFieldL andIsPassword:YES]];
    
    SWFrameButton *loginButtonL=[[SWFrameButton alloc]initWithFrame:CGRectMake(60, initialOriginY+230, self.view.frame.size.width-120, 35)];
    [loginButtonL clipsToBounds];
    [loginButtonL setTitle:@"Login" forState:UIControlStateNormal];
    loginButtonL.cornerRadius = 8.0f;
    loginButtonL.borderWidth = 1.3f;
    loginButtonL.tintColor = [UIColor whiteColor];
    [loginButtonL setTag:201];
    [loginButtonL addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_loginView addSubview:loginButtonL];
    
    
    //back button

   
    
    [_secondView addSubview:_loginView];
    
    
    //registerView Frame
    _registerView=[[UIView alloc]initWithFrame:_firstView.frame];
    [_registerView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *firstNameLabelR=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+75, self.view.frame.size.width/2, 30)];
    [_registerView addSubview:[self decorateLabel:firstNameLabelR withText:@"Name:"]];
    UILabel *lastNameLabelR=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, initialOriginY+75, self.view.frame.size.width/2, 30)];
    [_registerView addSubview:[self decorateLabel:lastNameLabelR withText:@"Surname:"]];
    UILabel *usernameLabelR=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+150, self.view.frame.size.width, 30)];
    [_registerView addSubview:[self decorateLabel:usernameLabelR withText:@"Username:"]];
    UILabel *passwordLabelR=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+225, self.view.frame.size.width, 30)];
    [_registerView addSubview:[self decorateLabel:passwordLabelR withText:@"Password:"]];
    UILabel *phoneNumberLabelR=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+300, self.view.frame.size.width, 30)];
    [_registerView addSubview:[self decorateLabel:phoneNumberLabelR withText:@"Phone number:"]];
    UILabel *mailLabelR=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+375, self.view.frame.size.width, 30)];
    [_registerView addSubview:[self decorateLabel:mailLabelR withText:@"e-mail:"]];
    
    _firstNameTextFieldR=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+105, self.view.frame.size.width/2-30, 35)];
    [_registerView addSubview:[self decorateTextField:_firstNameTextFieldR andIsPassword:NO]];
    _lastNameTextFieldR=[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, initialOriginY+105, self.view.frame.size.width/2-15, 35)];
    [_registerView addSubview:[self decorateTextField:_lastNameTextFieldR andIsPassword:NO]];
    _usernameTextFieldR=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+180, self.view.frame.size.width-30, 35)];
    [_registerView addSubview:[self decorateTextField:_usernameTextFieldR andIsPassword:NO]];
    _passwordTextFieldR=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+180+75, self.view.frame.size.width-30, 35)];
    [_registerView addSubview:[self decorateTextField:_passwordTextFieldR andIsPassword:YES]];
    _countryCodeTextFieldR=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+180+75+75, self.view.frame.size.width/6, 35)];
    [_countryCodeTextFieldR setText:@"+389"];
    [_registerView addSubview:[self decorateTextField:_countryCodeTextFieldR andIsPassword:NO]];
    _phoneNumberTextFieldR=[[UITextField alloc]initWithFrame:CGRectMake(15+_countryCodeTextFieldR.frame.size.width+15, initialOriginY+180+75+75, self.view.frame.size.width-45-_countryCodeTextFieldR.frame.size.width, 35)];
    [_registerView addSubview:[self decorateTextField:_phoneNumberTextFieldR andIsPassword:NO]];
    _mailTextFieldR=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+180+75+75+75, self.view.frame.size.width-30, 35)];
    [_registerView addSubview:[self decorateTextField:_mailTextFieldR andIsPassword:NO]];
    
    SWFrameButton *registerButtonR=[[SWFrameButton alloc]initWithFrame:CGRectMake(60, initialOriginY+180+75+75+75+50, self.view.frame.size.width-120, 35)];
    [registerButtonR clipsToBounds];
    [registerButtonR setTitle:@"Register" forState:UIControlStateNormal];
    registerButtonR.cornerRadius = 8.0f;
    registerButtonR.borderWidth = 1.3f;
    registerButtonR.tintColor = [UIColor whiteColor];
    [registerButtonR setTag:202];
    [registerButtonR addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_registerView addSubview:registerButtonR];
   // [_registerView addSubview:backButton];
   
    [_secondView addSubview:_registerView];
    [_secondView addSubview:backButton];
    
    
    [_loginView setHidden:YES];
    [_registerView setHidden:YES];
    [self setupPhoneAuthenticatinView];
    
}
-(void)setupPhoneAuthenticatinView{
    int initialOriginY=50;
    UILabel *phoneCodeLabelT=[[UILabel alloc]initWithFrame:CGRectMake(15, initialOriginY+85, self.view.frame.size.width, 30)];
    [_thirdView addSubview:[self decorateLabel:phoneCodeLabelT withText:@"Verification code"]];
    _phoneCodeTextFieldT=[[UITextField alloc]initWithFrame:CGRectMake(15, initialOriginY+115, self.view.frame.size.width-30, 35)];
    [_thirdView addSubview:[self decorateTextField:_phoneCodeTextFieldT andIsPassword:NO]];
    SWFrameButton *phoneCodeAuthenticateButtonT=[[SWFrameButton alloc]initWithFrame:CGRectMake(60, initialOriginY+175, self.view.frame.size.width-120, 35)];
    [phoneCodeAuthenticateButtonT clipsToBounds];
    [phoneCodeAuthenticateButtonT setTitle:@"Authenticate" forState:UIControlStateNormal];
    phoneCodeAuthenticateButtonT.cornerRadius = 8.0f;
    phoneCodeAuthenticateButtonT.borderWidth = 1.3f;
    phoneCodeAuthenticateButtonT.tintColor = [UIColor whiteColor];
    [phoneCodeAuthenticateButtonT setTag:401];
    [phoneCodeAuthenticateButtonT addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_thirdView addSubview:phoneCodeAuthenticateButtonT];
}
-(void)setupPinPadWithType:(int)type{
    if(type==0){
    _pinPadView=[[PinPadView alloc]initWithFrame:self.view.frame sender:self type:type];
    if(_fourthView.subviews.count>0){
    for(UIView*subview in _fourthView.subviews){
        [subview removeFromSuperview];
    }
    }
    [_fourthView addSubview:_pinPadView];
    }else if(type==2){
        CGRect frame=CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, self.view.frame.size.height);
        _pinPadView=[[PinPadView alloc]initWithFrame:frame sender:self type:type];
        if(_fourthView.subviews.count>0){
            for(UIView*subview in _fourthView.subviews){
                [subview removeFromSuperview];
            }
        }
        [_pinPadView setAlpha:0.0f];
        [self.view addSubview:_pinPadView];
    }else if(type==1){
        //CGRect frame=CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, self.view.frame.size.height);
        _pinPadView=[[PinPadView alloc]initWithFrame:self.view.frame sender:self type:type];
        if(_fourthView.subviews.count>0){
            for(UIView*subview in _fourthView.subviews){
                [subview removeFromSuperview];
            }
        }
        [_pinPadView setAlpha:1.0f];
        [_fourthView addSubview:_pinPadView];
    }
}


#pragma mark - HELPERS
//helpers
-(CGRect)getPageFrame:(int)pageNumber{
    CGRect returnFrame=CGRectMake(self.view.frame.size.width*pageNumber, 0, self.view.frame.size.width, self.view.frame.size.height);
    return returnFrame;
}
-(UILabel*)decorateLabel:(UILabel*)label withText:(NSString*)title{
    [label setText:title];
    [label setTextColor:[UIColor colorWithWhite:1 alpha:1]];
    [label setFont:[UIFont systemFontOfSize:20]];
    return label;
}
-(UITextField*)decorateTextField:(UITextField*)textField andIsPassword:(BOOL)isPass{

    [textField setBackgroundColor:[UIColor clearColor]];
    [textField setBorderStyle:UITextBorderStyleRoundedRect];
    [textField setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.55]];
    [textField setDelegate:self];
    if(isPass){
        if (@available(iOS 10.0, *)) {
            if (@available(iOS 11.0, *)) {
                [textField setTextContentType:UITextContentTypePassword];
                textField.secureTextEntry = YES;
            } else {
               textField.secureTextEntry = YES;
            }
        } else {
             textField.secureTextEntry = YES;
        }
    }
    return textField;
}
-(void)scrollToPage:(int)page{
    CGRect frame = _mainScrollView.frame;
    frame.origin.x = frame.size.width*page;
    frame.origin.y = 0;
    [_mainScrollView scrollRectToVisible:frame animated:YES];
}
-(void)showHUD{
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"Utility" bundle:nil];
    _HUD=[sb instantiateViewControllerWithIdentifier:@"HUDViewController"];
    _HUD.sender=self;
    [_HUD.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_HUD setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:_HUD animated:NO completion:nil];
//    [NSTimer scheduledTimerWithTimeInterval:3.0
//                                                 target:self
//                                              selector:@selector(dismissHUD:)
//                                               userInfo:HUD
//                                                repeats:NO];
}


-(void)dismissHUDWithTag:(int)tag{
    //HUDViewController*HUD=[timer userInfo];
    [_HUD dismissWithTag:tag];
    
}
-(UIVisualEffectView*)setButtonBlurEffect:(UIView*)view{
    MLWBluuurView *visualEffectView;
    visualEffectView = [[MLWBluuurView alloc] init];
    visualEffectView.frame = view.bounds;
    [visualEffectView.layer setCornerRadius:view.frame.size.width/2];
    visualEffectView.clipsToBounds = YES;
    [visualEffectView setBlurRadius:10.0f];
   //[visualEffectView setAlpha:0.5f];
    return visualEffectView;
}
#pragma mark - DELEGATES
//delegates
-(void)buttonClick:(UIButton*)sender{
    switch (sender.tag) {
        case 101:
            [_loginView setHidden:NO];
            [_registerView setHidden:YES];
            [self upLogoPage2];
            [self scrollToPage:1];
            break;
        case 102:
            [_loginView setHidden:YES];
            [_registerView setHidden:NO];
            [self upLogoPage2];
            [self scrollToPage:1];
            break;
        case 201:
            [self loginUserStep1];
            //[self scrollToPage:2];
            break;
        case 202:
            [self enter];
            break;
        case 301:
            [self upLogoPage1];
            [self scrollToPage:0];
            break;
        case 401:
            [self phoneAuthStep2];
            [self showHUD];
            break;
        default:
            break;
    }
    
}
-(void)upLogoPage2{
    [UIView animateWithDuration:0.3f animations:^{
        
        [_markOverlayImgView setFrame:_logoFramePage2];
        
    } completion:^(BOOL finished) {
        
    }];
}
-(void)upLogoPage1{
    [UIView animateWithDuration:0.3f animations:^{
        
        [_markOverlayImgView setFrame:_logoFramePage1];
        
    } completion:^(BOOL finished) {
        
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
-(void)enter{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    RetailCustomerID *rcID=[[RetailCustomerID alloc]init];
    NSString*cID=[rcID generateRetailCustomerID];
    [rcID setID:cID];

    [picker dismissViewControllerAnimated:YES completion:NULL];
    UIImage *img=[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    _profileImageData =  UIImageJPEGRepresentation([self imageWithImage:[self croppIngimageByImage:img] convertToSize:CGSizeMake(img.size.width*0.1, img.size.height*0.1)], 0.5f);
    [self showHUD];
    [self phoneAuthStep1];
    

}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
- (UIImage *)croppIngimageByImage:(UIImage *)imageToCrop
{
    CGRect rect = CGRectMake(0,0, imageToCrop.size.height, imageToCrop.size.height);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef
                        scale:1
                        orientation:UIImageOrientationRight];
    CGImageRelease(imageRef);
    
    return cropped;
}

-(void)phoneAuthStep1{
    [FIRAuth auth].languageCode = @"en";
    NSString *phoneNumber;
    if([[_phoneNumberTextFieldR.text substringToIndex:1]isEqualToString:@"0"]){
        phoneNumber=[NSString stringWithFormat:@"%@%@",_countryCodeTextFieldR.text,[_phoneNumberTextFieldR.text substringFromIndex:1]];
    }else if([[_phoneNumberTextFieldR.text substringToIndex:1]isEqualToString:@"7"]){
        phoneNumber=[NSString stringWithFormat:@"%@%@",_countryCodeTextFieldR.text,_phoneNumberTextFieldR.text];
    }
    
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:phoneNumber
                                            UIDelegate:nil
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                if (error) {
                                                    NSLog(@"ERROR %@",error);
                                                    [self dismissHUDWithTag:120];
                                                    return;
                                                }else{
                                                    NSLog(@"SUCCESS %@",verificationID);
                                                    [self setupPinPadWithType:0];
                                                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                    [defaults setObject:verificationID forKey:@"authVerificationID"];
                                                    [self dismissHUDWithTag:120];
                                                    [self scrollToPage:2];
                                                }
                                            }];
}
-(void)phoneAuthStep2{
    [_phoneCodeTextFieldT resignFirstResponder];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *verificationID = [defaults stringForKey:@"authVerificationID"];
    FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
                                     credentialWithVerificationID:verificationID
                                     verificationCode:_phoneCodeTextFieldT.text];
    
    [[FIRAuth auth] signInWithCredential:credential
                              completion:^(FIRUser *user, NSError *error) {
                                  if (error) {
                                      NSLog(@"ERROR %@",error);
                                      return;
                                  }else{
                                      [self dismissHUDWithTag:110];
                                      [self scrollToPage:3];
                                      _userUID=user.uid;
                                     // [self completeRegistrationWithID:user.uid];
                                  }
                              }];
}
-(void)completeRegistrationWithID:(NSString*)ID{
    [self showHUD];
    _entry=[[EntryDetails alloc]init];
    [_entry setUsername:_usernameTextFieldR.text];
    [_entry setPassword:_passwordTextFieldR.text];
    _customer=[[RetailCustomer alloc]init];
    [_customer setCustomerID:ID];
    [_customer setName:_firstNameTextFieldR.text];
    [_customer setSurname:_lastNameTextFieldR.text];
    [_customer setRegisterdate:[NSDate date]];
    [_customer setEntryDetails:_entry];
    
    NSString *phoneNumber;
    if([[_phoneNumberTextFieldR.text substringToIndex:1]isEqualToString:@"0"]){
        phoneNumber=[NSString stringWithFormat:@"%@%@",_countryCodeTextFieldR.text,[_phoneNumberTextFieldR.text substringFromIndex:1]];
    }else if([[_phoneNumberTextFieldR.text substringToIndex:1]isEqualToString:@"7"]){
        phoneNumber=[NSString stringWithFormat:@"%@%@",_countryCodeTextFieldR.text,_phoneNumberTextFieldR.text];
    }
    
    [_customer setPhone:phoneNumber];
    [_customer setEmail:_mailTextFieldR.text];
    [_customer setDevice:[[[Device alloc]init]deviceName]];
    [_customer setToken:[FIRMessaging messaging].FCMToken];
    [_customer setPin:[KFKeychain loadObjectForKey:@"PIN"]];

        NSString*location=[NSString stringWithFormat:@"images/RetailCustomers/%@.jpg",ID];
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *storageRef = [storage reference];
        FIRStorageReference *riversRef = [storageRef child:location];
        [riversRef putData:_profileImageData
                                                     metadata:nil
                                                   completion:^(FIRStorageMetadata *metadata,
                                                                NSError *error) {
    
                                                       if (error != nil) {
                                                           NSLog(@"ERROR %@",error);
                                                       } else {
                                                           NSURL *downloadURL = metadata.downloadURL;
                                                           [self setUrl:downloadURL];
                                                           [_customer setImgURL:_url.absoluteString];
                                                           NSLog(@"URL: %@",downloadURL);
                                                           _registrationDictionary=[_customer dictionaryWithPropertiesOfObject:_customer];
                                                           __block FIRDocumentReference *ref = [[[FIRFirestore firestore] collectionWithPath:@"RetailCustomers"]documentWithPath:ID];
                                                           [ref setData:_registrationDictionary completion:^(NSError * _Nullable error) {
                                                               if (error != nil) {
                                                                   NSLog(@"Error adding document: %@", error);
                                                               } else {
                                                                   NSLog(@"Document added with ID: %@", ref.documentID);
                                                                   [KFKeychain saveObject:ref.documentID forKey:@"customerID"];
                                                               }
                                                           }];
                                                           [self dismissHUDWithTag:110];
                                                           NSLog(@"SUCCESS!!!");
                                                           [self getServerData];
                                                       }
                                                   }];
    
   
}

-(void)loginUserStep1{
    [self showHUD];
         FIRQuery *query=[[[FIRFirestore firestore] collectionWithPath:@"RetailCustomers"] queryWhereField:@"EntryDetails.Username" isEqualTo:_usernameTextFieldL.text];
            [query getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
            NSLog(@"QUERY RESULT: %@",snapshot.documents);
                if(snapshot.documents.count>0){
                    NSLog(@"user exists");
                    RetailCustomer *customer;
                    for(FIRDocumentSnapshot *_Nullable snapshotDocument in snapshot.documents){
                        NSLog(@"Query Result: %@ data: %@",snapshotDocument.documentID,snapshotDocument.data);
                        customer=[[RetailCustomer alloc] getCustomerFromDict:snapshotDocument.data];
                    }
                    NSLog(@"customer:%@",customer.Phone);
                    EntryDetails*entry=(EntryDetails*)customer.EntryDetails;
                    NSLog(@"customer.PASSWORD: %@",entry.Password);
                    if([customer.EntryDetails.Password isEqualToString:_passwordTextFieldL.text]){
                        [KFKeychain saveObject:customer.Pin forKey:@"PIN"];
                        [self phoneAuthStepLogin:customer.Phone];
                    }else{
                        NSLog(@"wrongPassword");
                        [self dismissHUDWithTag:110];
                    }
                }else{
                    NSLog(@"user does not exists");
                    [self dismissHUDWithTag:110];
                }
            
        }];
}
-(void)phoneAuthStepLogin:(NSString*)phoneNumber{
    [FIRAuth auth].languageCode = @"en";
    
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:phoneNumber
                                            UIDelegate:nil
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                if (error) {
                                                    NSLog(@"ERROR %@",error);
                                                    [self dismissHUDWithTag:120];//phoneauthlogin
                                                    return;
                                                }else{
                                                    NSLog(@"SUCCESS %@",verificationID);
                                                    [self setupPinPadWithType:1];
                                                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                    [defaults setObject:verificationID forKey:@"authVerificationID"];
                                                    [self dismissHUDWithTag:120];//phoneauthlogin
                                                    [self scrollToPage:2];
                                                }
                                            }];
}
-(void)didFinishDismissHUDWithTag:(int)tag{
    if(tag==130){
    [self presentNext];
    }
    
}


-(void)pinEntrySucceeded:(NSNotification*)notif{
    int type=[notif.userInfo[@"type"]intValue];
    
    if(type==0){
        [self completeRegistrationWithID:_userUID];}
    else if(type==1){//login
        [self getServerData];
    }else if(type==2){//enter
        [self getServerData];
    }
    
}
-(void)presentNext{
        UIStoryboard *sb=[UIStoryboard storyboardWithName:@"MainInterface" bundle:nil];
        KYDrawerController *vc=[sb instantiateViewControllerWithIdentifier:@"MainInterfaceViewController"];
        //MainInterfaceViewController *vc=[sb instantiateViewControllerWithIdentifier:@"MainInterfaceViewController"];
        [self presentViewController:vc animated:NO completion:nil];
}
-(void)getServerData{
    [self showHUD];
    [[GetServerData sharedInstance]initializeWithSender:self];
}
-(void)finalizeServerSingleton{
    [self dismissHUDWithTag:130];
    NSLog(@"FINISH");
}


@end
