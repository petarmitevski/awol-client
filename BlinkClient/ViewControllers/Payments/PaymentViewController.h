//
//  PaymentViewController.h
//  BlinkClient
//
//  Created by Petar on 1/28/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MLWBluuurView.h>

@interface PaymentViewController : UIViewController<UIAlertViewDelegate>
@property UIVisualEffectView *vibrancyEffectView;
@property MLWBluuurView *backgroundEffectView;
@property UIView*containerView;
@property NSString *paymentType;

@property UIImageView*corporateLogo;
@property UILabel*corporateLabel;
@property UILabel*amount;

@property UIButton *payButton;
@property UIButton *cancelButton;

@property UIView*cardView;
@property UIImageView *cardImage;
@property UILabel *cardName;
@property UILabel *cardType;
@property UILabel *cardNumber;


@property UIScrollView *scrollView;
@property UIView*scrollViewFrame;
@property NSArray*retailCards;


@property UIViewController *sender;

@property NSDictionary*userInfo;



@end
