//
//  PaymentViewController.m
//  BlinkClient
//
//  Created by Petar on 1/28/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "PaymentViewController.h"
#import "GetServerData.h"
#import "RetailCard.h"
#import "RetailCardFunds.h"
#import "Mapper+RetailCard.h"
#import "MainInterfaceViewController.h"
#import "MICardsView.h"
#import <Firebase/Firebase.h>

@interface PaymentViewController ()

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated{
    _containerView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height/5*2)];
    [_containerView setBackgroundColor:[UIColor clearColor]];
    
    _retailCards=[[GetServerData sharedInstance]getCustomerRetailCards];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = _containerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:blurEffectView];
    [self.view addSubview: [self setBackgroundBlurEffect:self.view]];
    [self.view addSubview:_containerView];
    [self animateBackground];
    [super viewWillAppear:YES];
    [self addElements];
}
-(void)animateBackground{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame=_containerView.frame;
                         frame.origin.y=self.view.frame.size.height/5*3;
                         [_containerView setFrame:frame];
                         [_backgroundEffectView setBlurRadius:5.0f];
                         // [self.view setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
                     }
                     completion:nil];
}
-(UIVisualEffectView*)setBackgroundBlurEffect:(UIView*)view{
    _backgroundEffectView = [[MLWBluuurView alloc] init];
    _backgroundEffectView.frame = view.bounds;
    _backgroundEffectView.clipsToBounds = YES;
    [_backgroundEffectView setBlurRadius:0.0f];
    return _backgroundEffectView;
}

-(void)addElements{
    NSLog(@"USER INFO !!! %@",_userInfo);
    
    UILabel *newPayment=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 30)];
    [newPayment setFont:[UIFont systemFontOfSize:24]];
    [newPayment setTextColor: [UIColor colorWithWhite:1.0f alpha:0.5f]];
    if([_paymentType isEqualToString:@"1"]){
        [newPayment setText:@"iBeacon Payment"];
    }else if([_paymentType isEqualToString:@"2"]){
        [newPayment setText:@"NFC Payment"];
    }else {
        [newPayment setText:@"Payment"];
    }
    _cancelButton =[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/5*4-10, 10, self.view.frame.size.width/5, 30)];
    [_cancelButton.layer setCornerRadius:5.0f];
    [_cancelButton setBackgroundColor: [UIColor colorWithWhite:1.0f alpha:0.5f]];
    [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_cancelButton addTarget:self action:@selector(cancelPayment) forControlEvents:UIControlEventTouchUpInside];
    
    [_containerView addSubview: newPayment];
    [_containerView addSubview:_cancelButton];
    
    UIView *dashline1=[[UIView alloc]initWithFrame:CGRectMake(0, 45, self.view.frame.size.width, 1)];
    [dashline1 setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.5f]];
    [_containerView addSubview:dashline1];
    
    UIView *logoContainer=[[UIView alloc]initWithFrame:CGRectMake(30, 65, self.view.frame.size.width/6, self.view.frame.size.width/6)];
    [logoContainer.layer setCornerRadius:7.5f];
    [logoContainer setClipsToBounds:YES];
    [logoContainer setBackgroundColor:[UIColor redColor]];
    
    _corporateLogo=[[UIImageView alloc]initWithFrame:logoContainer.bounds];
    [logoContainer addSubview:_corporateLogo];
    [_containerView addSubview:logoContainer];
    
    _corporateLabel=[[UILabel alloc]initWithFrame:CGRectMake(30+30+self.view.frame.size.width/6, 65, self.view.frame.size.width-70-self.view.frame.size.width/6, 30)];
    [_corporateLabel setTextColor:[UIColor colorWithWhite:1.0 alpha:0.5f]];
    [_corporateLabel setText:@"Telekom MK Skopje Capitol Mall"];
    
    _amount=[[UILabel alloc]initWithFrame:CGRectMake(30+30+self.view.frame.size.width/6, 95, self.view.frame.size.width-70-self.view.frame.size.width/6, 30)];
    [_amount setFont:[UIFont systemFontOfSize:24]];
    [_amount setTextColor:[UIColor colorWithWhite:1.0 alpha:0.5f]];
    [_amount setTextAlignment:NSTextAlignmentRight];
    [_amount setText:@"450.0 Ден"];
    
    [_containerView addSubview:_corporateLabel];
    [_containerView addSubview:_amount];
    
    UIView *dashline2=[[UIView alloc]initWithFrame:CGRectMake(0, 65+self.view.frame.size.width/6+15, self.view.frame.size.width, 1)];
    [dashline2 setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.5f]];
    [_containerView addSubview:dashline2];
    
    int cardViewHeight=dashline2.frame.origin.y-dashline1.frame.origin.y;
    
    UIView *dashline3=[[UIView alloc]initWithFrame:CGRectMake(0, dashline2.frame.origin.y+cardViewHeight, self.view.frame.size.width, 1)];
    [dashline3 setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.5f]];
    [_containerView addSubview:dashline3];
    
    UIView *cardContainerView=[[UIView alloc]initWithFrame:CGRectMake(0, dashline2.frame.origin.y+1, self.view.frame.size.width, cardViewHeight)];
    
    NSLog(@"payment self %@ ",self);
    NSLog(@"payment type %@",_paymentType);
    MainInterfaceViewController*main=(MainInterfaceViewController*)_sender;
    MICardsView* cardView=main.cardsView;
    NSLog(@"MICardsView 2%@ and main: %@ sender: %@",cardView,main,_sender);
    NSLog(@"index %ld",(long)cardView.currentIndex);
    if([_paymentType isEqualToString:@"2"]){
        
        
        MainInterfaceViewController*main=(MainInterfaceViewController*)_sender;
        NSInteger index=[main.cardsView getCardIndex];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        RetailCard*card=[_retailCards objectAtIndex:index];
        
        [dict setObject:[card BankIssuer] forKey:@"BankIssuer"];
        [dict setObject:[card CardNumber] forKey:@"CardNumber"];
        [dict setObject:[card CardType] forKey:@"CardType"];
        [dict setObject:[card InterbankNetwork] forKey:@"InterbankNetwork"];
        [dict setObject:[card InterType] forKey:@"InterType"];
        [cardContainerView addSubview:[self getSingleCardView:dict height:cardViewHeight andPosition:0]];
        
        FIRFirestore *firestore=[FIRFirestore firestore];
        [[[firestore collectionWithPath:@"PCTransactions"] queryWhereField:@"Status" isEqualTo:@"Pending"]
         addSnapshotListener:^(FIRQuerySnapshot *snapshot, NSError *error) {
             if (snapshot == nil) {
                 NSLog(@"Error fetching documents: %@", error);
                 return;
             }
             //NSMutableArray *cities = [NSMutableArray array];
             for (FIRDocumentSnapshot *document in snapshot.documents) {
                 
                 [_corporateLabel setText:@"Ramstore MK POS-ID:101/102"];
                 [_corporateLogo setImage:[UIImage imageNamed:@"ramstoreLogo"]];
                 [_amount setText:[NSString stringWithFormat:@"%@ Ден",[document.data objectForKey:@"Amount"]]];
                 [self nfcPayment:document.data andCard:card andDocumentID:document.documentID];
                 
                 
             }
             // NSLog(@"Current cities in CA: %@", cities);
         }];
        
        
    }else if([_paymentType isEqualToString:@"1"]){
        //dodaj observer
        if(_userInfo){
            FIRFirestore *defaultFirestore=[FIRFirestore firestore];
            FIRDocumentReference *docRef = [[defaultFirestore collectionWithPath:@"PCTransactions"] documentWithPath:[_userInfo objectForKey:@"gcm.notification.custObject"]];
            [docRef getDocumentWithCompletion:^(FIRDocumentSnapshot *snapshot, NSError *error) {
                if (snapshot != nil) {
                    NSLog(@"Document data: %@", snapshot.data);
                    [_payButton setHidden:NO];
                } else {
                    NSLog(@"Document does not exist");
                }
            }];
        }
        int noOfPages=_retailCards.count;
        _scrollView=[[UIScrollView alloc]initWithFrame:cardContainerView.bounds];
        [_scrollView setPagingEnabled:YES];
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width*noOfPages, _scrollView.frame.size.height)];
        [_scrollView setScrollEnabled:YES];
        
        
        [_amount setText:[NSString stringWithFormat:@"%@ Ден",[_userInfo objectForKey:@"gcm.notification.amount"]]];
        [_corporateLabel setText:[NSString stringWithFormat:@"%@",[_userInfo objectForKey:@"gcm.notification.merchantName"]]];
        [_corporateLogo setImage:[UIImage imageNamed:@"ramstoreLogo"]];
        for(int i=0;i<noOfPages;i++){
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            RetailCard*card=[_retailCards objectAtIndex:i];
            
            [dict setObject:[card BankIssuer] forKey:@"BankIssuer"];
            [dict setObject:[card CardNumber] forKey:@"CardNumber"];
            [dict setObject:[card CardType] forKey:@"CardType"];
            [dict setObject:[card InterbankNetwork] forKey:@"InterbankNetwork"];
            [dict setObject:[card InterType] forKey:@"InterType"];
            
            [_scrollView addSubview:[self getSingleCardView:dict height:cardViewHeight andPosition:i]];
            
        }
        [cardContainerView addSubview:_scrollView];
    }
    [_containerView addSubview:cardContainerView];
    _payButton=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-self.view.frame.size.width/8, cardContainerView.frame.origin.y+cardContainerView.frame.size.height+20, self.view.frame.size.width/4, 40)];
    [_payButton.layer setCornerRadius:5.0f];
    [_payButton setBackgroundColor: [UIColor colorWithWhite:1.0f alpha:0.5f]];
    [_payButton setTitle:@"Pay" forState:UIControlStateNormal];
    [_payButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_payButton addTarget:self action:@selector(sendPayment) forControlEvents:UIControlEventTouchUpInside];
    [_payButton setHidden:YES];
    [_containerView addSubview:_payButton];
    
  
    
}
-(UIView*)getSingleCardView:(nullable NSDictionary*)dict height:(int)height andPosition:(int)position{
    
    /*
     [dict setObject:[card BankIssuer] forKey:@"BankIssuer"];
     [dict setObject:[card CardNumber] forKey:@"CardNumber"];
     [dict setObject:[card CardType] forKey:@"CardType"];
     [dict setObject:[card InterbankNetwork] forKey:@"InterbankNetwork"];
     [dict setObject:[card InterType] forKey:@"InterType"];
     */
    UIView*containerView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*position, 0, self.view.frame.size.width, height)];
    //[containerView setBackgroundColor:[UIColor greenColor]];
    UIView *cardContainerView=[[UIView alloc]initWithFrame:CGRectMake(20, 20, self.view.frame.size.width/6*1.55, self.view.frame.size.width/6)];
    [cardContainerView.layer setCornerRadius:5.5f];
    [cardContainerView setClipsToBounds:YES];
    // [cardContainerView setBackgroundColor:[UIColor redColor]];
    
    NSString*imgName=[Mapper_RetailCard bankCardForInterType:[dict objectForKey:@"InterType"]];
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:cardContainerView.bounds];
    [imgView setImage:[UIImage imageNamed:imgName]];
    [cardContainerView addSubview:imgView];
    
    UILabel *bankName=[[UILabel alloc]initWithFrame:CGRectMake(40+cardContainerView.frame.size.width, cardContainerView.frame.origin.y, self.view.frame.size.width-10-40-cardContainerView.frame.size.width, 30)];
    [bankName setTextColor:[UIColor colorWithWhite:1.0 alpha:0.5f]];
    [bankName setTextAlignment:NSTextAlignmentRight];
    [bankName setText:[dict objectForKey:@"BankIssuer"]];
    
    UILabel *cardType=[[UILabel alloc]initWithFrame:CGRectMake(40+cardContainerView.frame.size.width, cardContainerView.frame.origin.y+30, self.view.frame.size.width-10-40-cardContainerView.frame.size.width, 30)];
    [cardType setTextColor:[UIColor colorWithWhite:1.0 alpha:0.5f]];
    [cardType setTextAlignment:NSTextAlignmentLeft];
    [cardType setText:[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"InterbankNetwork"],[dict objectForKey:@"CardType"]]];
    
    UILabel *cardNumber=[[UILabel alloc]initWithFrame:CGRectMake(40+cardContainerView.frame.size.width, cardContainerView.frame.origin.y+30, self.view.frame.size.width-10-40-cardContainerView.frame.size.width, 30)];
    [cardNumber setTextColor:[UIColor colorWithWhite:1.0 alpha:0.5f]];
    [cardNumber setTextAlignment:NSTextAlignmentRight];
    [cardNumber setText:[NSString stringWithFormat:@"•••• %@", [[dict objectForKey:@"CardNumber"]substringFromIndex:12]]];
    //••••
    [containerView addSubview:cardContainerView];
    [containerView addSubview:bankName];
    [containerView addSubview:cardType];
    [containerView addSubview:cardNumber];
    
    
    return containerView;
}
-(void)cancelPayment{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)sendPayment{
    int page = _scrollView.contentOffset.x / _scrollView.frame.size.width;
    RetailCard*card=[_retailCards objectAtIndex:page];
    RetailCardFunds*funds=[card CardFunds];
    FIRFirestore* firestore=[FIRFirestore firestore];
    FIRDocumentReference *docRef =
    [[firestore collectionWithPath:@"PCTransactions"] documentWithPath:[_userInfo objectForKey:@"gcm.notification.custObject"]];
    if(funds.AvailableFunds>=[[_userInfo objectForKey:@"gcm.notification.amount"] doubleValue]){
        double newAmount=funds.AvailableFunds-[[_userInfo objectForKey:@"gcm.notification.amount"] doubleValue];
        
        FIRDocumentReference *docRefCard =
        [[firestore collectionWithPath:@"RetailCards"] documentWithPath:[card CardID]];
        // Set the "capital" field of the city
        [docRefCard updateData:@{
                                 @"CardFunds.AvailableFunds": [NSNumber numberWithDouble:newAmount]
                                 } completion:^(NSError * _Nullable error) {
                                     if (error != nil) {
                                         NSLog(@"Error updating document: %@", error);
                                     } else {
                                         NSLog(@"Document successfully updated");
                                         
                                         [docRef updateData:@{
                                                              @"Status": @"Success",
                                                              @"RetailCardID": card.CardID
                                                              } completion:^(NSError * _Nullable error) {
                                                                  if (error != nil) {
                                                                      NSLog(@"Error updating document: %@", error);
                                                                  } else {
                                                                      
                                                                  }
                                                              }];
                                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Transaction is successfuly executed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                         [alert show];
//                                         UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Success"
//                                                                                                        message:@"Transaction is successfuly executed"
//                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
//
//                                         UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                                                               handler:^(UIAlertAction * action) {}];
//
//                                         [alert addAction:defaultAction];
//                                         [self presentViewController:alert animated:YES completion:nil];
//
                                     }
                                 }];
    }else{
        [docRef updateData:@{
                             @"Status": @"Fail"
                             } completion:^(NSError * _Nullable error) {
                                 if (error != nil) {
                                     NSLog(@"Error updating document: %@", error);
                                 } else {
                                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Failed" message:@"Transaction fails, reason: (Not enough funds on chosen card)" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                     [alert show];
//                                     UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Failed"
//                                                                                                    message:@"Transaction fails, reason: (Not enough funds on chosen card)"
//                                                                                             preferredStyle:UIAlertControllerStyleAlert];
//
//                                     UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                                                           handler:^(UIAlertAction * action) {}];
//
//                                     [alert addAction:defaultAction];
//                                     [self presentViewController:alert animated:YES completion:nil];
                                     
                                 }
                             }];
        
    }
    
    
    
    
}
-(void)nfcPayment:(NSDictionary*)document andCard:(RetailCard*)card andDocumentID:(NSString*)documentID{
    //int page = _scrollView.contentOffset.x / _scrollView.frame.size.width;
    //RetailCard*card=[_retailCards objectAtIndex:page];
    RetailCardFunds*funds=[card CardFunds];
    FIRFirestore* firestore=[FIRFirestore firestore];
    FIRDocumentReference *docRef =
    [[firestore collectionWithPath:@"PCTransactions"] documentWithPath:documentID];
    
    if(funds.AvailableFunds>=[[document objectForKey:@"Amount"] doubleValue]){
        double newAmount=funds.AvailableFunds-[[document objectForKey:@"Amount"] doubleValue];
        
        FIRDocumentReference *docRefCard =
        [[firestore collectionWithPath:@"RetailCards"] documentWithPath:[card CardID]];
        // Set the "capital" field of the city
        [docRefCard updateData:@{
                                 @"CardFunds.AvailableFunds": [NSNumber numberWithDouble:newAmount]
                                 } completion:^(NSError * _Nullable error) {
                                     if (error != nil) {
                                         NSLog(@"Error updating document: %@", error);
                                     } else {
                                         NSLog(@"Document successfully updated");
                                         
                                         [docRef updateData:@{
                                                              @"Status": @"Success",
                                                              @"RetailCardID": card.CardID
                                                              } completion:^(NSError * _Nullable error) {
                                                                  if (error != nil) {
                                                                      NSLog(@"Error updating document: %@", error);
                                                                  } else {
                                                                      
                                                                  }
                                                              }];
                                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Transaction is successfuly executed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                         [alert show];
                                         //                                         UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Success"
                                         //                                                                                                        message:@"Transaction is successfuly executed"
                                         //                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                         //
                                         //                                         UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                         //                                                                                               handler:^(UIAlertAction * action) {}];
                                         //
                                         //                                         [alert addAction:defaultAction];
                                         //                                         [self presentViewController:alert animated:YES completion:nil];
                                         //
                                     }
                                 }];
    }else{
        [docRef updateData:@{
                             @"Status": @"Fail"
                             } completion:^(NSError * _Nullable error) {
                                 if (error != nil) {
                                     NSLog(@"Error updating document: %@", error);
                                 } else {
                                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Failed" message:@"Transaction fails, reason: (Not enough funds on chosen card)" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                     [alert show];
                                     //                                     UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Failed"
                                     //                                                                                                    message:@"Transaction fails, reason: (Not enough funds on chosen card)"
                                     //                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                     //
                                     //                                     UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                     //                                                                                           handler:^(UIAlertAction * action) {}];
                                     //
                                     //                                     [alert addAction:defaultAction];
                                     //                                     [self presentViewController:alert animated:YES completion:nil];
                                     
                                 }
                             }];
        
    }
    
    
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
}
@end
