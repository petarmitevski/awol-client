//
//  MainInterfaceViewController.h
//  BlinkClient
//
//  Created by Petar on 12/12/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLWBluuurView.h"
#import "MICardsView.h"
#import "MITransactionsView.h"
#import "MIContactsView.h"
#import "MILoyaltyCardsView.h"
#define ApplicationDidTimeoutNotification @"ApplicationDidTimeout"

@interface MainInterfaceViewController : UIViewController <CAAnimationDelegate>

@property (nonatomic, strong) UIImageView *markImgView;
@property (nonatomic, strong) UIImageView *markGradientOverlay;
@property (nonatomic, strong) UILabel *markLabel;
@property (nonatomic, strong) UIView *markGradientView;
@property (nonatomic, strong) UIImage *markImg;

@property UIScrollView* mainScrollView;
            @property UIView *MSVRetailCards;
            @property UIView *MSVTransactions;
            @property UIView *MSVContacts;
            @property UIView *MSVLoyaltyCards;
            @property UIView *MSVMore;


@property (nonatomic, strong) UIView *gradientLayerSurface;
@property CAGradientLayer *gradientLayer;
@property NSArray*currentColors;
@property NSArray*nextColors;



@property NSMutableArray*MIViewsArray;
@property MICardsView*cardsView;
@property MITransactionsView* transactionsView;
@property MIContactsView* contactsView;
@property MILoyaltyCardsView *loyaltyCardsView;



@property UIView *tabBarView;
@property UIView *tabbarDashline;
@property NSMutableArray *arrayOfTabbarButtons;

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
@end
