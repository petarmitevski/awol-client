//
//  CardsViewCell.m
//  BlinkClient
//
//  Created by Petar on 1/2/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "CardsViewCell.h"

@implementation CardsViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    self.bounds = CGRectMake(0,
                             0,
                             self.superview.frame.size.width,
                             self.bounds.size.height);
    
    [super layoutSubviews];
}


@end
