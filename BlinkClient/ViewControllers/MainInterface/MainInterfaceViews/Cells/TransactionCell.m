//
//  TransactionCell.m
//  BlinkClient
//
//  Created by Petar on 1/2/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "TransactionCell.h"
#import <QuartzCore/QuartzCore.h>
#import "CurrencyFormater.h"

@implementation TransactionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
(NSString *)reuseIdentifier withSize:(CGSize)size;

{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        //[view setBackgroundColor:[UIColor redColor]];
        UIView *content=[[UIView alloc]initWithFrame:CGRectMake(50, 0, view.bounds.size.width-65, size.height-35)];
        [content.layer setCornerRadius:10.0f];
        [content setBackgroundColor:UIColorFromRGBWithAlpha(0x443e6d, 0.75f)];
        content.layer.shadowRadius  = 5.0f;
        content.layer.shadowOffset  = CGSizeMake(5.0f, 5.0f);
        content.layer.shadowOpacity = 0.1f;
        content.layer.masksToBounds = NO;
        
        UIView*imgViewContainer=[[UIImageView alloc]initWithFrame:CGRectMake(content.frame.origin.x-25, 15, 50, 50)];
        [imgViewContainer.layer setCornerRadius:5.0f];
        //[imgViewContainer setBackgroundColor:[UIColor blueColor]];
        [imgViewContainer setClipsToBounds:YES];
        _imgView=[[UIImageView alloc]initWithFrame:imgViewContainer.bounds];
        [_imgView setClipsToBounds:YES];
        [_imgView setContentMode:UIViewContentModeScaleToFill];
        [imgViewContainer addSubview:_imgView];
        _lblTransName=[[UILabel alloc]initWithFrame:CGRectMake(30, 0, content.frame.size.width-90,content.frame.size.height)];
        [_lblTransName setTextColor:[UIColor whiteColor]];
        [_lblTransName setTextAlignment:NSTextAlignmentLeft];
        
        _amount=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, content.frame.size.width-5, content.frame.size.height)];
        [_amount setTextAlignment:NSTextAlignmentRight];
        [_amount setTextColor:[UIColor whiteColor]];
        
        _date=[[UILabel alloc]initWithFrame:CGRectMake(0, content.frame.size.height-12, content.frame.size.width-5, 10)];
        [_date setTextAlignment:NSTextAlignmentRight];
        [_date setTextColor:[UIColor colorWithWhite:1.0f alpha:0.5f]];
        [_date setFont:[UIFont systemFontOfSize:10]];
        
        [content addSubview:_lblTransName];
        [content addSubview:_amount];
        [content addSubview:_date];
        
        [self.contentView addSubview:view];
        [view addSubview:content];
        [view addSubview:imgViewContainer];
    }
    return self;
}

@end
