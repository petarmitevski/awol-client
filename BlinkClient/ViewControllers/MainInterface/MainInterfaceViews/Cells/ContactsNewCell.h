//
//  ContactsNewCell.h
//  BlinkClient
//
//  Created by Petar on 1/4/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsNewCell : UICollectionViewCell
@property UIView*imgViewContainer;
@property UIImageView*imgView;
@property UIActivityIndicatorView*spinner;
@property UILabel*lblName;
@end
