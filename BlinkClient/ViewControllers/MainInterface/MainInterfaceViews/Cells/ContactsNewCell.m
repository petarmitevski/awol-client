//
//  ContactsNewCell.m
//  BlinkClient
//
//  Created by Petar on 1/4/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "ContactsNewCell.h"

@implementation ContactsNewCell


-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    //[self setBackgroundColor:[UIColor redColor]];
    _imgViewContainer=[[UIView alloc]initWithFrame:CGRectMake(20, 5, frame.size.width-40, frame.size.width-40)];
    [_imgViewContainer.layer setCornerRadius:_imgViewContainer.frame.size.width/2];
    [_imgViewContainer setBackgroundColor:[UIColor clearColor]];
    [_imgViewContainer setClipsToBounds:YES];
    _imgView=[[UIImageView alloc]initWithFrame:_imgViewContainer.bounds];
    [_imgView setContentMode:UIViewContentModeScaleToFill];
    [_imgView setBackgroundColor:[UIColor clearColor]];
    [_imgView setClipsToBounds:YES];
    _lblName=[[UILabel alloc]initWithFrame:CGRectMake(5, 10+_imgViewContainer.frame.size.height, self.frame.size.width-10, 20)];
    [_lblName setFont:[UIFont systemFontOfSize:12]];
    [_lblName setTextColor:[UIColor whiteColor]];
    [_lblName setTextAlignment:NSTextAlignmentCenter];
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [_spinner setFrame:self.bounds];
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    NSLog(@"fatalError(init(coder:) has not been implemented)");
    return self;
}
-(void)layoutSubviews{
    [self addSubview:_imgViewContainer];
    [_imgViewContainer addSubview:_imgView];
    [self addSubview:_spinner];
    [self addSubview:_lblName];
    [super layoutSubviews];
}
@end
