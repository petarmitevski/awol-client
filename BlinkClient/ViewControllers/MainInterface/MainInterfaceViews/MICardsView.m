//
//  MICardsView.m
//  BlinkClient
//
//  Created by Petar on 12/24/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "MICardsView.h"
#import "RetailCard.h"
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import <Firebase/Firebase.h>
#import "GetServerData.h"
#import "BlinkClient-Swift.h"
#import "CurrencyFormater.h"
#import "CardsViewCell.h"
#import "TransactionCell.h"
#import "Mapper+RetailCard.h"

@interface MICardsView () <UITableViewDataSource,UITableViewDelegate,FSPagerViewDataSource,FSPagerViewDelegate, NFCNDEFReaderSessionDelegate>

@property (strong, nonatomic) NSArray<NSString *> *imageNames;
@property (assign, nonatomic) NSInteger typeIndex;
@property  FSPagerView *pagerView;


@end

@implementation MICardsView

-(void)reloadData{
    _cardsArray=[[GetServerData sharedInstance]getCustomerRetailCards];
    [self.pagerView reloadData];
    _pagerView.alpha=1.0f;
}

-(void)drawRect:(CGRect)rect {
}

- (id)initWithFrame:(CGRect)rect{
    self = [super initWithFrame:rect];
    if (self) {
//        _counter=0;
//        _numberOfPinEnters=0;
//        _mutPin=[[NSMutableString alloc]init];
        [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
//        NSLog(@"sender: %@",sender);
//        _sender=sender;


    }
     _cardsArray=[[GetServerData sharedInstance]getCustomerRetailCards];
    _pagerView=[[FSPagerView alloc]initWithFrame:CGRectMake(rect.origin.x, 0, rect.size.width, 300)];
    [self.pagerView registerClass:[FSPagerViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.pagerView.isInfinite = NO;
    self.typeIndex = 0;
    _pagerView.alpha=1.0f;
    _pagerView.delegate=self;
    _pagerView.dataSource=self;
    _currentIndex=0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"newPayment" object:nil];
    
    
    
    //[session beginSession];
    //[_nfcSession beginSession];
    UIView *tableview=[[UIView alloc]initWithFrame:CGRectMake(0, _pagerView.frame.size.height-40, rect.size.width, rect.size.height-_pagerView.frame.size.height)];
    [tableview setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [self addSubview:tableview];
    _tableView=[[UITableView alloc]initWithFrame:tableview.bounds];
    [_tableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    _tableView.dataSource=self;
    _tableView.delegate=self;
    _tableView.separatorColor=[UIColor colorWithWhite:1.0f alpha:0.0f];
    [tableview addSubview:_tableView];
    
    
    
    _scrollView=[[UIScrollView alloc]initWithFrame:rect];
    [self addSubview:_pagerView];
    
    _emptyCardsView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    UILabel *lonely=[[UILabel alloc]initWithFrame:CGRectMake(0, 200, self.frame.size.width, 200)];
    [lonely setText:@"It's so lonely here"];
    [lonely setTextAlignment:NSTextAlignmentCenter];
    [lonely setTextColor:[UIColor colorWithWhite:1.0f alpha:0.75f]];
    [lonely setFont:[UIFont systemFontOfSize:30]];
    
    UIImageView* sadSmiley=[[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width/2-self.frame.size.width/4/2, 325, self.frame.size.width/4, self.frame.size.width/4)];
    [sadSmiley setImage:[UIImage imageNamed:@"sadSmiley"]];
    [sadSmiley setAlpha:0.75f];
    
    
    UIView *cardView=[[UIView alloc]initWithFrame:CGRectMake(30, 50, self.frame.size.width-60, (self.frame.size.width-60)*0.6)];
    [cardView setBackgroundColor:[UIColor clearColor]];
    [cardView.layer setCornerRadius:10.0f];
    [cardView setClipsToBounds:YES];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurView=[[UIVisualEffectView alloc]initWithEffect:blurEffect];
    blurView.frame = cardView.bounds;
    blurView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [blurView setClipsToBounds:YES];
    [cardView addSubview:blurView];
    
    UIButton*plusInBox=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, cardView.frame.size.width/5, cardView.frame.size.width/5)];
    [plusInBox setImage:[UIImage imageNamed:@"plusBox"] forState:UIControlStateNormal];
    plusInBox.center=CGPointMake(CGRectGetMidX(cardView.bounds), CGRectGetMidY(cardView.bounds));
    [cardView addSubview:plusInBox];

    [_emptyCardsView addSubview:lonely];
    [_emptyCardsView addSubview:sadSmiley];
    [_emptyCardsView addSubview:cardView];
    [self addSubview:_emptyCardsView];
    
    if(_cardsArray.count==0){
        [_tableView setHidden:YES];
        [_pagerView setHidden:YES];
        [_emptyCardsView setHidden:NO];
    }else{
        [_tableView setHidden:NO];
        [_pagerView setHidden:NO];
        [_emptyCardsView setHidden:YES];
    }
    
    return self;
}
-(void)receiveNotification:(NSNotification*)notif{
    [self presentPayment:@"1" userInfo:notif.userInfo];
}
- (NSInteger)numberOfItemsInPagerView:(FSPagerView *)pagerView
{
    return _cardsArray.count;
    
}

- (FSPagerViewCell *)pagerView:(FSPagerView *)pagerView cellForItemAtIndex:(NSInteger)index
{
    FSPagerViewCell * cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"cell" atIndex:index];
    if(_cardsArray.count>0){
    RetailCard*currentCard=[_cardsArray objectAtIndex:index];
    NSString*InterType=[currentCard InterType];
    NSString*imgName=[Mapper_RetailCard bankCardForInterType:InterType];
    NSString*status=[currentCard Status];
    if([status isEqualToString:@"active"]){
        cell.imageView.image = [UIImage imageNamed:imgName];}
    else{
        cell.imageView.image = [self grayscaleImage:[UIImage imageNamed:imgName]];
    }
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    cell.imageView.clipsToBounds = YES;
    }
    return cell;
    
}

#pragma mark - FSPagerViewDelegate

- (void)pagerView:(FSPagerView *)pagerView didSelectItemAtIndex:(NSInteger)index
{
    [pagerView deselectItemAtIndex:index animated:YES];
    [pagerView scrollToItemAtIndex:index animated:YES];
    
}

-(void)pagerViewDidScroll:(FSPagerView *)pagerView{
    
    if(!(pagerView.currentIndex==_currentIndex)){
        _currentIndex=pagerView.currentIndex;
        NSLog(@"index: %ld",(long)pagerView.currentIndex);
        NSLog(@"MICardsView 1%@",self);
       // NSLog(@"self %@",self);
//        [UIView animateWithDuration:0.5
//                              delay:0
//                         animations:^{
//                             [_tableView setAlpha:0.0f];
//                         }
//          ];
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut  animations:^{
            [_tableView setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [_tableView reloadData];
            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut  animations:^{
                [_tableView setAlpha:1.0f];
            } completion:nil];
        }];
        
    }
    
}
#pragma mark - Private properties

- (void)setTypeIndex:(NSInteger)typeIndex
{
    FSPagerViewTransformerType type;
    type=FSPagerViewTransformerTypeLinear;
    self.pagerView.transformer = [[FSPagerViewTransformer alloc] initWithType:type];
    CGAffineTransform transform = CGAffineTransformMakeScale(0.85, 0.68);
    self.pagerView.itemSize = CGSizeApplyAffineTransform(self.pagerView.frame.size, transform);
    
    
}

-(void)setupScrollView:(NSArray*)cardsArray{
 //   NSArray*cardsArray=[RetailCard getRetailCards];
    int cardsCount=cardsArray.count;
    [_scrollView setPagingEnabled:YES];
    [_scrollView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width*cardsCount-100, _scrollView.frame.size.height)];
    [_scrollView setScrollEnabled:YES];
    [_scrollView setAlpha:1.0f];
    [_scrollView setScrollEnabled:YES];
  //  [self addSubview:_scrollView];
    int pageNumber=0;
    for(RetailCard* card in cardsArray){
        [self setSingleCardView:card onPage:pageNumber];
        pageNumber++;
    }

    
}
-(CGRect)getPageFrame:(int)pageNumber{
    CGRect returnFrame=CGRectMake(self.frame.size.width*pageNumber, 0, self.frame.size.width, self.frame.size.height);
    return returnFrame;
}


-(void)setSingleCardView:(RetailCard*)cardData onPage:(int)pageNumber{
    UIImageView *cardBg=[[UIImageView alloc]initWithFrame:CGRectMake(self.frame.origin.x+10+self.frame.size.width*pageNumber, 10, self.frame.size.width-20, self.frame.size.height-20)];
    [cardBg setImage:[UIImage imageNamed:@"cc-1"]];
    [cardBg.layer setCornerRadius:10.0f];
    cardBg.clipsToBounds=YES;
    [cardBg setContentMode:UIViewContentModeScaleAspectFit];
    [_scrollView addSubview:cardBg];
    
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath { 
    CardsViewCell*cell=[[CardsViewCell alloc]init];
    cell.layoutMargins=UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins=NO;
    [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    switch (indexPath.row) {
        case 0:
            cell=[self cellNo1:cell];
            break;
        case 1:
            cell=[self cellNo2:cell];
            break;
        case 2:
            cell=[self cellNo3:cell];
            break;
        case 3:
            cell=[self cellNo4:cell];
            break;
        default:
            break;
    }

    return cell;
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section { 
    return 4;
}
-(double)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    switch (indexPath.row) {
        case 0:
            return 20.0f;
            break;
        case 1:
            return 115.0f;
            break;
        case 2:
            return 130.0f;
            break;
        case 3:
            return 100.0f;
            break;
        default:
            return 130.0f;
            break;
    }

}
-(CardsViewCell*)cellNo1:(CardsViewCell*)cell{//_currentIndex
    [cell.contentView setFrame:CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]])];
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(25, 0, cell.contentView.bounds.size.width-50, cell.contentView.bounds.size.height)];
    
    if(_cardsArray.count>0){
    RetailCard*currentCard=[_cardsArray objectAtIndex:_currentIndex];
    NSString*status=[currentCard Status];
    UILabel *lblCardReady=[[UILabel alloc]initWithFrame:view.bounds];
    if([status isEqualToString:@"active"]){
        [lblCardReady setText:@"Card is ready"];}
    else {
        [lblCardReady setText:@"Card is blocked"];
    }
    [lblCardReady setFont:[UIFont systemFontOfSize:12.0f]];
    
    [lblCardReady setTextAlignment:NSTextAlignmentLeft];
    [lblCardReady setTextColor:[UIColor whiteColor]];
    RetailCardFunds*funds=[currentCard CardFunds];
    float availableAmount=[funds AvailableFunds];
    UILabel *lblAvailableAmount=[[UILabel alloc]initWithFrame:view.bounds];
    [lblAvailableAmount setText:[NSString stringWithFormat:@"Available: %@",[CurrencyFormater formatNumber:[NSNumber numberWithFloat:availableAmount]]]];
    [lblAvailableAmount setTextColor:[UIColor whiteColor]];
    [lblAvailableAmount setFont:[UIFont systemFontOfSize:12.0f]];
    [lblAvailableAmount setTextAlignment:NSTextAlignmentRight];
    
    UIView *dashline=[[UIView alloc]initWithFrame:CGRectMake(0, view.bounds.size.height-2, view.bounds.size.width, 1)];
    [dashline setBackgroundColor:[UIColor whiteColor]];
    
    

   
    [cell.contentView addSubview:view];
    //[cell.contentView setBackgroundColor:[UIColor redColor]];
    [view addSubview:lblCardReady];
    [view addSubview:lblAvailableAmount];
    [view addSubview:dashline];
    }
    return cell;
}
-(CardsViewCell*)cellNo2:(CardsViewCell*)cell{
    
    //https://firebasestorage.googleapis.com/v0/b/blink-34290.appspot.com/o/images%2FRetailCustomers%2F0Odwryma5mxAGTscCKVV.jpg?alt=media&token=d6156aa7-ac1e-44c3-81ac-f1d1478ac738//
    
    [cell.contentView setFrame:CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]])];
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(25, 0, cell.contentView.bounds.size.width-50, cell.contentView.bounds.size.height)];
    UILabel *lblLastTran=[[UILabel alloc]initWithFrame:CGRectMake(0,5, view.frame.size.width, 15)];
    [lblLastTran setText:@"Last transaction:"];
    [lblLastTran setTextColor:[UIColor whiteColor]];
    [lblLastTran setTextAlignment:NSTextAlignmentLeft];
    [lblLastTran setFont:[UIFont systemFontOfSize:12.0f]];
    
    UIImageView*imgView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 30, 50, 50)];
    [imgView.layer setCornerRadius:5.0f];
    [imgView setBackgroundColor:[UIColor clearColor]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url = [NSURL URLWithString:@"https://lh6.ggpht.com/uWuufcWJa7MTdq7kxOIgd0IQhyNn3DAu_domxT5TG2iLjM8cHEH55fb0peDUlsJCwI4=w300"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* image = [[UIImage alloc]initWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [imgView setImage:image];
            [imgView.layer setCornerRadius:5.0f];
        });
    });

    UIView *content=[[UIView alloc]initWithFrame:CGRectMake(30, 15, view.bounds.size.width-35, cell.contentView.bounds.size.height-35)];
    [content.layer setCornerRadius:10.0f];
    [content setBackgroundColor:UIColorFromRGBWithAlpha(0x443e6d, 1.0f)];
        content.layer.shadowRadius  = 5.0f;
    //content.layer.shadowColor   = [UIColor darkGrayColor].CGColor;
    content.layer.shadowOffset  = CGSizeMake(5.0f, 5.0f);
    content.layer.shadowOpacity = 0.1f;
    content.layer.masksToBounds = NO;
    //443e6d
    UILabel *lblTransName=[[UILabel alloc]initWithFrame:CGRectMake(30, 0, content.frame.size.width-90,content.frame.size.height)];
    [lblTransName setText:@"  Telekom MK"];
    [lblTransName setTextColor:[UIColor whiteColor]];
    [lblTransName setTextAlignment:NSTextAlignmentLeft];
    
    UILabel *amount=[[UILabel alloc]initWithFrame:content.bounds];
    [amount setText:[NSString stringWithFormat:@"%@  ",[CurrencyFormater formatNumber:[NSNumber numberWithDouble:-1500]]]];
    [amount setTextAlignment:NSTextAlignmentRight];
    [amount setTextColor:[UIColor whiteColor]];
    
    UILabel *date=[[UILabel alloc]initWithFrame:CGRectMake(0, content.frame.size.height-12, content.frame.size.width, 10)];
    [date setText:@"25.10.2017  "];
    [date setTextAlignment:NSTextAlignmentRight];
    [date setTextColor:[UIColor colorWithWhite:1.0f alpha:0.5f]];
    [date setFont:[UIFont systemFontOfSize:10]];
    
    [content addSubview:lblTransName];
    [content addSubview:amount];
    [content addSubview:date];
    
    
    [view setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [cell.contentView addSubview:view];
    [view addSubview:content];
    [view addSubview:imgView];
    
    
    return cell;
}
-(CardsViewCell*)cellNo3:(CardsViewCell*)cell{
    [cell.contentView setFrame:CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]])];
    if(_cardsArray.count>0){
        RetailCard*currentCard=[_cardsArray objectAtIndex:_currentIndex];
    NSString*bankIssuer=[currentCard BankIssuer];
    NSString*cardNumber=[currentCard CardNumber];
    NSString*expM=[currentCard EXPm];
    NSString*expY=[currentCard EXPy];
    NSString*interbankNetwork=[currentCard InterbankNetwork];
    NSString*cardType=[currentCard CardType];
    
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(25, 0, cell.contentView.bounds.size.width-50, cell.contentView.bounds.size.height)];
    UIView *dashline1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, view.bounds.size.width, 1)];
    [dashline1 setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *separator1=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, view.bounds.size.width, 10)];
    [separator1 setText:@" "];
    
    UILabel *lblIssuer=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, view.bounds.size.width, 20)];
    [lblIssuer setTextAlignment:NSTextAlignmentLeft];
    [lblIssuer setTextColor:[UIColor colorWithWhite:1 alpha:0.75f]];
    [lblIssuer setText:@"Bank issuer"];
    [lblIssuer setFont:[UIFont systemFontOfSize:12.0f]];
    
    UILabel *lblIssuerName=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, view.bounds.size.width, 20)];
    [lblIssuerName setTextAlignment:NSTextAlignmentRight];
    [lblIssuerName setTextColor:[UIColor colorWithWhite:1 alpha:1]];
    [lblIssuerName setText:bankIssuer];
    [lblIssuerName setFont:[UIFont systemFontOfSize:16.0f]];
    
    UILabel *separator2=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, view.bounds.size.width, 10)];
    [separator2 setText:@" "];
    
    UILabel *lblCarnNo=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, view.bounds.size.width, 20)];
    [lblCarnNo setTextAlignment:NSTextAlignmentLeft];
    [lblCarnNo setTextColor:[UIColor colorWithWhite:1 alpha:0.75f]];
    [lblCarnNo setText:@"Card number"];
    [lblCarnNo setFont:[UIFont systemFontOfSize:12.0f]];
    
    UILabel *lblCardNoNumber=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, view.bounds.size.width, 20)];
    [lblCardNoNumber setTextAlignment:NSTextAlignmentRight];
    [lblCardNoNumber setTextColor:[UIColor colorWithWhite:1 alpha:1]];
    [lblCardNoNumber setText:[NSString stringWithFormat:@"•••• •••• •••• %@",[cardNumber substringFromIndex:12]]];//@"•••• •••• •••• 1234"
    [lblCardNoNumber setFont:[UIFont systemFontOfSize:16.0f]];
    
    UILabel *separator3=[[UILabel alloc]initWithFrame:CGRectMake(0, 60, view.bounds.size.width, 10)];
    [separator3 setText:@" "];
    
    UILabel *lblExpDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 70, view.bounds.size.width, 20)];
    [lblExpDate setTextAlignment:NSTextAlignmentLeft];
    [lblExpDate setTextColor:[UIColor colorWithWhite:1 alpha:0.75f]];
    [lblExpDate setText:@"Exp date"];
    [lblExpDate setFont:[UIFont systemFontOfSize:12.0f]];
    
    UILabel *lblExpDateDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 70, view.bounds.size.width, 20)];
    [lblExpDateDate setTextAlignment:NSTextAlignmentRight];
    [lblExpDateDate setTextColor:[UIColor colorWithWhite:1 alpha:1]];
    [lblExpDateDate setText:[NSString stringWithFormat:@"%@/%@",expM,expY]];
    [lblExpDateDate setFont:[UIFont systemFontOfSize:16.0f]];
    
    UILabel *separator4=[[UILabel alloc]initWithFrame:CGRectMake(0, 90, view.bounds.size.width, 10)];
    [separator3 setText:@" "];
    
    UILabel *lblCvvCode=[[UILabel alloc]initWithFrame:CGRectMake(0, 100, view.bounds.size.width, 20)];
    [lblCvvCode setTextAlignment:NSTextAlignmentLeft];
    [lblCvvCode setTextColor:[UIColor colorWithWhite:1 alpha:0.75f]];
    [lblCvvCode setText:@"Card type"];
    [lblCvvCode setFont:[UIFont systemFontOfSize:12.0f]];
    
    UILabel *lblCvvCodeCode=[[UILabel alloc]initWithFrame:CGRectMake(0, 100, view.bounds.size.width, 20)];
    [lblCvvCodeCode setTextAlignment:NSTextAlignmentRight];
    [lblCvvCodeCode setTextColor:[UIColor colorWithWhite:1 alpha:1]];
    [lblCvvCodeCode setText:[NSString stringWithFormat:@"%@ %@",interbankNetwork,cardType]];
    [lblCvvCodeCode setFont:[UIFont systemFontOfSize:16.0f]];
//    UILabel *lblAvailableAmount=[[UILabel alloc]initWithFrame:view.bounds];
//    [lblAvailableAmount setText:[NSString stringWithFormat:@"Available: %@",[CurrencyFormater formatNumber:[NSNumber numberWithDouble:20000]]]];
//    [lblAvailableAmount setTextColor:[UIColor whiteColor]];
//    [lblAvailableAmount setFont:[UIFont systemFontOfSize:12.0f]];
//    [lblAvailableAmount setTextAlignment:NSTextAlignmentRight];
    
    UIView *dashline=[[UIView alloc]initWithFrame:CGRectMake(0, view.bounds.size.height-2, view.bounds.size.width, 1)];
    [dashline setBackgroundColor:[UIColor whiteColor]];
    
    
    
    [view setBackgroundColor:[UIColor clearColor]];
    [cell.contentView addSubview:view];
    //[cell.contentView setBackgroundColor:[UIColor redColor]];
    [view addSubview:dashline1];
    
    [view addSubview:separator1];
    [view addSubview:lblIssuer];
    [view addSubview:lblIssuerName];
    
    [view addSubview:separator2];
    [view addSubview:lblCarnNo];
    [view addSubview:lblCardNoNumber];
    
    [view addSubview:separator3];
    [view addSubview:lblExpDate];
    [view addSubview:lblExpDateDate];
    
    [view addSubview:separator4];
    [view addSubview:lblCvvCode];
    [view addSubview:lblCvvCodeCode];

//[view addSubview:lblAvailableAmount];
    [view addSubview:dashline];
    }
    return cell;
}
-(CardsViewCell*)cellNo4:(CardsViewCell*)cell{
    [cell.contentView setFrame:CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]])];
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(25, 0, cell.contentView.bounds.size.width-50, cell.contentView.bounds.size.height)];
    
    if(_cardsArray.count>0){
    int height=view.frame.size.height;
    UIButton *nfcButton=[[UIButton alloc]initWithFrame:CGRectMake(view.frame.size.width/2-height/2+10, 20, height-40, height-40)];
    [nfcButton setImage:[UIImage imageNamed:@"nfc"] forState:UIControlStateNormal];
    [nfcButton addTarget:self action:@selector(nfcSessionActivation) forControlEvents:UIControlEventTouchUpInside];
    //[nfcButton setBackgroundColor:[UIColor redColor]];
    
//    [UIView animateWithDuration:1
//                          delay:0
//                        options:UIViewKeyframeAnimationOptionAutoreverse | UIViewKeyframeAnimationOptionRepeat
//                     animations:^{
//                         nfcButton.transform = CGAffineTransformMakeScale(1.2, 1.2);
//                     }
//                     completion:nil];
    
    RetailCard*currentCard=[_cardsArray objectAtIndex:_currentIndex];
    NSString*status=[currentCard Status];
    if([status isEqualToString:@"active"]){
        [view setAlpha:1.0f]; }
        else{
            [view setAlpha:0.0f];
        }
    [view setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [cell.contentView addSubview:view];
    [view addSubview:nfcButton];
    }
        return cell;
}

- (UIImage *)grayscaleImage:(UIImage *)image {
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIImage *grayscale = [ciImage imageByApplyingFilter:@"CIColorControls"
                                    withInputParameters: @{kCIInputSaturationKey : @0.0}];
    return [UIImage imageWithCIImage:grayscale];
}

- (void) readerSession:(nonnull NFCNDEFReaderSession *)session didDetectNDEFs:(nonnull NSArray<NFCNDEFMessage *> *)messages {
    
    if (@available(iOS 11.0, *)) {
        for (NFCNDEFMessage *message in messages) {
            for (NFCNDEFPayload *payload in message.records) {
//                NSLog(@"Payload data:%@",payload.payload);
//                NSString *str = [[NSString alloc] initWithData:payload.payload encoding:NSASCIIStringEncoding];
//                NSLog(@"Payload data: %@", str);
                id parsedPayload = [VYNFCNDEFPayloadParser parse:payload];
                if (parsedPayload) {
                    NSString *text = @"";
                    NSString *urlString = nil;
                    if ([parsedPayload isKindOfClass:[VYNFCNDEFTextPayload class]]) {
                        text = @"[Text payload]\n";
                        text = [NSString stringWithFormat:@"%@%@", text, ((VYNFCNDEFTextPayload *)parsedPayload).text];
                    } else if ([parsedPayload isKindOfClass:[VYNFCNDEFURIPayload class]]) {
                        text = @"[URI payload]\n";
                        text = [NSString stringWithFormat:@"%@%@", text, ((VYNFCNDEFURIPayload *)parsedPayload).URIString];
                        urlString = ((VYNFCNDEFURIPayload *)parsedPayload).URIString;
                    } else if ([parsedPayload isKindOfClass:[VYNFCNDEFTextXVCardPayload class]]) {
                        text = @"[TextXVCard payload]\n";
                        text = [NSString stringWithFormat:@"%@%@", text, ((VYNFCNDEFTextXVCardPayload *)parsedPayload).text];
                    } else if ([parsedPayload isKindOfClass:[VYNFCNDEFSmartPosterPayload class]]) {
                        text = @"[SmartPoster payload]\n";
                        VYNFCNDEFSmartPosterPayload *sp = parsedPayload;
                        for (VYNFCNDEFTextPayload *textPayload in sp.payloadTexts) {
                            text = [NSString stringWithFormat:@"%@%@\n", text, textPayload.text];
                        }
                        text = [NSString stringWithFormat:@"%@%@", text, sp.payloadURI.URIString];
                        urlString = sp.payloadURI.URIString;
                    } else if ([parsedPayload isKindOfClass:[VYNFCNDEFWifiSimpleConfigPayload class]]) {
                        text = @"[WifiSimpleConfig payload]\n";
                        VYNFCNDEFWifiSimpleConfigPayload *wifi = parsedPayload;
                        for (VYNFCNDEFWifiSimpleConfigCredential *credential in wifi.credentials) {
                            text = [NSString stringWithFormat:@"%@SSID: %@\nPassword: %@\nMac Address: %@\nAuth Type: %@\nEncrypt Type: %@",
                                    text, credential.ssid, credential.networkKey, credential.macAddress,
                                    [VYNFCNDEFWifiSimpleConfigCredential authTypeString:credential.authType],
                                    [VYNFCNDEFWifiSimpleConfigCredential encryptTypeString:credential.encryptType]];
                        }
                        if (wifi.version2) {
                            text = [NSString stringWithFormat:@"%@\nVersion2: %@",
                                    text, wifi.version2.version];
                        }
                    } else {
                        text = @"Parsed but unhandled payload type";
                    }
                    NSLog(@"%@", text);
                    [self presentPayment:@"2" userInfo:nil];
                    
                [_nfcSession invalidateSession];
                   
                   
            }
            }}
    } else {
        // Fallback on earlier versions
    }
}
- (void)readerSession:(nonnull NFCNDEFReaderSession *)session didInvalidateWithError:(nonnull NSError *)error {
    NSLog(@"nfc error %@",error);
    if(error.code==204){
       // dispatch_async(dispatch_get_main_queue(), ^{
        
      //  });
        
    }
}
-(void)nfcSessionActivation{
    if (@available(iOS 11.0, *)) {
        _nfcSession=[[NFCNDEFReaderSession alloc] initWithDelegate:self queue:dispatch_queue_create(NULL, DISPATCH_QUEUE_CONCURRENT) invalidateAfterFirstRead:YES];
        
    } else {
        // Fallback on earlier versions
    }
    NSLog(@"click");
    [_nfcSession beginSession];
}
-(void)presentPayment:(NSString*)paymentType userInfo:(NSDictionary*)userInfo{
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"Payment" bundle:nil];
    PaymentViewController*payment=[sb instantiateViewControllerWithIdentifier:@"PaymentViewController"];
    [payment.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [payment setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    payment.userInfo=userInfo;
    payment.paymentType=paymentType;
    payment.sender=self.sender;
    [_sender presentViewController:payment animated:NO completion:nil];
//    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"Payment" bundle:nil];
//    PaymentViewController*payment=[sb instantiateViewControllerWithIdentifier:@"PaymentViewController"];
//    [payment.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
//    [payment setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//    payment.paymentType=@"2";
//    payment.sender=self.sender;
//    [_sender presentViewController:payment animated:NO completion:nil];
   // NSLog(@"payment %@ %@",payment,payment.paymentType);
   // payment=[sb instantiateViewControllerWithIdentifier:@"PaymentViewController"];
   
//    dispatch_async(dispatch_get_main_queue(), ^{
//        NSLog(@"called");
//        [payment.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
//        [payment setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//        payment.paymentType=@"2";
//        NSLog(@"payment %@ %@",payment,payment.paymentType);
//
//
//    });
//
//    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
//    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 1 * NSEC_PER_SEC);
//    dispatch_source_set_event_handler(timer, ^{
//         [_sender presentViewController:payment animated:NO completion:nil];
//    });
//    dispatch_resume(timer);
//
    
    
    
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [_sender presentViewController:payment animated:NO completion:nil];
//    });
//
    
    
    
    
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//        // [_sender presentViewController:payment animated:NO completion:nil];
//    });
}

-(NSInteger)getCardIndex{
    NSLog(@"getCardIndex and self %@",self);
    return _currentIndex;
}
@end
//•
/*
 UIView*imgView=[[UIView alloc]initWithFrame:CGRectMake(5, 10, 40, 40)];
 [imgView.layer setCornerRadius:5.0f];
 [imgView setBackgroundColor:[UIColor redColor]];
 
 UIView *content=[[UIView alloc]initWithFrame:CGRectMake(25, 0, self.frame.size.width-40, self.frame.size.height)];
 [content.layer setCornerRadius:5.0f];
 [content setBackgroundColor:[UIColor blueColor]];
 
 [self addSubview:content];
 [self addSubview:imgView];
 
 [UIView animateWithDuration:1
 delay:0
 options:UIViewKeyframeAnimationOptionAutoreverse | UIViewKeyframeAnimationOptionRepeat
 animations:^{
 yourView.transform = CGAffineTransformMakeScale(1.5, 1.5);
 }
 completion:nil];
 */
//
