//
//  MILoyaltyCardsView.m
//  BlinkClient
//
//  Created by Petar on 1/4/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "MILoyaltyCardsView.h"
#import "BlinkClient-Swift.h"

@interface MILoyaltyCardsView() <FSPagerViewDataSource,FSPagerViewDelegate>

@property (strong, nonatomic) NSArray<NSString *> *imageNames;
@property (assign, nonatomic) NSInteger typeIndex;
@property  FSPagerView *pagerView;
@property NSInteger currentIndex;

@end
@implementation MILoyaltyCardsView
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
}
-(id)initWithFrame:(CGRect)rect{
    self = [super initWithFrame:rect];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    }
    _pagerView=[[FSPagerView alloc]initWithFrame:CGRectMake(rect.origin.x, 0, rect.size.width, 600)];
    [self.pagerView registerClass:[FSPagerViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.pagerView.isInfinite = NO;
    self.typeIndex = 0;
    _pagerView.alpha=1.0f;
    _pagerView.delegate=self;
    _pagerView.dataSource=self;
    _currentIndex=0;
    [self addSubview:_pagerView];
    
    return self;
}
- (FSPagerViewCell *)pagerView:(FSPagerView *)pagerView cellForItemAtIndex:(NSInteger)index
{
    FSPagerViewCell * cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"cell" atIndex:index];
    [cell.imageView setImage:[UIImage imageNamed:@"testrotate"]];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    cell.imageView.clipsToBounds = YES;
    
    return cell;
}

- (void)setTypeIndex:(NSInteger)typeIndex
{
    FSPagerViewTransformerType type;
    type=FSPagerViewTransformerTypeLinear;
    self.pagerView.transformer = [[FSPagerViewTransformer alloc] initWithType:type];
    CGAffineTransform transform = CGAffineTransformMakeScale(0.85, 0.95);
    self.pagerView.itemSize = CGSizeApplyAffineTransform(self.pagerView.frame.size, transform);
    
    
}
- (NSInteger)numberOfItemsInPagerView:(FSPagerView *)pagerView
{
    return 5;
    
}
- (void)pagerView:(FSPagerView *)pagerView didSelectItemAtIndex:(NSInteger)index
{
    [pagerView deselectItemAtIndex:index animated:YES];
    [pagerView scrollToItemAtIndex:index animated:YES];
    
}
@end
