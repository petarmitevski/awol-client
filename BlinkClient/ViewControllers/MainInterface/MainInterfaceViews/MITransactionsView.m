//
//  MITransactionsView.m
//  BlinkClient
//
//  Created by Petar on 1/3/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "MITransactionsView.h"
#import "TransactionCell.h"
#import "CurrencyFormater.h"

@implementation MITransactionsView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //        _counter=0;
        //        _numberOfPinEnters=0;
        //        _mutPin=[[NSMutableString alloc]init];
        [self setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    }
    
    CGRect tbFrame=CGRectMake(0, 0, frame.size.width, frame.size.height);
    _tableView=[[UITableView alloc]initWithFrame:tbFrame];
    
    [_tableView registerClass:TransactionCell.class forCellReuseIdentifier:@"cell"];
    [_tableView setSeparatorColor:[UIColor clearColor]];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    [_tableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    //[_tableView setBackgroundColor:[UIColor redColor]];
    [self addSubview:_tableView];
    [self addLayerMask];
    
    return self;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TransactionCell*cell=[[TransactionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell" withSize:CGSizeMake(self.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:indexPath])];
    [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url = [NSURL URLWithString:@"https://scontent-vie1-1.xx.fbcdn.net/v/t31.0-8/p960x960/26171597_1532128653503250_4052823070414177344_o.png?oh=5d2abe60adeeeb0eb09b78f212471595&oe=5AFF1C3C"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* image = [[UIImage alloc]initWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.imgView setImage:image];
            [cell.amount setText:[NSString stringWithFormat:@"%@",[CurrencyFormater formatNumber:[NSNumber numberWithDouble:-1500]]]];
            [cell.lblTransName setText:@"  Telekom MK"];
            [cell.date setText:@"25.10.2017"];
        });
    });
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(double)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115;
}
-(TransactionCell*)drawCell:(TransactionCell*)cell{
    
    //https://firebasestorage.googleapis.com/v0/b/blink-34290.appspot.com/o/images%2FRetailCustomers%2F0Odwryma5mxAGTscCKVV.jpg?alt=media&token=d6156aa7-ac1e-44c3-81ac-f1d1478ac738//
    
    [cell.contentView setFrame:CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:_tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]])];
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(25, 0, cell.contentView.bounds.size.width-50, cell.contentView.bounds.size.height)];
    UILabel *lblLastTran=[[UILabel alloc]initWithFrame:CGRectMake(0,5, view.frame.size.width, 15)];
    [lblLastTran setText:@"Last transaction:"];
    [lblLastTran setTextColor:[UIColor whiteColor]];
    [lblLastTran setTextAlignment:NSTextAlignmentLeft];
    [lblLastTran setFont:[UIFont systemFontOfSize:12.0f]];
    
    UIImageView*imgView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 30, 50, 50)];
    [imgView.layer setCornerRadius:5.0f];
    [imgView setBackgroundColor:[UIColor clearColor]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url = [NSURL URLWithString:@"https://lh6.ggpht.com/uWuufcWJa7MTdq7kxOIgd0IQhyNn3DAu_domxT5TG2iLjM8cHEH55fb0peDUlsJCwI4=w300"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* image = [[UIImage alloc]initWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [imgView setImage:image];
            [imgView.layer setCornerRadius:5.0f];
            
        });
    });
    
    UIView *content=[[UIView alloc]initWithFrame:CGRectMake(30, 15, view.bounds.size.width-35, cell.contentView.bounds.size.height-35)];
    [content.layer setCornerRadius:10.0f];
    [content setBackgroundColor:UIColorFromRGBWithAlpha(0x443e6d, 1.0f)];
    content.layer.shadowRadius  = 5.0f;
    //content.layer.shadowColor   = [UIColor darkGrayColor].CGColor;
    content.layer.shadowOffset  = CGSizeMake(5.0f, 5.0f);
    content.layer.shadowOpacity = 0.1f;
    content.layer.masksToBounds = NO;
    //443e6d
    UILabel *lblTransName=[[UILabel alloc]initWithFrame:CGRectMake(30, 0, content.frame.size.width-90,content.frame.size.height)];
    [lblTransName setText:@"  Telekom MK"];
    [lblTransName setTextColor:[UIColor whiteColor]];
    [lblTransName setTextAlignment:NSTextAlignmentLeft];
    
    UILabel *amount=[[UILabel alloc]initWithFrame:content.bounds];
    [amount setText:[NSString stringWithFormat:@"%@ ",[CurrencyFormater formatNumber:[NSNumber numberWithDouble:-1500]]]];
    [amount setTextAlignment:NSTextAlignmentRight];
    [amount setTextColor:[UIColor whiteColor]];
    
    UILabel *date=[[UILabel alloc]initWithFrame:CGRectMake(0, content.frame.size.height-12, content.frame.size.width-5, 10)];
    [date setText:@"25.10.2017  "];
    [date setTextAlignment:NSTextAlignmentRight];
    [date setTextColor:[UIColor colorWithWhite:1.0f alpha:0.5f]];
    [date setFont:[UIFont systemFontOfSize:10]];
    
    [content addSubview:lblTransName];
    [content addSubview:amount];
    [content addSubview:date];
    
    
    [view setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [cell.contentView addSubview:view];
    [view addSubview:content];
    [view addSubview:imgView];
    
    
    return cell;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. The view for the header
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    
    // 2. Set a custom background color and a border
    headerView.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.0f];
    
    
    // 5. Finally return
    return headerView;
}-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0f;
}
-(void)addLayerMask{
    //    UIView *maskView=[[UIView alloc]initWithFrame:CGRectMake(0, 50, _tableView.frame.size.width, 50)];
    //    [maskView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    //    [self addSubview:maskView];
    CAGradientLayer* maskLayer = [CAGradientLayer layer];
    maskLayer.frame=_tableView.superview.bounds;
    CGColorRef outerColor = [UIColor clearColor].CGColor;
    CGColorRef innerColor = [UIColor blackColor].CGColor;
    maskLayer.colors = [NSArray arrayWithObjects:(__bridge id)outerColor,
                        (__bridge id)innerColor,(__bridge id)innerColor,(__bridge id)innerColor,(__bridge id)innerColor,(__bridge id)innerColor, nil];
    maskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.05],[NSNumber numberWithFloat:0.25],[NSNumber numberWithFloat:0.75],[NSNumber numberWithFloat:0.85],[NSNumber numberWithFloat:1.0], nil];
    
    // maskLayer.bounds = maskView.bounds;
    //maskLayer.anchorPoint = CGPointZero;
    _tableView.superview.layer.mask=maskLayer;
    //  maskView.layer.mask = maskLayer;
    //  [self addSubview:maskView];
}
@end
