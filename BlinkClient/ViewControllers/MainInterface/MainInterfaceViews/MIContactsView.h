//
//  MIContactsView.h
//  BlinkClient
//
//  Created by Petar on 1/4/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIContactsView : UIView<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property UICollectionView*collectionView;

@property NSInteger noOfCells;

@end
