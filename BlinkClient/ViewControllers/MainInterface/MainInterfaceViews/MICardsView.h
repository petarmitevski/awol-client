//
//  MICardsView.h
//  BlinkClient
//
//  Created by Petar on 12/24/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreNFC/CoreNFC.h>
#import <VYNFCKit/VYNFCKit.h>
#import "PaymentViewController.h"

@interface MICardsView : UIView


@property UIScrollView *scrollView;
@property UIViewController *sender;
@property UITableView *tableView;
@property UIView* emptyCardsView;
@property NFCNDEFReaderSession *nfcSession;
@property NSArray *cardsArray;
@property NSInteger currentIndex;
-(void)reloadData;
-(void)presentPayment:(NSString*)paymentType userInfo:(NSDictionary*)userInfo;
-(NSInteger)getCardIndex;
//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
@end
