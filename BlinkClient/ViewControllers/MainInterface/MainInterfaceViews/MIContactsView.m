//
//  MIContactsView.m
//  BlinkClient
//
//  Created by Petar on 1/4/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "MIContactsView.h"
#import "ContactsNewCell.h"
@implementation MIContactsView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
-(id)initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    }
    CGRect cvFrame=CGRectMake(0, 0, frame.size.width, frame.size.height);_noOfCells=25;
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc]init];
    layout.sectionInset = UIEdgeInsetsMake(50, 0, 150, 0);
    _collectionView=[[UICollectionView alloc]initWithFrame:cvFrame collectionViewLayout:layout];
    [_collectionView registerClass:ContactsNewCell.class forCellWithReuseIdentifier:@"Cell"];
    
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    _collectionView.delegate=self;
    _collectionView.dataSource=self;
//    UIView*header=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
//    [header setBackgroundColor:[UIColor clearColor]];
//    [_collectionView addSubview:header];
    [self addSubview:_collectionView];
    [self addLayerMask];
    return self;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    ContactsNewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    //[cell.spinner startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.spinner startAnimating];
        });
        NSURL *url = [NSURL URLWithString:@"https://scontent-vie1-1.xx.fbcdn.net/v/t31.0-8/p960x960/26171597_1532128653503250_4052823070414177344_o.png?oh=5d2abe60adeeeb0eb09b78f212471595&oe=5AFF1C3C"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* image = [[UIImage alloc]initWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [cell.imgView setImage:image];
            [cell.lblName setText:@"text text text"];
            [cell.spinner stopAnimating];
        });
        
        
    });
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _noOfCells;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(_collectionView.frame.size.width/3-10,_collectionView.frame.size.width/3-10);
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"viewForSupplementaryElementOfKind: %@",kind);
    // check if header or footer
    if (kind == UICollectionElementKindSectionFooter) {
        
        UICollectionReusableView *footerView = [self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _collectionView.frame.size.width, _collectionView.frame.size.width/3)];
        view.backgroundColor = [UIColor clearColor];
        
        
        [footerView addSubview:view];
        
        return footerView;
    }
    
    // return nil if view is not a headerview
    
    return nil;
}
-(void)addLayerMask{
    //    UIView *maskView=[[UIView alloc]initWithFrame:CGRectMake(0, 50, _tableView.frame.size.width, 50)];
    //    [maskView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    //    [self addSubview:maskView];
    CAGradientLayer* maskLayer = [CAGradientLayer layer];
    maskLayer.frame=_collectionView.superview.bounds;
    CGColorRef outerColor = [UIColor clearColor].CGColor;
    CGColorRef innerColor = [UIColor blackColor].CGColor;
    maskLayer.colors = [NSArray arrayWithObjects:(__bridge id)outerColor,
                        (__bridge id)innerColor,(__bridge id)innerColor,(__bridge id)innerColor,(__bridge id)innerColor,(__bridge id)innerColor, nil];
    maskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.05],[NSNumber numberWithFloat:0.25],[NSNumber numberWithFloat:0.75],[NSNumber numberWithFloat:0.85],[NSNumber numberWithFloat:1.0], nil];
    
    // maskLayer.bounds = maskView.bounds;
    //maskLayer.anchorPoint = CGPointZero;
    _collectionView.superview.layer.mask=maskLayer;
    //  maskView.layer.mask = maskLayer;
    //  [self addSubview:maskView];
}
@end
