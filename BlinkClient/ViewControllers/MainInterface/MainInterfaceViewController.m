//
//  MainInterfaceViewController.m
//  BlinkClient
//
//  Created by Petar on 12/12/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "MainInterfaceViewController.h"
#import <Firebase/Firebase.h>
#import "MICardsView.h"
#import "GetServerData.h"
#import "AppCore.h"
#import "KYDrawerController.h"

#import "ManagePaymentCardsViewController.h"
#import "NewPaymentCardViewController.h"



@interface MainInterfaceViewController ()
@end

@implementation MainInterfaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrayOfTabbarButtons=[[NSMutableArray alloc]init];
    _MIViewsArray=[[NSMutableArray alloc]init];
    [self setupBackground];
    [self setupTabBar];
    [self addNotifObservers];
   
}
-(BOOL)prefersHomeIndicatorAutoHidden{
    [super prefersHomeIndicatorAutoHidden];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)setupBackground{
    _gradientLayerSurface=[[UIView alloc]initWithFrame:self.view.bounds];
    _gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.view.bounds;
    _gradientLayer.colors = @[(id)UIColorFromRGBWithAlpha(0x7e68ff, 1.0).CGColor,
                              (id)UIColorFromRGBWithAlpha(0x31a1ff, 1.0).CGColor];
    _gradientLayer.startPoint = CGPointMake(0.0, 0.5);
    _gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    [_gradientLayerSurface.layer addSublayer:_gradientLayer];
    [self.view addSubview:_gradientLayerSurface];
    
    
    _markLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 75, self.view.frame.size.width-40, 50)];
    [_markLabel setText:@"payment cards"];
    [_markLabel setTextColor:[UIColor colorWithWhite:1 alpha:0.5f]];
    [_markLabel setFont:[UIFont systemFontOfSize:35]];
    CATransition *animation = [CATransition animation];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionFade;
    animation.duration = 0.75;
    [_markLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
    [self.view addSubview:_markLabel];
    
    UIButton *toggleButton=[[UIButton alloc]initWithFrame:CGRectMake(20, 50, 30, 30)];
    [toggleButton setBackgroundColor:[UIColor clearColor]];
    [toggleButton setBackgroundImage:[UIImage imageNamed:@"ic_hamburger"] forState:UIControlStateNormal];
    [toggleButton setAlpha:0.7f];
    [toggleButton addTarget:self action:@selector(showDrawer) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //    _markGradientOverlay=[[UIImageView alloc]initWithFrame:CGRectMake(-self.view.frame.size.width/2+50, 0, self.view.frame.size.width*2-100, self.view.frame.size.height)];
    //    [_markGradientOverlay setImage:[UIImage imageNamed:@"rcoverlay"]];
    //    [_markGradientOverlay setContentMode:UIViewContentModeScaleAspectFit];
    //    [self.view addSubview:_markGradientOverlay];
    
    UIView*containerView=[[UIView alloc]initWithFrame:self.view.bounds];
    [containerView setBackgroundColor:[UIColor clearColor]];
    [containerView setClipsToBounds:YES];
    //[self.view addSubview:containerView];
    _markImg=[UIImage imageNamed:@"MIbg"];
    _markImgView=[[UIImageView alloc]initWithFrame:CGRectMake(-self.view.frame.size.width/2, 0, self.view.frame.size.width*2, self.view.frame.size.height)];
    [_markImgView setImage:_markImg];
    //_markImgView.image = [_markImgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [containerView addSubview:_markImgView];
    [self.view addSubview:containerView];
    UIImageView *overlay=[[UIImageView alloc]initWithFrame:self.view.frame];
    [overlay setImage:[UIImage imageNamed:@"ic_bg_overlay"]];
    // [self.view addSubview:overlay];
    
    
    
    
    
    //test
    _mainScrollView=[[UIScrollView alloc]initWithFrame:self.view.frame];
    [_mainScrollView setPagingEnabled:YES];
    [_mainScrollView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    //[_mainScrollView setBackgroundColor:[UIColor redColor]];
    [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.frame.size.width*5, _mainScrollView.frame.size.height)];
    // [_mainScrollView setScrollEnabled:YES];
    //[_mainScrollView setAlpha:1.0f];
    [_mainScrollView setScrollEnabled:NO];
    [self.view addSubview:_mainScrollView];
    
    
    /*
     @property UIView *MSVRetailCards;
     @property UIView *MSVTransactions;
     @property UIView *MSVContacts;
     @property UIView *MSVLoyaltyCards;
     @property UIView *MSVMore;
     */
    _MSVRetailCards=[[UIView alloc]initWithFrame:[self getPageFrame:0]];
    [_MSVRetailCards setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_mainScrollView addSubview:_MSVRetailCards];
    _MSVTransactions=[[UIView alloc]initWithFrame:[self getPageFrame:1]];
    [_MSVTransactions setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_mainScrollView addSubview:_MSVTransactions];
    _MSVContacts=[[UIView alloc]initWithFrame:[self getPageFrame:2]];
    [_MSVContacts setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_mainScrollView addSubview:_MSVContacts];
    _MSVLoyaltyCards=[[UIView alloc]initWithFrame:[self getPageFrame:3]];
    [_MSVLoyaltyCards setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_mainScrollView addSubview:_MSVLoyaltyCards];
    _MSVMore=[[UIView alloc]initWithFrame:[self getPageFrame:4]];
    [_MSVMore setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [_mainScrollView addSubview:_MSVMore];
    
    
    UIButton*newPaymentCard=[[UIButton alloc]initWithFrame:CGRectMake(_MSVRetailCards.frame.size.width-50, 50, 30, 30)];
    [newPaymentCard setImage:[[UIImage imageNamed:@"ic_icon_plus"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [newPaymentCard setTintColor:[UIColor colorWithWhite:1.0f alpha:0.7f]];
    [newPaymentCard addTarget:self action:@selector(popupNewPaymentCard) forControlEvents:UIControlEventTouchUpInside];
    [_MSVRetailCards addSubview:newPaymentCard];
    //    UILabel *lblRetailCards=[[UILabel alloc]initWithFrame:CGRectMake(0, 75, self.view.frame.size.width, 40)];
    //    [lblRetailCards setText:@"Payment Cards"];
    //    [lblRetailCards setTextAlignment:NSTextAlignmentCenter];
    //    [lblRetailCards setTextColor:[UIColor whiteColor]];
    //    [lblRetailCards setFont:[UIFont systemFontOfSize:25]];
    //    [_MSVRetailCards addSubview:lblRetailCards];
    //
    //    UILabel *lblTransactions=[[UILabel alloc]initWithFrame:CGRectMake(0, 75, self.view.frame.size.width, 40)];
    //    [lblTransactions setText:@"Transactions"];
    //    [lblTransactions setTextAlignment:NSTextAlignmentCenter];
    //    [lblTransactions setTextColor:[UIColor whiteColor]];
    //    [lblTransactions setFont:[UIFont systemFontOfSize:25]];
    //    [_MSVTransactions addSubview:lblTransactions];
    //
    //    UILabel *lblContacts=[[UILabel alloc]initWithFrame:CGRectMake(0, 75, self.view.frame.size.width, 40)];
    //    [lblContacts setText:@"Contacts"];
    //    [lblContacts setTextAlignment:NSTextAlignmentCenter];
    //    [lblContacts setTextColor:[UIColor whiteColor]];
    //    [lblContacts setFont:[UIFont systemFontOfSize:25]];
    //    [_MSVContacts addSubview:lblContacts];
    //
    //    UILabel *lblLoyaltyCards=[[UILabel alloc]initWithFrame:CGRectMake(0, 75, self.view.frame.size.width, 40)];
    //    [lblLoyaltyCards setText:@"Loyalty Cards"];
    //    [lblLoyaltyCards setTextAlignment:NSTextAlignmentCenter];
    //    [lblLoyaltyCards setTextColor:[UIColor whiteColor]];
    //    [lblLoyaltyCards setFont:[UIFont systemFontOfSize:25]];
    //    [_MSVLoyaltyCards addSubview:lblLoyaltyCards];
    //
    //    UILabel *lblMore=[[UILabel alloc]initWithFrame:CGRectMake(0, 75, self.view.frame.size.width, 40)];
    //    [lblMore setText:@"More"];
    //    [lblMore setTextAlignment:NSTextAlignmentCenter];
    //    [lblMore setTextColor:[UIColor whiteColor]];
    //    [lblMore setFont:[UIFont systemFontOfSize:25]];
    //    [_MSVMore addSubview:lblMore];
    
    
    //  [self animateBG:_markImgView];
    //    UIView *whiteView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height/3*2)];
    //    [whiteView setBackgroundColor:[UIColor whiteColor]];
    //    [self.view addSubview:whiteView];
    //    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    //    CGRect maskFrame=CGRectMake(0, self.view.frame.size.height/3*2, self.view.frame.size.width, self.view.frame.size.height/4);
    //    gradientLayer.frame =maskFrame;
    //    gradientLayer.colors = @[(id)[UIColor whiteColor].CGColor,
    //
    //                            (id)[UIColor colorWithWhite:1 alpha:0.0].CGColor];
    //    [self.view.layer addSublayer:gradientLayer];
    
    
    
    [self.view addSubview:toggleButton];
    
}
-(void)animateBG:(UIImageView *)view{
    [UIImageView animateWithDuration:2.0 animations:^{
        [view setTintColor:[UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0]];
    }completion:^(BOOL finished) {
        [self animateBG:view];
    }];
    
}
-(void)setupTabBar{
    _tabBarView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/7*6+35, self.view.frame.size.width, self.view.frame.size.height-self.view.frame.size.height/7*6)];
    //[tabBarView.layer setCornerRadius:30];
    _tabBarView.clipsToBounds=YES;
    //  tabBarView.alpha=1f;
    // [tabBarView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.0]];
    //    UIVisualEffect *blurEffect;
    //    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    
    
    
    //UIVisualEffectView *visualEffectView =[self setBlurEffect:tabBarView];
    // [tabBarView addSubview:visualEffectView];
    [self.view addSubview:_tabBarView];
    //
    
    UIView *buttonContainer=[[UIView alloc]initWithFrame:CGRectMake(20, 15, _tabBarView.frame.size.width-30, _tabBarView.frame.size.height-30)];
    [buttonContainer setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.0f]];
    // [tabBarView addSubview:buttonContainer];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [blurEffectView setFrame:_tabBarView.bounds];
    blurEffectView.clipsToBounds=YES;
    [_tabBarView addSubview:blurEffectView];
    
    UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    UIVisualEffectView *vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
    [vibrancyEffectView setFrame:_tabBarView.bounds];
    
    int buttonSideSize=buttonContainer.frame.size.height-50;
    int spaceBetween=38;
    
    UIButton *btnCards=[[UIButton alloc]initWithFrame:CGRectMake(20, 15, buttonSideSize, buttonSideSize)];
    [btnCards setImage:[[UIImage imageNamed:@"ic_icon_crcard"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    //[btnCards setTintColor:[UIColor redColor]];
    [btnCards setTag:100];
    [btnCards addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnTrans=[[UIButton alloc]initWithFrame:CGRectMake(btnCards.frame.origin.x+btnCards.frame.size.width+spaceBetween, 15, buttonSideSize, buttonSideSize)];
    [btnTrans setImage:[[UIImage imageNamed:@"ic_icon_trans"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    //[btnTrans setTintColor:[UIColor lightGrayColor]];
    [btnTrans setTag:101];
    [btnTrans addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnContacts=[[UIButton alloc]initWithFrame:CGRectMake(btnTrans.frame.origin.x+btnTrans.frame.size.width+spaceBetween, 15, buttonSideSize, buttonSideSize)];
    [btnContacts setImage:[[UIImage imageNamed:@"ic_icon_contacts2"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    //[btnContacts setTintColor:[UIColor lightGrayColor]];
    [btnContacts setTag:102];
    [btnContacts addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnLoyalty=[[UIButton alloc]initWithFrame:CGRectMake(btnContacts.frame.origin.x+btnContacts.frame.size.width+spaceBetween, 15, buttonSideSize,buttonSideSize)];
    [btnLoyalty setImage:[[UIImage imageNamed:@"ic_icon_loyalty2"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    // [btnLoyalty setTintColor:[UIColor lightGrayColor]];
    [btnLoyalty setTag:103];
    [btnLoyalty addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(btnLoyalty.frame.origin.x+btnLoyalty.frame.size.width+spaceBetween, 15, buttonSideSize, buttonSideSize)];
    [btnMore setBackgroundImage:[[UIImage imageNamed:@"ic_icon_notifications"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    //[btnMore setTintColor:[UIColor lightGrayColor]];
    [btnMore setTag:104];
    [btnMore addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _tabbarDashline=[[UIView alloc]initWithFrame:CGRectMake(_tabBarView.frame.size.width/5*0+_tabBarView.frame.size.width/5/2/2, 5, _tabBarView.frame.size.width/5/2, 3)];
    [_tabbarDashline.layer setCornerRadius:1.5f];
    [_tabbarDashline setBackgroundColor:[UIColor whiteColor]];
    [_tabbarDashline tintColorDidChange];
    
    [[vibrancyEffectView contentView] addSubview:btnCards];
    [[vibrancyEffectView contentView] addSubview:btnTrans];
    [[vibrancyEffectView contentView] addSubview:btnContacts];
    [[vibrancyEffectView contentView] addSubview:btnLoyalty];
    [[vibrancyEffectView contentView] addSubview:btnMore];
    [[vibrancyEffectView contentView] addSubview:_tabbarDashline];
    [[blurEffectView contentView] addSubview:vibrancyEffectView];
    
    // [_tabBarView addSubview:buttonContainer];
    //    [buttonContainer addSubview:btnCards];
    //    [buttonContainer addSubview:btnTrans];
    //    [buttonContainer addSubview:btnContacts];
    //    [buttonContainer addSubview:btnLoyalty];
    //    [buttonContainer addSubview:btnMore];
    [_arrayOfTabbarButtons addObject:btnCards];
    [_arrayOfTabbarButtons addObject:btnTrans];
    [_arrayOfTabbarButtons addObject:btnContacts];
    [_arrayOfTabbarButtons addObject:btnLoyalty];
    [_arrayOfTabbarButtons addObject:btnMore];
    [_tabbarDashline tintColorDidChange];
    
    
    
    _cardsView=[[MICardsView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
    _cardsView.sender=self;
    NSLog(@"CardsView1: %@ and self: %@",_cardsView,self);
    [_MSVRetailCards addSubview:_cardsView];
    [_MIViewsArray addObject:_cardsView];
    _transactionsView=[[MITransactionsView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
    [_MSVTransactions addSubview:_transactionsView];
    [_MIViewsArray addObject:_transactionsView];
    _contactsView=[[MIContactsView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
    [_MSVContacts addSubview:_contactsView];
    [_MIViewsArray addObject:_contactsView];
    _loyaltyCardsView=[[MILoyaltyCardsView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
    [_MSVLoyaltyCards addSubview:_loyaltyCardsView];
    [_MIViewsArray addObject:_loyaltyCardsView];
    //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(cardsReloadData) userInfo:nil repeats:nil];
    //    [_cardsView reloadData];
    [self setupButtons];
    
    
}
-(void)addNotifObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismisSelf)
                                                 name:ApplicationDidTimeoutNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popupManagePaymentCards)
                                                 name:@"popupManagePaymentCards" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterInRegion) name:@"enterInRegion" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitRegion) name:@"exitRegion" object:nil];
}

-(void)cardsReloadData{
    // [_cardsView reloadData];
    NSLog(@"RELOADED");
    
}
-(void)setupButtons{
    
}



-(UIVisualEffectView*)setBlurEffect:(UIView*)view{
    MLWBluuurView *visualEffectView;
    visualEffectView = [[MLWBluuurView alloc] init];
    visualEffectView.frame = view.bounds;
    [visualEffectView.layer setCornerRadius:30];
    visualEffectView.clipsToBounds = YES;
    [visualEffectView setBlurRadius:7.0f];
    //[visualEffectView setAlpha:0.5f];
    return visualEffectView;
}
-(void)getCards{
    FIRFirestore *defaultFirestore = [FIRFirestore firestore];
    FIRQuery *query=[[defaultFirestore collectionWithPath:@"RetailCards"] queryWhereField:@"CustomerID" isEqualTo:[FIRAuth auth].currentUser.uid];
    
    [query getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
        NSLog(@"QUERY RESULT: %@",snapshot.documents);
        for(FIRDocumentSnapshot *_Nullable snapshotDocument in snapshot.documents){
            NSLog(@"Query Result: %@ data: %@",snapshotDocument.documentID,snapshotDocument.data);
        }
    }];
}
-(void)clickButton:(UIButton*)sender{
    switch (sender.tag) {
        case 100:
            [self moveDashLineToFrame:CGRectMake(_tabBarView.frame.size.width/5*0+_tabBarView.frame.size.width/5/2/2, 5, _tabBarView.frame.size.width/5/2, 3)];
            [self moveMarkImgViewToFrame:CGRectMake(-self.view.frame.size.width/2, 0, self.view.frame.size.width*2, self.view.frame.size.height)];
            [self changeLabelText:@"payment cards"];
            [self scrollToPage:0];
            [self hideUnvisibleViewsBut:_cardsView];
            break;
        case 101:
            [self moveDashLineToFrame:CGRectMake(_tabBarView.frame.size.width/5*1+_tabBarView.frame.size.width/5/2/2, 5, _tabBarView.frame.size.width/5/2, 3)];
            [self moveMarkImgViewToFrame:CGRectMake(-self.view.frame.size.width/2-30, 0, self.view.frame.size.width*2, self.view.frame.size.height)];
            [self changeLabelText:@"transactions"];
            _nextColors=[NSArray arrayWithObjects:
                         (id)[[UIColor redColor] CGColor],
                         (id)[[UIColor blueColor] CGColor], nil];
            [self changeGradientColorTo:_nextColors];
            [self scrollToPage:1];
            [self hideUnvisibleViewsBut:_transactionsView];
            break;
        case 102:
            [self moveDashLineToFrame:CGRectMake(_tabBarView.frame.size.width/5*2+_tabBarView.frame.size.width/5/2/2, 5, _tabBarView.frame.size.width/5/2, 3)];
            [self moveMarkImgViewToFrame:CGRectMake(-self.view.frame.size.width/2-60, 0, self.view.frame.size.width*2, self.view.frame.size.height)];
            [self changeLabelText:@"contacts"];
            _nextColors=[NSArray arrayWithObjects:
                         (id)[[UIColor blueColor] CGColor],
                         (id)[[UIColor yellowColor] CGColor], nil];
            [self changeGradientColorTo:_nextColors];
            [self scrollToPage:2];
            [self hideUnvisibleViewsBut:_contactsView];
            NSLog(@"topController: %@",[AppCore topController]);
            //[self sendRequest];
            break;
        case 103:
            [self moveDashLineToFrame:CGRectMake(_tabBarView.frame.size.width/5*3+_tabBarView.frame.size.width/5/2/2, 5, _tabBarView.frame.size.width/5/2, 3)];
            [self moveMarkImgViewToFrame:CGRectMake(-self.view.frame.size.width/2-90, 0, self.view.frame.size.width*2, self.view.frame.size.height)];
            [self changeLabelText:@"loyalty cards"];
            _nextColors=[NSArray arrayWithObjects:
                         (id)[[UIColor greenColor] CGColor],
                         (id)[[UIColor blueColor] CGColor], nil];
            [self changeGradientColorTo:_nextColors];
            [self scrollToPage:3];
            [self hideUnvisibleViewsBut:_loyaltyCardsView];
            break;
        case 104:
            [self moveDashLineToFrame:CGRectMake(_tabBarView.frame.size.width/5*4+_tabBarView.frame.size.width/5/2/2, 5, _tabBarView.frame.size.width/5/2, 3)];
            [self moveMarkImgViewToFrame:CGRectMake(-self.view.frame.size.width/2-120, 0, self.view.frame.size.width*2, self.view.frame.size.height)];
            [self changeLabelText:@"messages"];
            _nextColors=[NSArray arrayWithObjects:
                         (id)[[UIColor orangeColor] CGColor],
                         (id)[[UIColor darkGrayColor] CGColor], nil];
            [self changeGradientColorTo:_nextColors];
            [self scrollToPage:4];
            break;
            
        default:
            break;
    }
    
}
-(void)changeGradientColorTo:(NSArray*)color{
    CABasicAnimation *animateLayer = [CABasicAnimation animationWithKeyPath:@"colors"];
    animateLayer.fromValue=_currentColors;
    animateLayer.toValue = color;
    animateLayer.duration   = 3.0;
    animateLayer.removedOnCompletion = NO;
    animateLayer.fillMode = kCAFillModeBoth;
    animateLayer.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animateLayer.delegate=self;
    [_gradientLayer addAnimation:animateLayer forKey:@"animation"];
    _currentColors=color;
    
    
    //_gradientLayer.colors=color;
    //[_gradientLayerSurface.layer insertSublayer:_gradientLayer atIndex:0];
}
-(void)moveDashLineToFrame:(CGRect)frame{
    [UIView animateWithDuration:0.3f
                     animations:^
     {
         [_tabbarDashline setFrame:frame];
         //                        _gradientLayer.colors=@[(id)[UIColor redColor].CGColor,
         //                                                (id)[UIColor blueColor].CGColor];
     }
                     completion:^(BOOL finished)
     {
         
     }
     ];
}
-(void)moveMarkImgViewToFrame:(CGRect)frame{
    [UIView animateWithDuration:0.3f
                     animations:^
     {
         [_markImgView setFrame:frame];
     }
                     completion:^(BOOL finished)
     {
         
     }
     ];
}


-(void)changeLabelText:(NSString*)text{
    [UIView transitionWithView:_markLabel
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        _markLabel.text = text;
                        
                    } completion:nil];
}
-(void)hideUnvisibleViewsBut:(UIView*)visible{
    for(UIView* view in _MIViewsArray){
        if(visible!=view){
            [self animateHiding:view];
        }else{
            [self animateShowing:view];
        }
    }
    
}
-(void)animateHiding:(UIView*)unvisible{
    [UIView animateWithDuration:0.3f animations:^{
        [unvisible setAlpha:0.0f];
    }];
}
-(void)animateShowing:(UIView*)visible{
    [UIView animateWithDuration:0.3f animations:^{
        [visible setAlpha:1.0f];
    }];
    
}

-(CGRect)getPageFrame:(int)pageNumber{
    CGRect returnFrame=CGRectMake(self.view.frame.size.width*pageNumber, 0, self.view.frame.size.width, self.view.frame.size.height);
    return returnFrame;
}
-(void)scrollToPage:(int)page{
    CGRect frame = _mainScrollView.frame;
    frame.origin.x = frame.size.width*page;
    frame.origin.y = 0;
    [_mainScrollView scrollRectToVisible:frame animated:YES];
}
//-(void)sendRequest{
//    //POST DATA
//    NSString *bodystr=[NSString stringWithFormat:@"Ново барање за наплата од: %@ MKD",_amountLabel.text];
//    NSDictionary *dictInside=[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:bodystr,@"Нова трансакција",@"default", nil] forKeys:[NSArray arrayWithObjects:@"body",@"title",@"sound", nil]];
//    NSDictionary *dict=[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"fJZKdlZPrDQ:APA91bEdvcYq3_Q4opkuOPqMsWc_j9uUk3_EyEbUNJTzyXiNMx3QfoZ_o5_pT0LXK_MtxE6EnIilEEoHtkTYMENQrR7e2YObrOmNZK3gNLomZC5-ITBMa2ivY9Dxofzl5weV7G9QFLHn",dictInside, nil] forKeys:[NSArray arrayWithObjects:@"to",@"notification", nil]];
//    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *jsonStr = [[NSString alloc] initWithData:data
//                                              encoding:NSUTF8StringEncoding];
//    NSLog(@"JSON REQUEST %@",jsonStr);
//    //URL CONFIG
//    NSString *serverURL = @"https://fcm.googleapis.com/fcm/send";
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: serverURL]];
//    //POST DATA SETUP
//    [request setHTTPMethod:@"POST"];
//    [request setHTTPBody:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"key=AAAAOdEtjWA:APA91bGjNKYGDGYIbUfmpawv0n4fD8tTJTyUCAz6uks36a-fRXBUW8dQGkLn1cbYuTaREWzJ8mLJDcekp3Rojxtzr2XqbXjTuTVXgLLZUAWc9PRKkw_RWPah4F_v7y2J9LjOq2iouXew" forHTTPHeaderField:@"Authorization"];
//    //DEBUG MESSAGE
//    NSLog(@"Trying to call ws %@",serverURL);
//    //EXEC CALL
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        if (error) {
//            NSLog(@"Error:%@",error.description);
//        }
//        if (data) {
//
//            //
//            // THIS CODE IS FOR PRINTING THE RESPONSE
//            //
//            NSString *returnString = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
//            NSLog(@"Response:%@",returnString);
//
//            //PARSE JSON RESPONSE
//            NSDictionary *json_response = [NSJSONSerialization JSONObjectWithData:data
//                                                                          options:0
//                                                                            error:NULL];
//            NSLog(@"json_response: %@",json_response);
//            //            if ( json_response ) {
//            //                if ( [json_response isKindOfClass:[NSDictionary class]] ) {
//            //                    // do dictionary things
//            //                    for ( NSString *key in [json_response allKeys] ) {
//            //                        NSLog(@"%@: %@", key, json_response[key]);
//            //                    }
//            //                }
//            //                else if ( [json_response isKindOfClass:[NSArray class]] ) {
//            //                    NSLog(@"%@",json_response);
//            //                }
//            //            }
//            //            else {
//            //                NSLog(@"Error serializing JSON: %@", error);
//            //                NSLog(@"RAW RESPONSE: %@",data);
//            //                NSString *returnString2 = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
//            //                NSLog(@"Response:%@",returnString2);
//            //            }
//        }
//    }];
//}
-(void)dismisSelf{
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)showDrawer{
    NSLog(@"showDrawer1NOTIFSENT");
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ShowDrawer"
     object:self
     userInfo:nil];
}
-(void)popupManagePaymentCards{
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"Popups" bundle:nil];
    ManagePaymentCardsViewController*mpcvc=[sb instantiateViewControllerWithIdentifier:@"ManagePaymentCardsViewController"];
    [mpcvc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:mpcvc animated:NO completion:nil];
}

-(void)popupNewPaymentCard{
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"Popups" bundle:nil];
    NewPaymentCardViewController*npcvc=[sb instantiateViewControllerWithIdentifier:@"NewPaymentCardViewController"];
    [npcvc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [npcvc setSender:_cardsView];
    [self presentViewController:npcvc animated:NO completion:nil];
}
-(void)enterInRegion{
    FIRFirestore *firestore=[FIRFirestore firestore];
    NSString*str=[[RetailCustomerID alloc]generateRetailCustomerID];
    RetailCustomer*customer=[[GetServerData sharedInstance]getCustomer];
    [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"RegionID"];
    NSString*cID=customer.CustomerID;
    [[[firestore collectionWithPath:@"ACTIVITYLOGS"] documentWithPath:@"C8K49eOzZRpoCzCNNB7o"]
     setData:@{ str: cID }
     options:[FIRSetOptions merge]
     completion:^(NSError * _Nullable error) {
         // ...
     }];
}
-(void)exitRegion{
    //[FIRFieldValue fieldValueForDelete]
    NSString *str=[[NSUserDefaults standardUserDefaults]objectForKey:@"RegionID"];
    if(str){
        FIRFirestore *firestore=[FIRFirestore firestore];
        [[[firestore collectionWithPath:@"ACTIVITYLOGS"] documentWithPath:@"C8K49eOzZRpoCzCNNB7o"]
         updateData:@{str:[FIRFieldValue fieldValueForDelete]}
         completion:^(NSError * _Nullable error) {
             // ...
         }];
    }
    
    
}
@end
