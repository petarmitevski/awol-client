//
//  SideMenuViewController.h
//  BlinkClient
//
//  Created by Petar on 1/7/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "RetailCustomer.h"

@interface SideMenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property FIRAuth*auth;
@property FIRStorage*storage;
@property UITableView*tableView;
@property UIVisualEffectView *vibrancyEffectView;
@property int smWidth;
@property RetailCustomer *customer;
@property UILabel *lastPlace;


@end
