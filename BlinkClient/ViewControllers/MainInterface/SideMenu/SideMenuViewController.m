//
//  SideMenuViewController.m
//  BlinkClient
//
//  Created by Petar on 1/7/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "SideMenuViewController.h"
#import "StandardCell.h"
#import "GetServerData.h"


@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setClipsToBounds:YES];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [blurEffectView setFrame:self.view.bounds];
    blurEffectView.clipsToBounds=YES;
    [self.view addSubview:blurEffectView];
    UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    _vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
    [_vibrancyEffectView setFrame:self.view.bounds];
    [[blurEffectView contentView] addSubview:_vibrancyEffectView];
    _storage = [FIRStorage storage];
    _auth=[FIRAuth auth];
    _smWidth=310;
    _customer=[[GetServerData sharedInstance]getCustomer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterInRegion) name:@"enterInRegion" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitRegion) name:@"exitRegion" object:nil];
    
//    _tableView=[[UITableView alloc]initWithFrame:CGRectMake(10, 50+smWidth/4+10, smWidth-20, self.view.frame.size.height-200)];
//    _tableView.delegate=self;
//    _tableView.dataSource=self;
//    [_tableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
//    [_tableView setSeparatorColor:[UIColor clearColor]];
//    [self.view addSubview:_tableView];
    
    
    
    
    [self addProfilePicture];
    [self addTableView];
    
    
    
}
-(void)addProfilePicture{
    _smWidth=310;
    UIView*profilePictureView=[[UIView alloc]initWithFrame:CGRectMake(_smWidth/2-_smWidth/4/2, 50, _smWidth/4, _smWidth/4)];
    [profilePictureView.layer setCornerRadius:profilePictureView.frame.size.width/2];
    [profilePictureView setClipsToBounds:YES];
    UIImageView*profilePicture=[[UIImageView alloc]initWithFrame:profilePictureView.bounds];
    [profilePictureView addSubview:profilePicture];
    [self.view addSubview:profilePictureView];
    
    __block NSString *imgReference=[NSString stringWithFormat:@"gs://blink-34290.appspot.com/images/RetailCustomers/%@.jpg",_auth.currentUser.uid];
    __block UIImage *image=[UIImage new];
    FIRStorageReference *pathReference = [_storage referenceForURL:imgReference];
    [pathReference dataWithMaxSize:1 * 1024 * 1024 completion:^(NSData *data, NSError *error){
        if (error != nil) {
            NSLog(@"error for image, %@",error);
        } else {
            image = [UIImage imageWithData:data];
            [profilePicture setImage:image];
        }
    }];
    
}
-(void)addTableView{
    
    UITapGestureRecognizer *tapGesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    
    int startY=50+_smWidth/4+20;
    UILabel *welcomeLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, startY, 310, 30)];
    [welcomeLabel setText:@"Welcome back,"];
    [welcomeLabel setFont:[UIFont systemFontOfSize:24.0f]];
    [welcomeLabel setTextColor:[UIColor whiteColor]];
    [welcomeLabel setTextAlignment:NSTextAlignmentCenter];
    [[_vibrancyEffectView contentView] addSubview:welcomeLabel];
    
    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(0, startY+30, 310, 30)];
    [name setText:[NSString stringWithFormat:@"%@ %@", [_customer Name],[_customer Surname]]];
    //[name setText:[NSString stringWithFormat:@"%@ %@", [_customer Name],[_customer Surname]]];
    [name setFont:[UIFont systemFontOfSize:24.0f]];
    [name setTextColor:[UIColor whiteColor]];
    [name setTextAlignment:NSTextAlignmentCenter];
    [[_vibrancyEffectView contentView] addSubview:name];
    
    UIView *separatorOne=[[UIView alloc]initWithFrame:CGRectMake(10, startY+70, 290, 1)];
    [separatorOne setBackgroundColor:[UIColor whiteColor]];
    [separatorOne tintColorDidChange];
    [[_vibrancyEffectView contentView] addSubview:separatorOne];
    
    UILabel *lastPlaceLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, startY+80, 290, 30)];
    [lastPlaceLabel setText:@"Inside region:"];
    [lastPlaceLabel setTextColor:[UIColor whiteColor]];
    [[_vibrancyEffectView contentView] addSubview:lastPlaceLabel];
    
    _lastPlace=[[UILabel alloc]initWithFrame:CGRectMake(30, startY+110, 290, 30)];
    [_lastPlace setText:@"Outside of any region"];
    [_lastPlace setTextColor:[UIColor whiteColor]];
    [[_vibrancyEffectView contentView] addSubview:_lastPlace];
    //•
    UIView *separatorTwo=[[UIView alloc]initWithFrame:CGRectMake(10, startY+150, 290, 1)];
    [separatorTwo setBackgroundColor:[UIColor whiteColor]];
    [separatorTwo tintColorDidChange];
    [[_vibrancyEffectView contentView] addSubview:separatorTwo];
    
    UILabel *managePaymentCards=[[UILabel alloc]initWithFrame:CGRectMake(10, startY+170, 290, 30)];
    [managePaymentCards setTextColor:[UIColor whiteColor]];
    [managePaymentCards setText:@"• Manage payment cards"];
    [managePaymentCards setFont:[UIFont systemFontOfSize:20.0f]];
    [managePaymentCards setTag:101];
    [[_vibrancyEffectView contentView] addSubview:managePaymentCards];
    [managePaymentCards setUserInteractionEnabled:YES];
    [managePaymentCards addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    
    UILabel *manageFriends=[[UILabel alloc]initWithFrame:CGRectMake(10, startY+220, 290, 30)];
    [manageFriends setTextColor:[UIColor whiteColor]];
    [manageFriends setFont:[UIFont systemFontOfSize:20.0f]];
    [manageFriends setText:@"• Manage friends"];
    [manageFriends setTag:102];
    [[_vibrancyEffectView contentView] addSubview:manageFriends];
    [manageFriends setUserInteractionEnabled:YES];
    [manageFriends addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    
    UILabel *manageLoyaltyCards=[[UILabel alloc]initWithFrame:CGRectMake(10, startY+270, 290, 30)];
    [manageLoyaltyCards setTextColor:[UIColor whiteColor]];
    [manageLoyaltyCards setFont:[UIFont systemFontOfSize:20.0f]];
    [manageLoyaltyCards setText:@"• Manage loyalty cards"];
    [manageLoyaltyCards setTag:103];
    [[_vibrancyEffectView contentView] addSubview:manageLoyaltyCards];
    [manageLoyaltyCards setUserInteractionEnabled:YES];
    [manageLoyaltyCards addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    
    UILabel *changePIN=[[UILabel alloc]initWithFrame:CGRectMake(10, startY+320, 290, 30)];
    [changePIN setTextColor:[UIColor whiteColor]];
    [changePIN setFont:[UIFont systemFontOfSize:20.0f]];
    [changePIN setText:@"• Change entrance PIN"];
    [changePIN setTag:104];
    [[_vibrancyEffectView contentView] addSubview:changePIN];
    [changePIN setUserInteractionEnabled:YES];
    [changePIN addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    
    UILabel *changeLanguage=[[UILabel alloc]initWithFrame:CGRectMake(10, startY+370, 290, 30)];
    [changeLanguage setTextColor:[UIColor whiteColor]];
    [changeLanguage setFont:[UIFont systemFontOfSize:20.0f]];
    [changeLanguage setText:@"• Change Language"];
    [changeLanguage setTag:105];
    [[_vibrancyEffectView contentView] addSubview:changeLanguage];
    [changeLanguage setUserInteractionEnabled:YES];
    [changeLanguage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    
    UILabel *manageSesion=[[UILabel alloc]initWithFrame:CGRectMake(10, startY+420, 290, 30)];
    [manageSesion setTextColor:[UIColor whiteColor]];
    [manageSesion setFont:[UIFont systemFontOfSize:20.0f]];
    [manageSesion setText:@"• Manage Sesion"];
    [manageSesion setTag:106];
    [[_vibrancyEffectView contentView] addSubview:manageSesion];
    [manageSesion setUserInteractionEnabled:YES];
    [manageSesion addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
}
- (void)didTapLabelWithGesture:(id)sender {
    int tag=[(UIGestureRecognizer *)sender view].tag;
    NSLog(@"klikna, %d",tag);
    [self hideDrawer];
    switch (tag) {
        case 101:
            [self popupManagePaymentCards];
            break;
        case 102:
            [self popupManageFriends];
            break;
        case 103:
            [self popupManageLoyaltyCards];
            break;
        case 104:
            [self popupChangePin];
            break;
        case 105:
            [self popupChangeLanguage];
            break;
        case 106:
            [self popupManageSesion];
            break;
        default:
            break;
    }
   
    
}
-(void)hideDrawer{
    NSLog(@"hideDrawer1NOTIFSENT");
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"HideDrawer"
     object:self
     userInfo:nil];
}

-(void)popupManagePaymentCards{
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"popupManagePaymentCards"
     object:self
     userInfo:nil];
}

-(void)popupManageFriends{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"popupManageFriends"
     object:self
     userInfo:nil];
}

-(void)popupManageLoyaltyCards{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"popupManageLoyaltyCards"
     object:self
     userInfo:nil];
}

-(void)popupChangePin{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"popupChangePin"
     object:self
     userInfo:nil];
}

-(void)popupChangeLanguage{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"popupChangeLanguage"
     object:self
     userInfo:nil];
}

-(void)popupManageSesion{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"popupManageSesion"
     object:self
     userInfo:nil];
}

//
//- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
////    CardsViewCell*cell=[[CardsViewCell alloc]init];
////    cell.layoutMargins=UIEdgeInsetsZero;
////    cell.preservesSuperviewLayoutMargins=NO;
////    [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
////    switch (indexPath.row) {
////        case 0:
////            cell=[self cellNo1:cell];
////            break;
////        case 1:
////            cell=[self cellNo2:cell];
////            break;
////        case 2:
////            cell=[self cellNo3:cell];
////            break;
////        case 3:
////            cell=[self cellNo4:cell];
////            break;
////        default:
////            break;
////    }
////
//   // return cell;
//
//    StandardCell *cell=[[StandardCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell" withSize:CGSizeMake(100, [self tableView:_tableView heightForRowAtIndexPath:indexPath])];
//    [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
//    switch (indexPath.row) {
//        case 0:
//            [cell.label setText:@"Last visited place:"];
//            break;
//        case 1:
//            [cell.label setTextAlignment:NSTextAlignmentRight];
//            [cell.label setText:@"Telekom MK Skopje Capitol Mall"];
//            break;
//        case 2:
//            [cell.label setText:@" "];
//            break;
//        case 3:
//            [cell.label setText:@"AWOL Wallet"];
//            break;
//        case 4:
//            [cell.label setTextAlignment:NSTextAlignmentRight];
//            [cell.label setText:@"Petar Mitevski"];
//            break;
//        default:
//            break;
//    }
//    return cell;
//}
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return 1;
//}
//- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 10;
//}
//-(double)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 30.0f;
////    switch (indexPath.row) {
////        case 0:
////            return 20.0f;
////            break;
////        case 1:
////            return 115.0f;
////            break;
////        case 2:
////            return 130.0f;
////            break;
////        case 3:
////            return 100.0f;
////            break;
////        default:
////            return 130.0f;
////            break;
////    }
////
//}
//
-(void)enterInRegion{
    [_lastPlace setText:@"Ramstore MK POS-ID:101/102"];
}
-(void)exitRegion{
    [_lastPlace setText:@"Outside of any region"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
