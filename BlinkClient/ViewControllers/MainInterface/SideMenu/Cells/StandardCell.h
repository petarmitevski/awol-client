//
//  StandardCell.h
//  BlinkClient
//
//  Created by Petar on 1/23/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StandardCell : UITableViewCell
@property UILabel *label;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
(NSString *)reuseIdentifier withSize:(CGSize)size;
@end
