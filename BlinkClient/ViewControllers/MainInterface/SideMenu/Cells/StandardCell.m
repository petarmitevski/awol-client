//
//  StandardCell.m
//  BlinkClient
//
//  Created by Petar on 1/23/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "StandardCell.h"

@implementation StandardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
(NSString *)reuseIdentifier withSize:(CGSize)size;

{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        [self.contentView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
        _label=[[UILabel alloc]initWithFrame:self.contentView.frame];
        [_label setTextColor:[UIColor whiteColor]];
        [self.contentView addSubview:view];
        [view addSubview:_label];
    }
    return self;
}

@end
