//
//  HUDViewController.m
//  BlinkClient
//
//  Created by Petar on 12/10/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "HUDViewController.h"
#import "PreloginViewController.h"

@interface HUDViewController ()

@end

@implementation HUDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    if (@available(iOS 10.0, *)) {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        _blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    } else {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        _blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    }
    _blurEffectView.frame = self.view.bounds;
    _blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    [self.view addSubview:_blurEffectView];
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_spinner setCenter:CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2)];
    [_spinner setColor:[UIColor blackColor]];
    [self.view addSubview:_spinner];
    [_spinner startAnimating];
    [_blurEffectView setAlpha:0.0];
}
-(void)viewWillAppear:(BOOL)animated{
    [UIView animateWithDuration:0.5f animations:^{
        
        [_blurEffectView setAlpha:1.0f];
        
    } completion:^(BOOL finished) {
    }];
}
-(void)dismissWithTag:(int)tag{
    [UIView animateWithDuration:0.5f animations:^{
        [_spinner stopAnimating];
        [_blurEffectView setAlpha:0.0f];
        
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];

//        if(tag==110){}
//        else if(tag==120){}
//        else if(tag==130){
        if([_sender isKindOfClass:[PreloginViewController class]]){
            PreloginViewController *PVC=(PreloginViewController*)_sender;
            [PVC didFinishDismissHUDWithTag:tag];
        }
        
 //   }
        
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
