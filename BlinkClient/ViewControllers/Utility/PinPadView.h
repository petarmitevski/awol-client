//
//  PinPadView.h
//  BlinkClient
//
//  Created by Petar on 12/15/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLWBluuurView.h"
#import "KFKeychain.h"

@interface PinPadView : UIView
@property int type;

@property UIView *pinPadView;

@property UIView *dotsView;
    //single dots
    @property UIImageView *dot1;
    @property UIImageView *dot2;
    @property UIImageView *dot3;
    @property UIImageView *dot4;
            //images
            @property UIImage *emptyImage;
            @property UIImage *filledImage;
        //frames
        @property CGRect dotsViewFrameMinus;
        @property CGRect dotsViewFrameMinusMinus;
        @property CGRect dotsViewFramePlus;
        @property CGRect dotsViewFrameZero;

@property int counter;
@property int numberOfPinEnters;
@property NSString* pin1;
@property NSString* pin2;
@property NSMutableString *mutPin;
@property UIViewController *sender;
@property UILabel *titleLabel1;
@property UILabel *titleLabel2;
@property UIView *titleView;

- (id)initWithFrame:(CGRect)frame sender:(UIViewController*)sender type:(int)type;
@end
