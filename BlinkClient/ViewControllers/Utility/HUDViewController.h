//
//  HUDViewController.h
//  BlinkClient
//
//  Created by Petar on 12/10/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HUDViewController : UIViewController
@property UIVisualEffectView* blurEffectView;
@property UIActivityIndicatorView *spinner;

-(void)dismissWithTag:(int)tag;
@property UIViewController *sender;
@end
