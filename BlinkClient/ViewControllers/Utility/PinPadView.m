//
//  PinPadView.m
//  BlinkClient
//
//  Created by Petar on 12/15/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "PinPadView.h"
#import <LocalAuthentication/LocalAuthentication.h>
@implementation PinPadView

-(void)drawRect:(CGRect)rect {

}
- (id)initWithFrame:(CGRect)frame sender:(UIViewController*)sender type:(int)type {
    self = [super initWithFrame:frame];
    if (self) {
        _counter=0;
        _numberOfPinEnters=0;
        _mutPin=[[NSMutableString alloc]init];
        [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
        NSLog(@"sender: %@",sender);
        _sender=sender;
        _type=type;
        if(type==0){//register
            
        }else if(type==1){//login
            
        }else if(type==2){//enter
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(localAuthentication)
                                                         name:@"ShowLocalAuthenticator" object:nil];
        }
    }
    
    _pinPadView=[[UIView alloc]initWithFrame:CGRectMake(frame.origin.x, frame.size.height/2-100, frame.size.width, frame.size.height/2)];
    _dotsView=[[UIView alloc]initWithFrame:CGRectMake(frame.origin.x, frame.size.height/2-270, frame.size.width, 170)];
    [self setupPinPad];
    [self setupDotsView];
    [self setupTitleLabel];
    return self;
}
-(void)setupPinPad{
    UIView *containerView=[[UIView alloc]initWithFrame:_pinPadView.frame];
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(40, 10, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v2=[[UIView alloc]initWithFrame:CGRectMake(v1.frame.size.width+60, 10, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v3=[[UIView alloc]initWithFrame:CGRectMake(v1.frame.size.width*2+80, 10, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v4=[[UIView alloc]initWithFrame:CGRectMake(40, v1.frame.size.height+30, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v5=[[UIView alloc]initWithFrame:CGRectMake(v1.frame.size.width+60, v1.frame.size.height+30, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v6=[[UIView alloc]initWithFrame:CGRectMake(v1.frame.size.width*2+80, v1.frame.size.height+30, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v7=[[UIView alloc]initWithFrame:CGRectMake(40, v1.frame.size.height*2+50, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v8=[[UIView alloc]initWithFrame:CGRectMake(v1.frame.size.width+60, v1.frame.size.height*2+50, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v9=[[UIView alloc]initWithFrame:CGRectMake(v1.frame.size.width*2+80, v1.frame.size.height*2+50, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIView *v0=[[UIView alloc]initWithFrame:CGRectMake(v1.frame.size.width+60, v1.frame.size.height*3+70, _pinPadView.frame.size.width/3-40, _pinPadView.frame.size.width/3-40)];
    UIButton *b1=[[UIButton alloc]initWithFrame:v1.bounds];
    UIButton *b2=[[UIButton alloc]initWithFrame:v2.bounds];
    UIButton *b3=[[UIButton alloc]initWithFrame:v3.bounds];
    UIButton *b4=[[UIButton alloc]initWithFrame:v4.bounds];
    UIButton *b5=[[UIButton alloc]initWithFrame:v5.bounds];
    UIButton *b6=[[UIButton alloc]initWithFrame:v6.bounds];
    UIButton *b7=[[UIButton alloc]initWithFrame:v7.bounds];
    UIButton *b8=[[UIButton alloc]initWithFrame:v8.bounds];
    UIButton *b9=[[UIButton alloc]initWithFrame:v9.bounds];
    UIButton *b0=[[UIButton alloc]initWithFrame:v0.bounds];
    [b1 setTag:101];
    [b2 setTag:102];
    [b3 setTag:103];
    [b4 setTag:104];
    [b5 setTag:105];
    [b6 setTag:106];
    [b7 setTag:107];
    [b8 setTag:108];
    [b9 setTag:109];
    [b0 setTag:100];
    
    [b1 setTitle:@"1" forState:UIControlStateNormal];
    [b1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b1.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b1 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b2 setTitle:@"2" forState:UIControlStateNormal];
    [b2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b2.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b2 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b3 setTitle:@"3" forState:UIControlStateNormal];
    [b3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b3.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b3 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b4 setTitle:@"4" forState:UIControlStateNormal];
    [b4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b4.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b4 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b5 setTitle:@"5" forState:UIControlStateNormal];
    [b5 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b5.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b5 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b6 setTitle:@"6" forState:UIControlStateNormal];
    [b6 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b6.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b6 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b7 setTitle:@"7" forState:UIControlStateNormal];
    [b7 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b7.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b7 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b8 setTitle:@"8" forState:UIControlStateNormal];
    [b8 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b8.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b8 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b9 setTitle:@"9" forState:UIControlStateNormal];
    [b9 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b9.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b9 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];
    
    [b0 setTitle:@"0" forState:UIControlStateNormal];
    [b0 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b0.titleLabel setFont:[UIFont systemFontOfSize:45]];
    [b0 addTarget:self
           action:@selector(buttonClick:)
 forControlEvents:UIControlEventTouchUpInside];

    [v1.layer setCornerRadius:b1.frame.size.width/2];
    [v2.layer setCornerRadius:b2.frame.size.width/2];
    [v3.layer setCornerRadius:b3.frame.size.width/2];
    [v4.layer setCornerRadius:b4.frame.size.width/2];
    [v5.layer setCornerRadius:b5.frame.size.width/2];
    [v6.layer setCornerRadius:b6.frame.size.width/2];
    [v7.layer setCornerRadius:b7.frame.size.width/2];
    [v8.layer setCornerRadius:b8.frame.size.width/2];
    [v9.layer setCornerRadius:b9.frame.size.width/2];
    [v0.layer setCornerRadius:b0.frame.size.width/2];

    [v1 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v2 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v3 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v4 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v5 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v6 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v7 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v8 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v9 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    [v0 setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
    
    [b1 setBackgroundColor:[UIColor clearColor]];
    [b2 setBackgroundColor:[UIColor clearColor]];
    [b3 setBackgroundColor:[UIColor clearColor]];
    [b4 setBackgroundColor:[UIColor clearColor]];
    [b5 setBackgroundColor:[UIColor clearColor]];
    [b6 setBackgroundColor:[UIColor clearColor]];
    [b7 setBackgroundColor:[UIColor clearColor]];
    [b8 setBackgroundColor:[UIColor clearColor]];
    [b9 setBackgroundColor:[UIColor clearColor]];
    [b0 setBackgroundColor:[UIColor clearColor]];

    [v1 addSubview:[self setButtonBlurEffect:v1]];
    [v2 addSubview:[self setButtonBlurEffect:v2]];
    [v3 addSubview:[self setButtonBlurEffect:v3]];
    [v4 addSubview:[self setButtonBlurEffect:v4]];
    [v5 addSubview:[self setButtonBlurEffect:v5]];
    [v6 addSubview:[self setButtonBlurEffect:v6]];
    [v7 addSubview:[self setButtonBlurEffect:v7]];
    [v8 addSubview:[self setButtonBlurEffect:v8]];
    [v9 addSubview:[self setButtonBlurEffect:v9]];
    [v0 addSubview:[self setButtonBlurEffect:v0]];

    [v1 addSubview:b1];
    [v2 addSubview:b2];
    [v3 addSubview:b3];
    [v4 addSubview:b4];
    [v5 addSubview:b5];
    [v6 addSubview:b6];
    [v7 addSubview:b7];
    [v8 addSubview:b8];
    [v9 addSubview:b9];
    [v0 addSubview:b0];
    
    [containerView addSubview:v1];
    [containerView addSubview:v2];
    [containerView addSubview:v3];
    [containerView addSubview:v4];
    [containerView addSubview:v5];
    [containerView addSubview:v6];
    [containerView addSubview:v7];
    [containerView addSubview:v8];
    [containerView addSubview:v9];
    [containerView addSubview:v0];
    
    [self addSubview:containerView];
}
-(void)setupDotsView{
    [_dotsView setBackgroundColor:[UIColor clearColor]];
    UIView *dotsViewCenter=[[UIView alloc]initWithFrame:CGRectMake(_dotsView.frame.size.width/2-67.5f, _dotsView.frame.size.height/2, 135, 50)];
    [dotsViewCenter setBackgroundColor:[UIColor clearColor]];
    
    int size=20;
    _dot1=[[UIImageView alloc]initWithFrame:CGRectMake(5, 15, size, size)];
    _dot2=[[UIImageView alloc]initWithFrame:CGRectMake(40, 15, size, size)];
    _dot3=[[UIImageView alloc]initWithFrame:CGRectMake(75, 15, size, size)];
    _dot4=[[UIImageView alloc]initWithFrame:CGRectMake(110, 15, size, size)];
    
    _emptyImage=[UIImage imageNamed:@"ic_pin_empty"];
    _filledImage=[UIImage imageNamed:@"ic_pin_filled"];
    
    [_dot1 setImage:_emptyImage];
    [_dot2 setImage:_emptyImage];
    [_dot3 setImage:_emptyImage];
    [_dot4 setImage:_emptyImage];
    
    [dotsViewCenter addSubview:_dot1];
    [dotsViewCenter addSubview:_dot2];
    [dotsViewCenter addSubview:_dot3];
    [dotsViewCenter addSubview:_dot4];
    
    [_dotsView addSubview:dotsViewCenter];
    [self addSubview:_dotsView];
    _dotsViewFrameMinusMinus=CGRectMake(-75, _dotsView.frame.origin.y, _dotsView.frame.size.width, _dotsView.frame.size.height);
    _dotsViewFrameMinus=CGRectMake(-25, _dotsView.frame.origin.y, _dotsView.frame.size.width, _dotsView.frame.size.height);
    _dotsViewFramePlus=CGRectMake(+50, _dotsView.frame.origin.y, _dotsView.frame.size.width, _dotsView.frame.size.height);
    _dotsViewFrameZero=CGRectMake(0, _dotsView.frame.origin.y, _dotsView.frame.size.width, _dotsView.frame.size.height);
}
-(void)buttonClick:(UIButton*)sender{
    [UIView animateWithDuration:0.13f animations:^{
        [sender.superview setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.6]];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.13f animations:^{
            [sender.superview setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
        } completion:^(BOOL finished) {
        }];
    }];
    switch (sender.tag) {
        case 101:
            [self keyPressed:@"1"];
            break;
        case 102:
            [self keyPressed:@"2"];
            break;
        case 103:
            [self keyPressed:@"3"];
            break;
        case 104:
            [self keyPressed:@"4"];
            break;
        case 105:
            [self keyPressed:@"5"];
            break;
        case 106:
            [self keyPressed:@"6"];
            break;
        case 107:
            [self keyPressed:@"7"];
            break;
        case 108:
            [self keyPressed:@"8"];
            break;
        case 109:
            [self keyPressed:@"9"];
            break;
        case 100:
            [self keyPressed:@"0"];
            break;
        case 301:
            break;
        default:
            break;
    }
    
}
-(UIVisualEffectView*)setButtonBlurEffect:(UIView*)view{
    MLWBluuurView *visualEffectView;
    visualEffectView = [[MLWBluuurView alloc] init];
    visualEffectView.frame = view.bounds;
    [visualEffectView.layer setCornerRadius:view.frame.size.width/2];
    visualEffectView.clipsToBounds = YES;
    [visualEffectView setBlurRadius:10.0f];
    return visualEffectView;
}
-(void)keyPressed:(NSString *)c{
    NSLog(@"self.superview %@",self.superview);
    NSLog(@"self.superclass %@",self.superclass);
    [_mutPin appendString:c];
    if(_counter==0){
        [_dot1 setImage:_filledImage];
    }
    else if(_counter==1){
        [_dot2 setImage:_filledImage];
    }
    else if(_counter==2){
         [_dot3 setImage:_filledImage];
    }
    else if(_counter==3){
        [_dot4 setImage:_filledImage];
    }
    _counter++;
    if(_counter==4){
        if(_type==0){//register
        if(_numberOfPinEnters==0){
            _pin1=_mutPin;
            _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_pin1 withString:@""] mutableCopy];
            _numberOfPinEnters=1;
            [self enterPin2];
        }
        else if(_numberOfPinEnters==1){
            _pin2=_mutPin;
            if([_pin1 isEqualToString:_pin2]){
                NSLog(@"EQUAL");
                [KFKeychain saveObject:_pin1 forKey:@"PIN"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@YES forKey:@"hasToken"];
                
                
                NSLog(@"PIN: %@",[KFKeychain loadObjectForKey:@"PIN"]);
                NSDictionary * userInfo = @{ @"type" : [NSNumber numberWithInt:_type]};
                [[NSNotificationCenter defaultCenter]
                  postNotificationName:@"PinEntrySucceded"
                  object:self
                  userInfo:userInfo];
                _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_mutPin withString:@""] mutableCopy];
                [self resetPinEntryWithoutAnimation];
            }else {
                NSLog(@"NOT EQUAL");
                _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_pin2 withString:@""] mutableCopy];
                _pin2=[_pin2 stringByReplacingOccurrencesOfString:_pin2 withString:@""];
            [self wrongPinAnimation];
                
            }
        }
        
        }else if(_type==1){//login
            if([_mutPin isEqualToString:[KFKeychain loadObjectForKey:@"PIN"]]){
                NSLog(@"PIN SUCCESSFULL");
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@YES forKey:@"hasToken"];
                NSDictionary * userInfo = @{ @"type" : [NSNumber numberWithInt:_type]};
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"PinEntrySucceded"
                 object:self
                 userInfo:userInfo];
                _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_mutPin withString:@""] mutableCopy];
                [self resetPinEntryWithoutAnimation];
            }else{
                _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_mutPin withString:@""] mutableCopy];
                [self wrongPinAnimation];
            }
        }else if(_type==2){//enter
            if([_mutPin isEqualToString:[KFKeychain loadObjectForKey:@"PIN"]]){
                NSLog(@"PIN SUCCESSFULL");
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@YES forKey:@"hasToken"];
                
                NSDictionary * userInfo = @{ @"type" : [NSNumber numberWithInt:_type]};
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"PinEntrySucceded"
                 object:self
                 userInfo:userInfo];
                _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_mutPin withString:@""] mutableCopy];
                [self resetPinEntryWithoutAnimation];
            }else{
                _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_mutPin withString:@""] mutableCopy];
                [self wrongPinAnimation];
            }
        }
        
    }
}
-(void)wrongPinAnimation{
    [UIView animateWithDuration:0.17f animations:^{
        [_dotsView setFrame:_dotsViewFrameMinusMinus];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.135f animations:^{
            [_dotsView setFrame:_dotsViewFramePlus];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1f animations:^{
                [_dotsView setFrame:_dotsViewFrameMinus];
             } completion:^(BOOL finished) {
                 [UIView animateWithDuration:0.1f animations:^{
                     [_dotsView setFrame:_dotsViewFrameZero];
                 } completion:^(BOOL finished) {
                     _counter=0;
                     [_dot1 setImage:_emptyImage];
                     [_dot2 setImage:_emptyImage];
                     [_dot3 setImage:_emptyImage];
                     [_dot4 setImage:_emptyImage];
                 }];
            }];
        }];
    }];
}
-(void)enterPin2{
    _counter=0;
    [_dot1 setImage:_emptyImage];
    [_dot2 setImage:_emptyImage];
    [_dot3 setImage:_emptyImage];
    [_dot4 setImage:_emptyImage];
    [UIView animateWithDuration:0.3f animations:^{
        CGRect frame=_titleView.frame;
        frame.origin.x=-self.frame.size.width;
        [_titleView setFrame:frame];
        
    } completion:^(BOOL finished) {
        
    }];}
-(void)authenticate:(NSString*)pin{
    
}
-(void)setupTitleLabel{
    _titleView=[[UIView alloc]initWithFrame:CGRectMake(0, _dotsView.frame.origin.y, self.frame.size.width*2, 50)];
    _titleLabel1=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
    [_titleLabel1 setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel1 setTextColor:[UIColor colorWithWhite:1.0f alpha:0.6f]];
    [_titleLabel1 setText:@"Please enter your new PIN!"];
    
    _titleLabel2=[[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width, 0, self.frame.size.width, 50)];
    [_titleLabel2 setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel2 setTextColor:[UIColor colorWithWhite:1.0f alpha:0.6f]];
    [_titleLabel2 setText:@"Please re-enter your PIN!"];
    if(_type==0){//register
    [_titleView addSubview:_titleLabel1];
    [_titleView addSubview:_titleLabel2];
    }else if(_type==1){//login
        [_titleLabel1 setText:@"Please enter your old PIN!"];
        [_titleView addSubview:_titleLabel1];
    }if(_type==2){//enter
        [_titleLabel1 setText:@"Please enter your PIN!"];
        [_titleView addSubview:_titleLabel1];
    }
    [self addSubview:_titleView];
    
}
-(void)resetPinEntryWithoutAnimation{
    _counter=0;
    [_dot1 setImage:_emptyImage];
    [_dot2 setImage:_emptyImage];
    [_dot3 setImage:_emptyImage];
    [_dot4 setImage:_emptyImage];
}
-(void)localAuthentication{
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"check";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    [self authenticated];
                                } else {
                                     NSLog(@"not good, %@",error);
                                }
                            }];
    } else {
         NSLog(@"not good good %@",authError);
        // Could not evaluate policy; look at authError and present an appropriate message to user
    }
}
-(void)authenticated{
    dispatch_async(dispatch_get_main_queue(), ^{
        _mutPin=[[_mutPin stringByReplacingOccurrencesOfString:_mutPin withString:@""] mutableCopy];
        [self resetPinEntryWithoutAnimation];
        NSDictionary * userInfo = @{ @"type" : [NSNumber numberWithInt:_type]};
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"PinEntrySucceded"
         object:self
         userInfo:userInfo];
    });
}
@end
