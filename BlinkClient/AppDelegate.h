//
//  AppDelegate.h
//  BlinkClient
//
//  Created by Petar on 10/14/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import <Firebase/Firebase.h>
#import <UserNotifications/UserNotifications.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate, FIRMessagingDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property  CLLocationManager *locationManager;

- (void)saveContext;


@end

