//
//  AppDelegate.m
//  BlinkClient
//
//  Created by Petar on 10/14/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "AppDelegate.h"
#import "BeaconTestReceiver.h"
#import "NSBundle+Language.h"
#import "Localization.h"

@interface AppDelegate ()

@end

@implementation AppDelegate{
    //    CLLocationManager *_locationManager;
    BOOL _isInsideRegion; // flag to prevent duplicate sending of notification
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];
    FIRFirestore *defaultFirestore = [FIRFirestore firestore];
    [FIRMessaging messaging].delegate = self;
    NSString *fcmToken = [FIRMessaging messaging].FCMToken;
    
    //        NSLog(@"FCM registration token: %@", fcmToken);
    //    NSString *documentName=[NSString stringWithFormat:@"token-%@",fcmToken];
    //     __block FIRDocumentReference *ref = [[defaultFirestore collectionWithPath:@"testCollection"]documentWithPath:documentName];
    //    [ref setData:@{
    //                   @"first": @"Petar",
    //                   @"last": @"Mitevski",
    //                   @"token": fcmToken
    //                   } completion:^(NSError * _Nullable error) {
    //                       if (error != nil) {
    //                           NSLog(@"Error adding document: %@", error);
    //                       } else {
    //                           NSLog(@"Document added with ID: %@", ref.documentID);
    //                       }
    //                   }];
    //
    //
    //    [ref addSnapshotListener:^(FIRDocumentSnapshot *snapshot, NSError *error) {
    //        if (snapshot == nil) {
    //            NSLog(@"Error fetching document: %@", error);
    //            return;
    //        }
    //        NSLog(@"Current data: %@", snapshot.data);
    //    }];
    //
    //    [[[defaultFirestore collectionWithPath:@"RetailCustomers"]documentWithPath:@"w1d4rfbmtBt8rRw9BgfZ"] getDocumentWithCompletion:^(FIRDocumentSnapshot * _Nullable snapshot, NSError * _Nullable error) {
    //        if(snapshot==nil){
    //            NSLog(@"ERROR");
    //        }
    //        NSLog(@"Data: %@",snapshot.data);
    //    }];
    //
    //    [[[defaultFirestore collectionWithPath:@"RetailCustomers"]documentWithPath:@"w1d4rfbmtBt8rRw9BgfZ"] setData:
    //      @{@"Token":fcmToken}
    //      options:[FIRSetOptions merge]
    //          completion:^(NSError * _Nullable error) {
    //              if(!error){
    //                  NSLog(@"Document updated");
    //              }
    //          }];
    //
    //     FIRQuery *query=[[defaultFirestore collectionWithPath:@"RetailCustomers"] queryWhereField:@"Surnmae" isEqualTo:@"Petar"];
    //
    //    [query getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
    //        NSLog(@"QUERY RESULT: %@",snapshot.documents);
    //        for(FIRDocumentSnapshot *_Nullable snapshotDocument in snapshot.documents){
    //            NSLog(@"Query Result: %@ data: %@",snapshotDocument.documentID,snapshotDocument.data);
    //        }
    //    }];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [application registerForRemoteNotifications];
    
    
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"DefaultLanguage"]){
        [NSBundle setLanguage:@"en"];
    }
    
    // create a location manager
    _locationManager = [[CLLocationManager alloc] init];
    
    // set delegate, not the angle brackets
    _locationManager.delegate = self;
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager requestAlwaysAuthorization];
    
    NSUUID *UUID = [[NSUUID alloc] initWithUUIDString:@"D5B2A2B1-EE59-4992-B6F7-C41BB23BEE35"];
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:UUID
                                                                identifier:@"BlinkAppIdentifier4992"];
    
    //CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
    //                                                            identifier:@"blah"];
    //region.notifyOnEntry = YES;
    //region.notifyOnExit = YES;
    //region.notifyEntryStateOnDisplay = YES;
    //
    //[self.locationManager startMonitoringForRegion:region];
    //[self.locationManager startRangingBeaconsInRegion:region];
    
    
    
    // launch app when display is turned on and inside region
    region.notifyOnEntry=YES;
    region.notifyOnExit=YES;
    region.notifyEntryStateOnDisplay = YES;
    
    if ([CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]])
    {
        [_locationManager startMonitoringForRegion:region];
        [_locationManager startRangingBeaconsInRegion:region];
        
        // get status update right away for UI
        [_locationManager requestStateForRegion:region];
    }
    else
    {
        NSLog(@"This device does not support monitoring beacon regions");
    }
    
    // Override point for customization after application launch.
    
    
   
    return YES;
    
}
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}
- (void)_sendEnterLocalNotification
{
    //if (!_isInsideRegion)
    // {
    UILocalNotification *notice = [[UILocalNotification alloc] init];
    [notice setFireDate:[NSDate date]];
    [notice setTimeZone:[NSTimeZone defaultTimeZone]];
    notice.alertBody = @"Inside Estimote beacon region!";
    notice.alertAction = @"Open";
    notice.soundName = UILocalNotificationDefaultSoundName;
    
    
    //    notice.applicationIconBadgeNumber=1;
    //    [notice setHasAction:YES];
    //        [[UIApplication sharedApplication] scheduleLocalNotification:notice];
    //  }
    
    //  _isInsideRegion = YES;
}

- (void)_sendExitLocalNotification
{
    // if (_isInsideRegion)
    //{
    UILocalNotification *notice = [[UILocalNotification alloc] init];
    
    notice.alertBody = @"Left Estimote beacon region!";
    notice.alertAction = @"Open";
    notice.soundName = UILocalNotificationDefaultSoundName;
    
    //  [[UIApplication sharedApplication] scheduleLocalNotification:notice];
    //  }
    
    //  _isInsideRegion = NO;
}

- (void)_updateUIForState:(CLRegionState)state
{
    
    UIStoryboard *sb=[UIStoryboard storyboardWithName:@"FirstView" bundle:nil];
    
    BeaconTestReceiver *vc=[sb instantiateViewControllerWithIdentifier:@"BeaconTestReceiver"];
    // ViewController *vc = (ViewController *)self.window.rootViewController;
    
    if (state == CLRegionStateInside)
    {
        vc.label.text = @"Inside";
        NSLog(@"Inside");
    }
    else if (state == CLRegionStateOutside)
    {
        vc.label.text = @"Outside";
        NSLog(@"Outside");
        
    }
    else
    {
        vc.label.text = @"Unknown";
        NSLog(@"Unknown");
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    // always update UI
    [self _updateUIForState:state];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        // don't send any notifications
        return;
    }
    
    if (state == CLRegionStateInside)
    {
        [self _sendEnterLocalNotification];
    }
    else
    {
        [self _sendExitLocalNotification];
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
    
    NSLog(@"entered in region");
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"enterInRegion" object:self];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    NSLog(@"exit region");
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"exitRegion" object:self];
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    
    //NSLog(@"beacon: %@",beacons);
    if(beacons.count>0){
        //    for(CLBeacon*beacon in beacons){
        //        NSLog(@"UUID:  %@",beacon.proximityUUID);
        //        NSLog(@"Major: %@",beacon.major);
        //        NSLog(@"Minor: %@",beacon.minor);
        //        NSLog(@"Proximity: %ld",(long)beacon.accuracy);
        //
        //    }
        [self _sendEnterLocalNotification];
    }else{
        [self _sendExitLocalNotification];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"BlinkClient"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive");
        
        //Show the view with the content of the push
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        NSLog(@"Background");
        
        NSString *info = [[userInfo valueForKey:@"aps"]valueForKey:@"alert"];
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        
        NSLog(@"Active");
        
        //Show an in-app banner
        NSLog(@"userInfo: %@",userInfo);
        if([[userInfo objectForKey:@"gcm.notification.notifType"] isEqualToString:@"1"]){//ibeacon
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"newPayment" object:self userInfo:userInfo];
            
        }
        completionHandler(UIBackgroundFetchResultNewData);
        
    }
}
#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
