//
//  AppCore.m
//  BlinkClient
//
//  Created by Petar on 12/27/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "AppCore.h"

@implementation AppCore

- (void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];

    if(!_idleTimer) {
        [self resetIdleTimer];
    }
    NSSet *allTouches = [event allTouches];
    
    if  ([allTouches count] > 0)
    {
        UITouchPhase phase  = ((UITouch *)[allTouches anyObject]).phase;
        if  (phase == UITouchPhaseBegan)
        {
            [self resetIdleTimer];
            //NSLog(@"Catched event");
        }
    }
}

- (void)resetIdleTimer
{
    if  (_idleTimer)
    {
        [_idleTimer invalidate];
    }
    _idleTimer = [NSTimer scheduledTimerWithTimeInterval:ApplicationTimeoutInMinutes*60
                                                  target:self
                                                selector:@selector(idleTimerExceeded)
                                                userInfo:nil
                                                 repeats:NO];
    
}

- (void)idleTimerExceeded
{
    NSLog(@"pomina vremeto");
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:ApplicationDidTimeoutNotification object:nil];
}

+ (UIViewController*)topController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


@end
