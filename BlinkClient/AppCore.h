//
//  AppCore.h
//  BlinkClient
//
//  Created by Petar on 12/27/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#define ApplicationTimeoutInMinutes 1
#define ApplicationDidTimeoutNotification @"ApplicationDidTimeout"
@interface AppCore : UIApplication
{
    NSTimer *_idleTimer;
}
- (void)resetIdleTimer;
+ (UIViewController*)topController;
@end
