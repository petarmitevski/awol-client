//
//  UIColor+ColorPallete.m
//  BlinkClient
//
//  Created by Petar on 12/9/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "UIColor+ColorPallete.h"

@implementation UIColor (ColorPallete)
+(UIColor *)preloginPurpleColor{
    return [UIColor colorWithRed:0.5 green:0.87 blue:0.0 alpha:1.0];
}


@end
