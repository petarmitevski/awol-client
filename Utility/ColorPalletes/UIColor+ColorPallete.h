//
//  UIColor+ColorPallete.h
//  BlinkClient
//
//  Created by Petar on 12/9/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorPallete)
+(UIColor *)preloginPurpleColor;
@end
