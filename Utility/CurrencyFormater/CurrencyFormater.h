//
//  CurrencyFormater.h
//  BlinkClient
//
//  Created by Petar on 1/2/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyFormater : NSObject
+(NSString*)formatNumber:(NSNumber*)number;
@end
