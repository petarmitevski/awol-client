//
//  CurrencyFormater.m
//  BlinkClient
//
//  Created by Petar on 1/2/18.
//  Copyright © 2018 Petar. All rights reserved.
//

#import "CurrencyFormater.h"

@implementation CurrencyFormater

+(NSString*)formatNumber:(NSNumber*)number{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"mk_MK"]];
    NSString* str = [formatter stringFromNumber:number];
    
    return str;
}
@end
