//
//  Localization.h
//  BlinkClient
//
//  Created by Petar on 12/9/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Localization : NSObject
+(instancetype)sharedInstance;
+(NSString*)localizedKey:(NSString*)string;
@end
