//
//  Localization.m
//  BlinkClient
//
//  Created by Petar on 12/9/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import "Localization.h"

@implementation Localization
+ (instancetype)sharedInstance {
    static Localization *localization = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        localization = [[self alloc] init];
    });
    return localization;
}

- (id)init {
    self = [super init];
    return self;
}

- (void)dealloc {
}

+(NSString*)localizedKey:(NSString*)string{
    return NSLocalizedString(string, string);
}
@end
