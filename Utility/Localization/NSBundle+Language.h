//
//  NSBundle+Language.h
//  BlinkClient
//
//  Created by Petar on 12/9/17.
//  Copyright © 2017 Petar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)
+(void)setLanguage:(NSString*)language;
@end
